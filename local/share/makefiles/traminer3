# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# load modules for all phases
define load_modules
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load tools/imrdenom/0.4.0f; \
	module load sw/bio/samtools/0.1.19; \
	module load libs/zlib/1.2.8; \
	module load sw/bio/genometools/1.4.1; \
	module load sw/aligners/mummer/3.23; \
	module load sw/bio/tom/trunk; \
	module load tools/ucsc-utils/16-Dec-2013; \
	module load tools/bedtools/2.20.1; \
	module load tools/bedops/2.4.2; \
	module load tools/picard-tools/1.139; \
	module load aligners/lastz/1.03.54
endef


REFERENCE := /mnt/vol1/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta.gz

CONTIGS := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/traminer3/phase_2/1bp.final.contigs.fasta.gz

SCAFFOLDS := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/traminer3/phase_2/1bp.final.assembly.fasta.gz

HETEROZYGOSITY_SEG := ../../zygosity/heterozygosity.3-6.seg

# bed file  that defines homozygous and heterozygous regions:
#       1       chromosome      chr1
#       2       start   0
#       3       end     9000000
#       4       comment (het homo)      het
CUSTOM_HET_REGIONS_BED := ../../zygosity/vitis_vinifera/custom_heterozygous_regions.bed
CUSTOM_HOMO_REGIONS_BED := ../../zygosity/vitis_vinifera/custom_homozygous_regions.bed
#

FINAL_SUMMARY := /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/allpaths_assembly/dataset/traminer3/phase_3/final.summary


SAMPLE_NAME := traminer3

# need by phase_6
EXTERN_ALN := yes

# frequency of kmers for given position
KMER_FREQ_WIG := ../../repeats/test-vidotto-20mers.wig.gz

# list of chromosomes on witch calculate metrix in phase 5
QUERY_NAME := chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19

# annotations - need by phase_8
GENES := ../../annot/vitis_vinifera/genes.V2.1.gff3

REPEATS := ../../annot/vitis_vinifera/repeats.gff

LNC_ANTISENSE := ../../annot/vitis_vinifera/lncAntisense.gff3

LNC_INTERGENIC := ../../annot/vitis_vinifera/lncIntergenic.gff3

LNC_INTRONIC := ../../annot/vitis_vinifera/lncIntronic.gff3
