# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

TYPE := repeats genes mirna

annotations.gff3.gz:
	wget -q -O - $(GFF_SOURCE) >$@

# make annotations compatible with 
# zea maize genome v3 with chr named as chr01, chr02, chr03, ..
sobstituted.gff3: annotations.gff3.gz
	zcat $< \
	| bawk '/^[\#+]/ { print $$0; }' \
	| sed 's/n   1 /n   chr01 /' \
	| sed 's/n   2 /n   chr02 /' \
	| sed 's/n   3 /n   chr03 /' \
	| sed 's/n   4 /n   chr04 /' \
	| sed 's/n   5 /n   chr05 /' \
	| sed 's/n   6 /n   chr06 /' \
	| sed 's/n   7 /n   chr07 /' \
	| sed 's/n   8 /n   chr08 /' \
	| sed 's/n   9 /n   chr09 /' \
	| sed 's/n   10 /n   chr10 /' \
	| sed '/^###/d'>$@; \
	paste \
	<( zcat <$< \
	| bawk '!/^[\#+,$$]/ { print $$1, $$2; }' \
	| sed 's/^1\t/chr01\t/' \
	| sed 's/^2\t/chr02\t/' \
	| sed 's/^3\t/chr03\t/' \
	| sed 's/^4\t/chr04\t/' \
	| sed 's/^5\t/chr05\t/' \
	| sed 's/^6\t/chr06\t/' \
	| sed 's/^7\t/chr07\t/' \
	| sed 's/^8\t/chr08\t/' \
	| sed 's/^9\t/chr09\t/' \
	| sed 's/^10\t/chr10\t/' \
	| cut -f1) \
	<(zcat $< \
	| bawk '!/^[\#+,$$]/ { print $$2, $$3, $$4, $$5, $$6, $$7, $$8, $$9; }') \
	>>$@

sobstituted.gff3.gz: sobstituted.gff3
	gzip -c $< >$@

STATS = $(addsuffix .stats,$(TYPE))
%.stats: %.gff3.gz
	$(call load_modules); \
	gt stat -v $< >$@

VAL = $(addsuffix .validation,$(TYPE))
%.validation: %.gff3.gz
	$(call load_modules); \
	gt gff3validator $< >$@

sources: sobstituted.gff3.gz
	zcat $< \
	| bawk '!/^[\#+,$$]/ { print $$0 }' \
	| cut -f2 \
	| bsort \
	| uniq >$@

types: sobstituted.gff3.gz
	zcat $< \
	| bawk '!/^[\#+,$$]/ { print $$0 }' \
	| cut -f2 \
	| bsort \
	| uniq >$@

# GENE
# gene
# CDS
# exon
# five_prime_UTR
# three_prime_UTR
# transcript

# MIRNA
# miRNA
# miRNA_gene

# REPEATS
# repeat_region

##################

# REPEATS
# dust
# repeatmasker
# trf

# MIRNA
# mirbase

# GENE
# gramene

GFF = $(addsuffix .gff3.gz,$(TYPE))
repeats.gff3.gz: sobstituted.gff3.gz
	$(call load_modules); \
	zcat $< \
	| bawk '!/^[\#+,$$]/ { if ( $$2 ~ /dust|repeatmasker|trf/ ) print $$0; }' \
	| gt gff3 -retainids -addids yes -fixregionboundaries yes -sort yes -tidy yes -checkids yes -gzip -o $@ -

genes.gff3.gz: sobstituted.gff3.gz
	$(call load_modules); \
	zcat $< \
	| bawk '!/^[\#+,$$]/ { if ( $$2 ~ /gramene/ ) print $$0; }' \
	| gt gff3 -retainids -addids yes -fixregionboundaries yes -sort yes -tidy yes -checkids yes  -gzip -o $@ -

mirna.gff3.gz: sobstituted.gff3.gz
	$(call load_modules); \
	zcat $< \
	| bawk '!/^[\#+,$$]/ { if ( $$2 ~ /mirbase/ ) print $$0; }' \
	| gt gff3 -retainids -addids yes -fixregionboundaries yes -sort yes -tidy yes -checkids yes  -gzip -o $@ -


ALL += annotations.stats \
	annotations.validation \
	sobstituted.stats \
	sobstituted.validation \
	sources \
	types \
	$(GFF) \
	$(STATS) \
	$(VAL)

INTERMEDIATE += sobstituted.gff3

CLEAN +=
