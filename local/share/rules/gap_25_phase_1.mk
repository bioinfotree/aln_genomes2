# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

# tranform alignments to bed12 format
assembly.bamToBed.bed: assembly.bam
	$(call load_modules); \
	bamToBed \
	-color "255,0,0" \
	-cigar \
	-split \
	-bed12 \
	-i $< \
	>$@

### ScaPA ################################################################

.META: filtered.assembly.bam_placed-full.txt
	1	query_scaff
	2	query_start
	3	query_end
	4	query_span
	5	scaffold_length
	6	subj_chr
	7	chr_start
	8	chr_end
	9	chr_span
	10	query_coverage
	11	parsimony
	12	orientation
	13	SW-anchored
	14	SCORE
	15	identity
	16	glob_span
	17	seq_span
	18	gaps_cumul
	19	self.aligned
	20	alignments
	21	merged_object


# export alignments of the golden block for each placed scaffold
# in bed-12 format
filtered.assembly.bam_placed-full.txt: assembly.bam final.summary contigs.fasta
	ScaPA_alpha_r004 \
	--alignment-files $< \
	--final-summary $^2 \
	--contig-fasta $^3 \
	--max-aln-gap 25000 \
	--dump-aln \
	--out $(firstword $(subst _, ,$@))


filtered.assembly.bam_placed.alignments.bed: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_placed-splits.txt: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_unaligned.txt: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_ambiguous.txt: filtered.assembly.bam_placed-full.txt
	touch $@


###### test algorithm to place more ambiguous scaffolds
.META: ambiguous.placed.bed
	1       chr	chr1
	2       chr_start	14727718
	3       chr_end	14729875
	4       query	scaffold_7678
	5       score	8
	6       strand	+
	7       query_start	1
	8       query_end	12081
	9       color	255,0,0
	10      fixed to 1
	11      chr size 	2157
	12      fixed to 0	0
	13      scaffold_ length	12081


ambiguous.placed.bed: filtered.assembly.bam_ambiguous.txt
	module purge; \
	module load lang/python/2.7; \
	cat << 'EOF' | python - >$@
	
	# to obtain floating point from division
	from __future__ import division
	from vfork.io.util import safe_rstrip
	import operator
	
	class Aln(object):
			def __init__(self, **kwargs):
				self.scaff = [ kwargs.get('scaff', '') ]
				self.scaff_start = [ kwargs.get('scaff_start', '') ]
				self.scaff_end = [ kwargs.get('scaff_end', '') ]
				self.scaff_frac = [ kwargs.get('scaff_frac', '') ]
				
				self.chr = [ kwargs.get('chr', '') ]
				self.chr_start = [ kwargs.get('chr_start', '') ]
				self.chr_end = [ kwargs.get('chr_end', '') ]
				self.chr_span = [ kwargs.get('chr_span', '') ]
				
				self.strand = [ kwargs.get('strand', '') ]
	
	def main():
			alns = {}
			scaff_len = {}
			with open('$<', 'r') as f:
				for i, l in enumerate(f):
					if i == 0:
							continue
					
					token = safe_rstrip(l).split()
					
					aln = Aln()
					# scaffold name
					aln.scaff = token[0]

					aln.scaff_start = int(token[1])
					
					aln.scaff_end = int(token[2])
					# fraction aligned. Floating point division
					aln.scaff_frac =  int(token[3]) / int(token[4])
					# chr name
					aln.chr = token[5]
					
					aln.chr_start = int(token[6])
					
					aln.chr_end = int(token[7])
					# chr span in bp
					aln.chr_span = int(token[8])
					
					aln.strand = '-' if float(token[11]) == -1 else '+'
					
					# length of the scaffold
					scaff_len[token[0]] = int(token[4])
					
					if not aln.scaff in alns:
						l = []
						alns[aln.scaff] = l
					
					alns[aln.scaff].append(aln)

			for scaff in alns.keys():
				chrs = {}
				#print k, ' '.join([a.chr for a in alns[k]])
				for a in alns[scaff]:
					if not a.chr in chrs:
						chrs[ a.chr ] = 0.0
					
					chrs[ a.chr ] += a.scaff_frac
				# return tuple of chromosomes and total span in this chromosome, sorted by total span in
				# descending order
				sorted_list = sorted(chrs.items(), key=operator.itemgetter(1), reverse=True)
				
				place = ''			
				for chr in sorted_list:
					# scaffolds result placed if they span for more
					# then 70% in a single chromosome.
					# The chromosome were they spann more is the first 
					if chr[1] >= 0.7:
						place = chr[0]
					break
				
				scaff_placed_start = []
				scaff_placed_end = []
				chr_placed_start = []
				chr_placed_end = []
				if place:
					for a in alns[scaff]:
						if a.chr == place:
							# for the scaffolds placed, get all the start and end positions
							# of the alignments for the chromosome were they should bed
							# placed
							scaff_placed_start.append(a.scaff_start)
							scaff_placed_end.append(a.scaff_end)
							chr_placed_start.append(a.chr_start)
							chr_placed_end.append(a.chr_end)
					# new spanning positions are calculated from
					# the min aln start to the max aln end
					new_chr_start = min(chr_placed_start)
					new_chr_end = max(chr_placed_end)
					new_scaff_stat = min(scaff_placed_start)
					new_scaff_end = max(scaff_placed_end)
					
					# select only the scaffolds that span to the reference <= 10 times their spannig
					if ( new_chr_end - new_chr_start ) <= 10 * ( new_scaff_end - new_scaff_stat ):
						# extra column with scaffold length
						print '\t'.join([ place, str(min(chr_placed_start)), str(max(chr_placed_end)), scaff, '8', '+', str(min(scaff_placed_start)), str(max(scaff_placed_end)), '255,0,0', '1', str(new_chr_end - new_chr_start), '0', str(scaff_len[scaff]) ])
					#else:
						#print '\t'.join([ '*', place, str(min(chr_placed_start)), str(max(chr_placed_end)), str(new_chr_end - new_chr_start), scaff, str(min(scaff_placed_start)), str(max(scaff_placed_end)), str(scaff_len[scaff]) ])
			return 0
	
	if __name__ == '__main__':
			main()
	
	EOF
	
# get statistics of newly placed ambiguous
ambiguous.placed.stat: ambiguous.placed.bed unaligned.fasta
	$(call load_modules);
	placed_bp=$$(bawk '{ print $$8 - $$7 + 1 }' $< \
	| stat_base --total 1 --precision 10);
	trimmed_off=$$(bedtools subtract -a <(bawk '{ print $$4, 0, $$12 }' $<) -b <(bawk '{ print $$4, $$7 - 1, $$8 }' $<) \
	| bawk '{ print $$3 - $$2 }' \
	| stat_base --total 1 --precision 10);
	total_bp=$$(bawk '{ print $$12 }' $< \
	| stat_base --total 1 --precision 10);
	unaligned_scaff=$$(fasta_count -s <$^2);
	unaligned_bp=$$(fasta_count -l <$^2);
	cat <<EOF >>$@
	rescued bp:	$$placed_bp
	trimmed off:	$$trimmed_off
	total bp:	$$total_bp
	unaligned scaff:	$$unaligned_scaff
	unaligned bp:	$$unaligned_bp
	EOF
######


.META: assembly.bed
	1       chr	chr1
	2       chr_start	14727718
	3       chr_end	14729875
	4       query	scaffold_7678
	5       score	8
	6       strand	+
	7       query_start	1
	8       query_end	12081
	9       color	255,0,0
	10      fixed to 1
	11      chr size 	2157
	12      fixed to 0	0


# contains placed scaffolds as assembly.bed
# In addition to the scaffolds placed in a clean way, 
# it also contains the scaffolds placed after the split operation.
assembly.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@


### ScaPA ################################################################

### ambiguous
ambiguous.fasta: filtered.assembly.bam_ambiguous.txt scaffolds.fasta
	$(call load_modules);
	(
	unhead $< \
	| cut -f1 \
	| bsort \
	| uniq \
	| xargs samtools faidx $^2
	) >$@

### unaligned
unaligned.fasta: filtered.assembly.bam_unaligned.txt scaffolds.fasta
	$(call load_modules);
	(
	cat $< \
	| xargs samtools faidx $^2
	) >$@

ambiguous-unaligned.fasta: ambiguous.fasta unaligned.fasta
	cat $^ >$@

### select genes that lie on ambiguous unaligned
ambiguous-unaligned.annot.sort.bed: ambiguous.fasta unaligned.fasta ../../../../pangenome4/dataset/pn_ref-rk_4/phase_2/annot.sort.bed
	filter_1col 1 \
	<(cat $< $^2 \
	| fasta2tab  \
	| cut -f1) <$^3 >$@
	
#############


ALL += filtered.assembly.bam_placed-full.txt \
	filtered.assembly.bam_placed.alignments.bed \
	filtered.assembly.bam_placed-splits.txt \
	filtered.assembly.bam_unaligned.txt \
	filtered.assembly.bam_ambiguous.txt \
	ambiguous.placed.bed \
	assembly.bamToBed.bed \
	assembly.bed \
	unaligned.fasta