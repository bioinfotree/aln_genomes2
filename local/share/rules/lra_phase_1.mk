# Copyright 2021 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

# .ONESHELL:

REFERENCE ?=

CONTIG = $(addsuffix .contigs.fasta,$(SAMPLES))
SCAFF = $(addsuffix .scaffolds.fasta,$(SAMPLES))
%.contig.fasta %.scaffolds.fasta:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell cat <$(call get,$(SAMPLE),CTIG) >$(SAMPLE).contigs.fasta) \
		$(shell cat <$(call get,$(SAMPLE),SCAFF) >$(SAMPLE).scaffolds.fasta) \
	)
	sleep 3

log:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@



# test bwasw aligner
.PRECIOUS: %.bwasw.bam
%.bwasw.bam: log $(REFERENCE) %.fasta
	!threads
	$(call load_env); \
	/usr/bin/time -v \
	bwa bwasw \
	-t $$THREADNUM \
	$^2 $^3 \
	2>$</bwa-bwasw.$@.log \
	| samtools view -S -b \
	| samtools sort -@ $$THREADNUM -o $@ -m 4G \
	&& samtools index $@


SCAFF_BWASW_VCF =   $(addsuffix .scaffolds.bwasw.vcf,$(SAMPLES))
CONTIGS_BWASW_VCF = $(addsuffix .contigs.bwasw.vcf,$(SAMPLES))
%.bwasw.vcf: log $(REFERENCE) %.bwasw.bam
	$(call load_env); \
	bcftools mpileup -f $^2 $^3 \
	| bcftools call --ploidy 1 -mv -Ov -o $@ \
	2>$</samtools-mpileup.$@.log



# test lra aligner
.PRECIOUS: %.lra.bam
%.lra.bam: log $(REFERENCE) %.fasta
	!threads
	$(call load_env); \
	/usr/bin/time -v \
	lra align \
	-CONTIG \
	$^2 $^3 \
	-t $$THREADNUM \
	-p s \
	2>$</lra.$@.log \
	| samtools view -S -b \
	| samtools sort -@ $$THREADNUM -o $@ -m 4G \
	&& samtools index $@



SCAFF_LRA_VCF =   $(addsuffix .scaffolds.lra.vcf,$(SAMPLES))
CONTIGS_LRA_VCF = $(addsuffix .contigs.lra.vcf,$(SAMPLES))
%.lra.vcf: log $(REFERENCE) %.lra.bam
	$(call load_env); \
	bcftools mpileup -f $^2 $^3 \
	| bcftools call --ploidy 1 -mv -Ov -o $@ \
	2>$</samtools-mpileup.$@.log





.PHONY: test
test:
	@echo $(SCAFF_BWASW_VCF)
	@echo $(CONTIGS_BWASW_VCF)


ALL += log \
	reference.fasta \
	$(SCAFF_BWASW_VCF) \
	$(CONTIGS_BWASW_VCF) \
	$(SCAFF_LRA_VCF) \
	$(CONTIGS_LRA_VCF)

INTERMEDIATE +=

CLEAN +=
