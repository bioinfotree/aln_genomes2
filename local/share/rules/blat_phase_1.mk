# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# heterozygosity Vs coverage of contigs on the reference

# TODO:

context prj/kmer_count2

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

MIN_REF_LEN ?= 50000

# log dir
logs:
	mkdir -p $@

contigs.fasta:
	fasta2tab <$(CONTIGS) \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }' \
	| bsort \
	| tab2fasta 2 \
	| resolve_iupac --bed-file=contigs.sub.bed >$@

# save list of chunks to make var
chunks.mk:
	ARRAY=( $$(seq -s " " $(SPLIT)) ); \
	echo "FASTA_CHUNCKS := $${ARRAY[@]/#/contigs.fasta.}" >$@

include chunks.mk

# split fasta
contigs.fasta.%: contigs.fasta
	$(call load_modules); \
	if [ "$*" == "1" ]; then \ 
		gt splitfasta -numfiles $(SPLIT) -force yes $<; \
	else \
		sleep 5; \   * wait untill all pieces are generated *
	fi


scaffolds.fasta:
	ln -sf $(SCAFFOLDS) $@

# denom can't handle to short references
reference.fasta:
	zcat $(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( $$1 ~ /Pt|Mt|chr[0-9]+/ ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 \
	| resolve_iupac --bed-file=reference.sub.bed >$@

reference.2bit: reference.fasta
	$(call load_modules); \
	faToTwoBit $< $@


# contigs.psl: reference.2bit contigs.fasta
	# !threads
	# $(call load_modules); \
	# /usr/bin/time -v \
	# pblat -threads=$$THREADNUM -stepSize=5 -repMatch=2253 -minScore=0 -minIdentity=0 $< $^2 $@
	# 2>&1 \
	# | tee $@.mm.blast

BLAT_PSL = $(addprefix contigs.psl.,$(shell seq $(SPLIT)))
contigs.psl.%: reference.2bit contigs.fasta.%
	$(call load_modules); \
	/usr/bin/time -v \
	blat -stepSize=5 -repMatch=2253 -minScore=0 -minIdentity=0 $< $^2 $@ \
	2>&1 \
	| tee $@.mm.blat

contigs.sort.psl: $(BLAT_PSL)
	$(call load_modules); \
	pslCat -check -nohead $^ \
	| bsort -k 10 >$@

contigs.best.sort.psl: contigs.sort.psl
	$(call load_modules); \
	pslReps $< $@ $(basename $@).psr

contigs.best.sort.sam: contigs.fasta contigs.best.sort.psl
	this-psl2sam $< $^2 >$@

contigs.best.sort.bam: contigs.best.sort.sam
	$(call load_modules); \
	samtools view $<


.PHONY: test
test:
	@echo 


ALL += contigs.fasta \
	$(BLAT_PSL)

INTERMEDIATE += 

CLEAN += 