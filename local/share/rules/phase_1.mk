# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# heterozygosity Vs coverage of contigs on the reference

# TODO:

.ONESHELL:

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

MIN_REF_LEN ?= 50000

# log dir
log:
	mkdir -p $@


contigs.fasta:
	zcat <$(CONTIGS) >$@

scaffolds.fasta:
	zcat <$(SCAFFOLDS) >$@

heterozygosity.seg:
	ln -sf $(HETEROZYGOSITY_SEG) $@

final.summary:
	ln -sf $(FINAL_SUMMARY) $@

# denom can't handle to short references
reference.fasta:
	zcat $(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( length($$NF) >= $(MIN_REF_LEN) ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 >$@


# save list of chunks to make var
chunks.mk:
	!threads
	ARRAY=( $$(seq -s " " 8) ); \
	echo "FASTA_CHUNCKS := $${ARRAY[@]/#/contigs.fasta.}" >$@

include chunks.mk


# split fasta
contigs.fasta.%: contigs.fasta
	$(call load_modules); \
	if [ "$*" == "1" ]; then \ 
		gt splitfasta -numfiles 8 -force yes $<; \
	else \
		sleep 5; \   * wait untill all pieces are generated *
	fi



# execute denom for every chunks
# it runs in succession the indexing and alignment. 
# In oreder to avoid to rebuild indexs for each chunk, I wait for it to finish indexing and allignment of chuck 1.
# For chuncks >1, I create links for indexs in directory 1 to their respective directories. I run denom easyrun.
# It finds the index files and proceeds whith the alignment, that can be executed in parallel for all the chunk> 1.
FASTA_BAMS = $(addsuffix .bam,$(addprefix assembly.,$(shell seq 8)))
assembly.%.bam: reference.fasta contigs.fasta.% log
	$(call load_modules); \
	mkdir -p $*; \
	if [ "$*" == "1" ]; then \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>../$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	else \
		until [ -s 1/assembly.1.bam ]; do \   * wait for assembly.1.bam to be ready *
			sleep 15; \
			echo "process for chunk $* is spleeping 15 seconds.."; \
		done; \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		ln -sf ../1/$<.amb .; \
		ln -sf ../1/$<.ann .; \
		ln -sf ../1/$<.bwt .; \
		ln -sf ../1/$<.pac .; \
		ln -sf ../1/$<.fai .; \
		ln -sf ../1/$<.sa .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>../$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	fi



# merge and sort the bams
assembly.bam: $(FASTA_BAMS)
	!threads
	$(call load_modules); \
	samtools merge -f \
	- $^ \
	| samtools sort -@ $$THREADNUM - $(basename $@)



# generate bam index
assembly.bam.bai: assembly.bam
	$(call load_modules); \
	samtools index $<


.META: assembly.sdi
	1	chromosome	The same name as the chromosome id in reference fasta file
	2	position	1-based leftmost position
	3	length	the length difference of the changed sequence against reference (0 for SNPs, negative for deletions, positive for insertions)
	4	reference base [-A-Z]+	(regular expression range)
	5	consensus base [-A-Z]+	((regular expression range), IUPAC code is used for heterozygous sites
	6	quality value	* means no value available; [0-9]+ shows the quality of this variant. It is not necessary Phred quality.
	7	percentage value	* means no value available; 0~100 shows for whether the variant is heterozygous (used for INDELs) 

# denovo call variants using denom varcall
# after margin I have to do it again
assembly.sdi: reference.fasta assembly.bam
	$(call load_modules); \
	denom varcall --outputfile $@ $< $^2
	

# limit the analysis to sequences listed in QUERY_NAME variable 
heterozygosity.tmp.seg: heterozygosity.seg
	bawk '!/^[#+,$$,ID]/ && $$2 ~ "^($(call merge,|,$(QUERY_NAME)))$$" { \   * filter for given QUERY_NAMEs *
	print $$0; }' $< >$@



# After each interval in heterozygosity.seg, coverageBed will report:
#
#     The number of reads in assembly.bam that overlapped (by at least one base pair) the heterozygosity.seg interval.
#     The number of bases in heterozygosity.seg that had non-zero coverage from reads in bam file.
#     The length of the windows in heterozygosity.seg.
#     The fraction of bases in heterozygosity.seg that had non-zero coverage from reads in bam file.
coverages.bed: assembly.bam heterozygosity.tmp.seg
	$(call load_modules); \
	coverageBed -abam $< -b <(cut -f 2,3,4 <$^2) \
	| sortBed -i stdin >$@

.META: coverages.bed
	1	chr	1
	2	start	0
	3	stop	100000
	4	number_of_reads_in_.bam_that_overlapped_the_heterozygosity.seg_interval	19
	5	number_of_bases_in_heterozygosity.seg_that_had_non-zero_coverage_from_reads_in_bam_file	35502
	6	length_of_the_windows	100000
	7	fraction_of_bases_in_heterozygosity.seg_that_had_non-zero_coverage_from_reads_in_bam	0.3550200




# tranform to .seg file format for IGV
coverages.seg: coverages.bed
	$(call load_modules); \
	cut -f 1-3,7 $< \
	| paste_value "cov_$*" \
	| sed '1s/^/\#track type="Other" name="$(basename $@)" color="50,150,255" graphType="bar" viewLimits="0.0:1.0" scaleType="linear"\nID\tchrom\tloc.start\tloc.end\tseg.mean\n/' >$@


### ScaPA ################################################################

.META: filtered.assembly.bam_placed-full.txt
	1	query_scaff
	2	query_start
	3	query_end
	4	query_span
	5	scaffold_length
	6	subj_chr
	7	chr_start
	8	chr_end
	9	chr_span
	10	query_coverage
	11	parsimony
	12	orientation
	13	SW-anchored
	14	SCORE
	15	identity
	16	glob_span
	17	seq_span
	18	gaps_cumul
	19	self.aligned
	20	alignments
	21	merged_object


# export alignments of the golden block for each placed scaffold
# in bed-12 format
filtered.assembly.bam_placed-full.txt: assembly.bam final.summary contigs.fasta
	ScaPA_alpha_r003 \
	--denom-bam $< \
	--final-summary $^2 \
	--contig-fasta $^3 \
	--dump-aln \
	--out $(firstword $(subst _, ,$@))


filtered.assembly.bam_placed.alignments.bed: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_placed-splits.txt: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_unaligned.txt: filtered.assembly.bam_placed-full.txt
	touch $@



ambiguous.placed.bed: filtered.assembly.bam_ambiguous.txt
	module purge; \
	module load lang/python/2.7; \
	cat << 'EOF' | python - >$@
	
	# to obtain floating point from division
	from __future__ import division
	from vfork.io.util import safe_rstrip
	import operator
	
	class Aln(object):
			def __init__(self, **kwargs):
				self.scaff = [ kwargs.get('scaff', '') ]
				self.scaff_start = [ kwargs.get('scaff_start', '') ]
				self.scaff_end = [ kwargs.get('scaff_end', '') ]
				self.scaff_frac = [ kwargs.get('scaff_frac', '') ]
				
				self.chr = [ kwargs.get('chr', '') ]
				self.chr_start = [ kwargs.get('chr_start', '') ]
				self.chr_end = [ kwargs.get('chr_end', '') ]
				self.chr_span = [ kwargs.get('chr_span', '') ]
				
				self.strand = [ kwargs.get('strand', '') ]
	
	def main():
			alns = {}
			scaff_len = {}
			with open('$<', 'r') as f:
				for i, l in enumerate(f):
					if i == 0:
							continue
					
					token = safe_rstrip(l).split()
					
					aln = Aln()
					# scaffold name
					aln.scaff = token[0]

					aln.scaff_start = int(token[1])
					
					aln.scaff_end = int(token[2])
					# fraction aligned. Floating point division
					aln.scaff_frac =  int(token[3]) / int(token[4])
					# chr name
					aln.chr = token[5]
					
					aln.chr_start = int(token[6])
					
					aln.chr_end = int(token[7])
					# chr span in bp
					aln.chr_span = int(token[8])
					
					aln.strand = '-' if float(token[11]) == -1 else '+'
					
					# length of the scaffold
					scaff_len[token[0]] = int(token[4])
					
					if not aln.scaff in alns:
						l = []
						alns[aln.scaff] = l
					
					alns[aln.scaff].append(aln)

			for scaff in alns.keys():
				chrs = {}
				#print k, ' '.join([a.chr for a in alns[k]])
				for a in alns[scaff]:
					if not a.chr in chrs:
						chrs[ a.chr ] = 0.0
					
					chrs[ a.chr ] += a.scaff_frac
				# return tuple of chromosomes and total span in this chromosome, sorted by total span in
				# descending order
				sorted_list = sorted(chrs.items(), key=operator.itemgetter(1), reverse=True)
				
				place = ''			
				for chr in sorted_list:
					# scaffolds result placed if they span for more
					# then 80% in a single chromosome.
					# The chromosome were they spann more is the first 
					if chr[1] >= 0.7:
						place = chr[0]
					break
				
				scaff_placed_start = []
				scaff_placed_end = []
				chr_placed_start = []
				chr_placed_end = []
				if place:
					for a in alns[scaff]:
						if a.chr == place:
							# for the scaffolds placed, get all the start and end positions
							# of the alignments for the chromosome were they should bed
							# placed
							scaff_placed_start.append(a.scaff_start)
							scaff_placed_end.append(a.scaff_end)
							chr_placed_start.append(a.chr_start)
							chr_placed_end.append(a.chr_end)
					# new spanning positions are calculated from
					# the min aln start to the max aln end
					new_chr_start = min(chr_placed_start)
					new_chr_end = max(chr_placed_end)
					new_scaff_stat = min(scaff_placed_start)
					new_scaff_end = max(scaff_placed_end)
					
					# select only the scaffolds that span to the reference <= 10 times their spannig
					if ( new_chr_end - new_chr_start ) <= 10 * ( new_scaff_end - new_scaff_stat ):
						print '\t'.join([ place, str(min(chr_placed_start)), str(max(chr_placed_end)), scaff, '8', '+', str(min(scaff_placed_start)), str(max(scaff_placed_end)), str(scaff_len[scaff]) ])
					#else:
						#print '\t'.join([ '*', place, str(min(chr_placed_start)), str(max(chr_placed_end)), str(new_chr_end - new_chr_start), scaff, str(min(scaff_placed_start)), str(max(scaff_placed_end)), str(scaff_len[scaff]) ])
			return 0
	
	if __name__ == '__main__':
			main()
	
	EOF





# tranform alignments to bed12 format
assembly.bamToBed.bed: assembly.bam
	$(call load_modules); \
	bamToBed \
	-color "255,0,0" \
	-cigar \
	-split \
	-bed12 \
	-i $< \
	>$@

# contains placed scaffolds. 
# The coordinates corresponds to the blocks that anchor scaffolds to the reference
assembly.placed-full.bed: filtered.assembly.bam_placed-full.txt
	$(call load_modules); \
	unhead $< \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@

# contains placed scaffolds as assembly.bed
# In addition to the scaffolds placed in a clean way, 
# it also contains the scaffolds placed after the split operation.
assembly.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@

# same as above but add ambiguous
assembly2.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt filtered.assembly.bam_ambiguous.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) <(unhead $^3) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@



# contains contigs.
# The coordinates corresponds to the alignments that anchor contigs to the reference
# only for placed scaffolds (not splitted)
# ScaPA_alpha_r002 now produce bed file without conversion
assembly.aligns.bed: filtered.assembly.bam_placed.alignments.bed
	$(call load_modules); \
	sortBed -i $< >$@

### ScaPA ################################################################

### unaligned
unaligned.fasta: filtered.assembly.bam_unaligned.txt scaffolds.fasta
	$(call load_modules);
	(
	cat $< \
	| xargs samtools faidx $^2
	) >$@
	

#############


scatterplot.heterozygosity.Vs.contigs.coverage.pdf: heterozygosity.tmp.seg coverages.seg
	paste \
	<(cat $< \
	| cut -f 5) \
	<(cat $^2 \
	| tail -n +3 \
	| cut -f 5) \
	| scatterplot \
	--no-header \
	--title="$(SAMPLE_NAME) heterozygosity Vs coverage" \
	--xlab="heterozygosity" \
	--ylab="coverage" \
	--output-file=$@ \
	1,2


# use this_stratified_boxplot instead of stratified_boxplot because
# a modification was needed when process PN40024
stratified_boxplot.heterozygosity.Vs.contigs.coverage.pdf: heterozygosity.tmp.seg coverages.seg
	paste \
	<(cat $^2 \
	| tail -n +3 \
	| cut -f 5) \
	<(cat $< \
	| cut -f 5) \
	| this_stratified_boxplot \
	--no-header \
	--num-bins=10 \
	--output-file=$@ \
	--title="$(SAMPLE_NAME) coverage distribution for layers of heterozygosity" \
	--xlabel="heterozygosity layers" \
	--ylabel="coverage" \
	1 2

.META: heterozygosity.Vs.contigs.length.bed
	1	chr	1
	2	start	0
	3	stop	100000
	6	mean heterozygosity in interval	0.00495378
	8	median of contigs length that align on the interval	4025
	9	median of portion aligned of contigs	1096.5

heterozygosity.Vs.contigs.length.bed: scaffolds.fasta assembly.bed heterozygosity.tmp.seg
	$(call load_modules); \
	translate -a <(fasta_length <$<) 4 <$^2 \
	| select_columns 1 2 3 4 5 12 \
	| sortBed -i stdin \
	| bedtools intersect \
	-bed \   * write output as BED, not bam *
	-loj \
	-a <(tail -n +3 <$^3 | select_columns 2 3 4 5) \
	-b stdin \
	| mergeBed -i stdin -d -1 -c "4,9,10" -o "distinct,median,median" >$@



scatterplot.heterozygosity.Vs.contigs.length.pdf: heterozygosity.Vs.contigs.length.bed
	scatterplot \
	--no-header \
	--title="$(SAMPLE_NAME) heterozygosity Vs contig length" \
	--xlab="heterozygosity" \
	--ylab="contig length" \
	--output-file=$@ \
	4,5 <$<


#####################################################################################


# bed file  that defines homozygous and heterozygous regions:
.META: custom_het_regions.bed
	1	chromosome	chr1
	2	start	0
	3	end	9000000
	4	comment (het homo)	het

custom_het_regions.bed:
	ln -sf $(CUSTOM_HET_REGIONS_BED) $@

custom_homo_regions.bed:
	ln -sf $(CUSTOM_HOMO_REGIONS_BED) $@

custom_all_regions.bed: custom_het_regions.bed custom_homo_regions.bed
	cat $^ \
	| bsort -k 1,1 -k2,2n >$@


.META: custom.%.bed
	1	chr	1
	2	start	0
	3	stop	100000
	4	min heterozygosity in interval	0.00045
	5	max heterozygosity in interval	0.01079
	6	mean heterozygosity in interval	0.00495378
	7	median heterozygosity in interval	0.00476

custom.%.bed: heterozygosity.seg custom_%_regions.bed
	$(call load_modules); \
	bawk '!/^[#+,$$,ID]/ && $$2 ~ "^($(call merge,|,$(QUERY_NAME)))$$" { \   * filter for given QUERY_NAMEs *
	print $$2,$$3,$$4,$$5; }' $< \
	| bedtools intersect \
	-bed \   * write output as BED, not bam *
	-loj \
	-a <(cut -f -3 <$^2) \
	-b stdin \
	| mergeBed -i stdin -d -1 -c 7 -o "min,max,mean,median" >$@


custom.%.coverages.bed: assembly.bed custom.%.bed
	$(call load_modules); \
	coverageBed -a $< -b $^2 \
	| sortBed -i stdin >$@

.META: custom.%.coverages.seg
	1	track name
	2	chr	1
	3	start	0
	4	stop	100000
	8	median heterozygosity in interval	0.00476

custom.%.coverages.seg: custom.%.coverages.bed
	$(call load_modules); \
	cut -f 1-3,11 $< \
	| paste_value "cov" \
	| sed '1s/^/\#track type="Other" name="$(basename $@)" color="50,150,255" graphType="bar" viewLimits="0.0:1.0" scaleType="linear"\nID\tchrom\tloc.start\tloc.end\tseg.mean\n/' >$@





.META: custom.%.Vs.contigs.length.bed
	1	chr	1
	2	start	0
	3	stop	100000
	4	min heterozygosity in interval	0.00045
	5	max heterozygosity in interval	0.01079
	6	mean heterozygosity in interval	0.00495378
	7	median heterozygosity in interval	0.00476
	8	median of contigs length that align on the interval	4025
	9	median of portion aligned of contigs	1096.5

custom.%.Vs.contigs.length.bed: scaffolds.fasta assembly.bed custom.%.bed
	$(call load_modules); \
	translate -a <(fasta_length <$<) 4 <$^2 \
	| select_columns 1 2 3 4 5 12 \
	| sortBed -i stdin \
	| bedtools intersect \
	-bed \   * write output as BED, not bam *
	-loj \
	-a $^3 \
	-b stdin \
	| mergeBed -i stdin -d -1 -c "4,5,6,12,13" -o "distinct,distinct,distinct,median,median" >$@




scatterplot.custom.all.Vs.contigs.aln.length.pdf: custom.het.Vs.contigs.length.bed custom.homo.Vs.contigs.length.bed
	paste <(select_columns 6 8 <$<) \
	<(select_columns 6 8 <$^2) \
	| bawk '!/^[\#+,$$]/ { print $$1,$$2,$$3,$$4; }' \
	| scatterplot \
	--remove-na \
	--no-header \
	--title="$(SAMPLE_NAME) heterozygosity Vs length of the alignment of contigs" \
	--xlab="heterozygosity" \
	--ylab="length contig alignments" \
	--xlim="0:0.008" \
	--ylim="0:2000" \
	--pch="1,2" \
	--color="blue,red" \
	--output-file=$@ \
	1,2 3,4


scatterplot.custom.all.Vs.contigs.length.pdf: custom.het.Vs.contigs.length.bed custom.homo.Vs.contigs.length.bed
	paste <(select_columns 6 7 <$<) \
	<(select_columns 6 7 <$^2) \
	| bawk '!/^[\#+,$$]/ { print $$1,$$2,$$3,$$4; }' \
	| scatterplot \
	--remove-na \
	--no-header \
	--title="$(SAMPLE_NAME) heterozygosity Vs contig length" \
	--xlab="heterozygosity" \
	--ylab="length" \
	--pch="1,2" \
	--xlim="0:0.008" \
	--ylim="1000:8000" \
	--color="blue,red" \
	--output-file=$@ \
	1,2 3,4


scatterplot.custom.all.Vs.coverage.pdf: custom.het.bed custom.het.coverages.seg custom.homo.bed custom.homo.coverages.seg
	paste \
	<(cat $< \
	| cut -f 7) \
	<(cat $^2 \
	| tail -n +3 \
	| cut -f 5) \
	<(cat $^3 \
	| cut -f 7) \
	<(cat $^4 \
	| tail -n +3 \
	| cut -f 5) \
	| bawk '!/^[\#+,$$]/ { print $$1,$$2,$$3,$$4; }' \
	| scatterplot \
	--remove-na \
	--no-header \
	--title="$(SAMPLE_NAME) heterozygosity Vs coverage" \
	--xlab="heterozygosity" \
	--ylab="coverage" \
	--pch="1,2" \
	--output-file=$@ \
	--xlim="0:0.008" \
	--color="blue,red" \
	3,4 1,2

# stratified boxplot makes sense only for all regions
stratified_boxplot.custom.all.Vs.coverage.pdf: custom.all.bed custom.all.coverages.seg
	paste \
	<(cat $^2 \
	| tail -n +3 \
	| bsort --key 2,2 \
	| cut -f 5) \
	<(cat $< \
	| bsort --key 1,1 \
	| cut -f 7) \
	| this_stratified_boxplot \
	--no-header \
	--num-bins=5 \
	--output-file=$@ \
	--title="$(SAMPLE_NAME) coverage distribution for layers of heterozygosity" \
	--xlabel="heterozygosity layers" \
	--ylabel="coverage" \
	1 2

boxplot.custom.all.coverage.pdf: custom.het.coverages.seg custom.homo.coverages.seg
	paste <(tail -n +3 $< \
	| cut -f5 \
	| sed '1i heterozygous') \
	<(tail -n +3 $^2 \
	| cut -f5 | sed '1i homozygous') \
	| boxplot -n -o $@


boxplot.custom.all.contig.length.pdf: custom.het.Vs.contigs.length.bed custom.homo.Vs.contigs.length.bed
	paste <( cut -f 7 $< | sed '1i heterozygous') \
	<(cut -f 7 $^2 | sed '1i homozygous') \
	| boxplot -n -o $@

boxplot.custom.all.contig.aln.length.pdf: custom.het.Vs.contigs.length.bed custom.homo.Vs.contigs.length.bed
	paste <( cut -f 8 $< | sed '1i heterozygous') \
	<(cut -f 8 $^2 | sed '1i homozygous') \
	| boxplot -n -o $@


.PHONY: test
test:
	@echo 


ALL += contigs.fasta \
	assembly.bam \
	assembly.bam.bai \
	assembly.sdi \
	coverages.seg \
	\
	assembly.bed \
	assembly.placed-full.bed \
	assembly.aligns.bed \
	\
	scatterplot.heterozygosity.Vs.contigs.coverage.pdf \
	stratified_boxplot.heterozygosity.Vs.contigs.coverage.pdf \
	heterozygosity.Vs.contigs.length.bed \
	scatterplot.heterozygosity.Vs.contigs.length.pdf \
	\
	custom.het.bed \
	custom.het.coverages.seg \
	custom.het.Vs.contigs.length.bed \
	\
	custom.homo.bed \
	custom.homo.coverages.seg \
	custom.homo.Vs.contigs.length.bed \
	\
	custom.all.coverages.seg \
	scatterplot.custom.all.Vs.coverage.pdf \
	stratified_boxplot.custom.all.Vs.coverage.pdf \
	custom.all.Vs.contigs.length.bed \
	scatterplot.custom.all.Vs.contigs.length.pdf \
	scatterplot.custom.all.Vs.contigs.aln.length.pdf \
	\
	boxplot.custom.all.coverage.pdf \
	boxplot.custom.all.contig.length.pdf \
	boxplot.custom.all.contig.aln.length.pdf


INTERMEDIATE += reference.fasta \
		$(FASTA_BAMS) \
		$(FASTA_CHUNCKS)

CLEAN += chunks.mk \
	 $(wildcard reference.*) \
	 $(shell seq 8)
