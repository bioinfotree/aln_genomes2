# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# heterozygosity Vs coverage of contigs on the reference

# TODO:

context prj/kmer_count2

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

MIN_REF_LEN ?= 50000

# log dir
log:
	mkdir -p $@

contigs.fasta:
	fasta2tab <$(CONTIGS) \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }' \
	| bsort \
	| tab2fasta 2 \
	| resolve_iupac --bed-file=contigs.sub.bed >$@

# save list of chunks to make var
chunks.mk:
	ARRAY=( $$(seq -s " " $(SPLIT)) ); \
	echo "FASTA_CHUNCKS := $${ARRAY[@]/#/contigs.fasta.}" >$@

include chunks.mk

# split fasta
contigs.fasta.%: contigs.fasta
	$(call load_modules); \
	if [ "$*" == "1" ]; then \ 
		gt splitfasta -numfiles $(SPLIT) -force yes $<; \
	else \
		sleep 5; \   * wait untill all pieces are generated *
	fi


scaffolds.fasta:
	ln -sf $(SCAFFOLDS) $@

heterozygosity.seg:
	ln -sf $(HETEROZYGOSITY_SEG) $@

final.summary:
	ln -sf $(FINAL_SUMMARY) $@

# denom can't handle to short references
reference.fasta:
	zcat $(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( $$1 ~ /Pt|Mt|chr[0-9]+/ ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 \
	| resolve_iupac --bed-file=reference.sub.bed >$@


# execute denom for every chunks
# it runs in succession the indexing and alignment. 
# In oreder to avoid to rebuild indexs for each chunk, I wait for it to finish indexing and allignment of chuck 1.
# For chuncks >1, I create links for indexs in directory 1 to their respective directories. I run denom easyrun.
# It finds the index files and proceeds whith the alignment, that can be executed in parallel for all the chunk> 1.
FASTA_BAMS = $(addsuffix .bam,$(addprefix assembly.,$(shell seq $(SPLIT))))
assembly.%.bam: reference.fasta contigs.fasta.% log
	$(call load_modules); \
	mkdir -p $*; \
	if [ "$*" == "1" ]; then \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	else \
		until [ -s 1/assembly.1.bam ]; do \   * wait for assembly.1.bam to be ready *
			sleep 15; \
			echo "process for chunk $* is spleeping 15 seconds.."; \
		done; \
		cd $*; \
		ln -sf ../$< .; \
		ln -sf ../$^2 .; \
		ln -sf ../1/$<.amb .; \
		ln -sf ../1/$<.ann .; \
		ln -sf ../1/$<.bwt .; \
		ln -sf ../1/$<.pac .; \
		ln -sf ../1/$<.fai .; \
		ln -sf ../1/$<.sa .; \
		/usr/bin/time -v denom easyrun $< $^2 $@ $(basename $@).sdi 2>$^3/denom-easyrun.$@.log \
		&& cd ..; \
		ln -sf $*/$@ .; \
	fi


# merge and sort the bams
assembly.bam: $(FASTA_BAMS)
	!threads
	$(call load_modules); \
	samtools merge -f \
	- $^ \
	| samtools sort -@ $$THREADNUM - $(basename $@)



# generate bam index
assembly.bam.bai: assembly.bam
	$(call load_modules); \
	samtools index $<


.PHONY: test
test:
	@echo 


ALL += contigs.fasta \
	assembly.bam

INTERMEDIATE += 

CLEAN += 