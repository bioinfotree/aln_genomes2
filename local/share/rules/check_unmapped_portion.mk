# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

# Perform analysis of gap that remains between placed scaffolds scaffolds

.ONESHELL:

genome.tab:
	fasta_length </mnt/vol1/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta >$@

# retrieve 
uncovered.bed: ../phase_1/assembly.bed genome.tab
	$(call load_modules); \
	bedtools complement -i $< -g $^2 >$@

# caluclate some statistics
uncovered.stats: uncovered.bed
	$(call load_modules); \
	cat <<'EOF' | bRscript - >$@
	options(width=10000)
	setwd("$(PWD)")
	# return general statistics
	library(Hmisc)
	
	uncov = bread(file="$<", header=F, check.names=T)
	colnames(uncov) = c("chr","start","end") 	
	uncov$$length = as.numeric(uncov$$end) - as.numeric(uncov$$start)

	describe(uncov$$length)
	
	summary(uncov$$length)
	
	sum(uncov$$length)
	
	pdf("uncovered.pdf")
	
	hist(uncov$$length, breaks=(seq(0, 500000, 20000)), xaxt="n",
		# remove X axis label
		xlab="",
		# histogram title
		main="Distribution of genomic portions not covered by scaffolds", sub="full, split, abiguous resuced")
	
	# add x axes to the histogram
	axis(1, at=seq(0 , 500000, by=20000), lwd=0, lwd.ticks=1, las=3, cex.axis=0.7)
	
	dev.off()
	
	EOF

# get the length of the ambiguous scaffolds that remains
# after second placeing
ambiguous.remaining.length: ../phase_1/filtered.assembly.bam_ambiguous.txt ../phase_1/ambiguous.placed.bed ../phase_1/scaffolds.fasta
	$(call load_modules); \
	unhead $< \
	| cut -f1 \
	| bsort \
	| uniq \
	| filter_1col -v 1 <(cut -f4 $^2 | bsort | uniq) \
	| xargs samtools faidx $^3 \
	| fasta_length >$@

# calculate statistics of that scaffolds
ambiguous.remaining.stats: ambiguous.remaining.length
	cat <<'EOF' | bRscript - >$@
	options(width=10000)
	setwd("$(PWD)")
	# return general statistics
	library(Hmisc)
	
	unplaced = bread(file="$<", header=F, check.names=T)
	colnames(unplaced) = c("scaff","length") 	

	describe(unplaced$$length)
	
	summary(unplaced$$length)
	
	sum(unplaced$$length)
	
	pdf("unplaced.pdf")
	
	hist(unplaced$$length, 
		xaxt="n",
		# remove X axis label
		xlab="",
		breaks=(seq(0, 1400000, 20000)),
		# histogram title
		main="Distribution of unplaced scaffold lengths", sub="abiguous")
	
	# add x axes to the histogram
	axis(1, at=seq(0 , 1400000, by=20000), lwd=0, lwd.ticks=1, las=3, cex.axis=0.5)
	
	dev.off()
	
	EOF

# select gaps >=1kbp
uncovered.1kb.fasta: uncovered.bed /mnt/vol1/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta
	$(call load_modules); \
	bawk '{ if ( $$3-$$2 >= 1000 ) print $$1":"$$2"-"$$3 }' $< \
	| xargs samtools faidx $^2 \
	| fasta2tab \
	| bawk '{ split($$1,a,":|-"); \
			seq=$$2; \   * make a copy that will be manipulated by gsub *
			if ( gsub("N|n","",seq) < (length(seq) * 0.8) ) print $$1,$$2 \
	}' \
	| bsort \
	| tab2fasta 2 >$@

# get fastas of the ambiguous that remains
ambiguous.remaining.fasta: ../phase_1/filtered.assembly.bam_ambiguous.txt ../phase_1/ambiguous.placed.bed ../phase_1/scaffolds.fasta
	$(call load_modules); \
	unhead $< \
	| cut -f1 \
	| bsort \
	| uniq \
	| filter_1col -v 1 <(cut -f4 $^2 | bsort | uniq) \
	| xargs samtools faidx $^3 >$@

# map gaps to the ambiguous contigs
uncovered.1kb.coords: uncovered.1kb.fasta ambiguous.remaining.fasta
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load lang/r/3.3; \
	module load compilers/gcc/4.9.3; \
	module load sw/aligners/mummer/3.23; \
	/usr/bin/time -v nucmer --maxmatch -l 30 -c 65 --banded -p $(basename $@) -D 5 $< $^2  2>nucmer.$@.log \
	&& delta-filter -o 90 -i 90 $(basename $@).delta >$(basename $@).fdelta \
	&& show-coords -THbr $(basename $@).fdelta >$@

# return coordinates
uncovered.1kb.aln.bed: uncovered.1kb.coords
	cat $^ \
	| bawk '!/^[\#+,$$]/ { \
	ref_start=$$1; \
	ref_end=$$2; \
	query_start=$$3; \
	query_end=$$4; \
	ref_len=$$5; \
	query_len=$$6; \
	split($$7,a,":"); \
	ref=$$7; \
	query=$$8; \
	print a[1], ref_start-1, ref_end, query, query_start-1, query_end }' \
	| bsort -k 1,1 -k2,2n >$@


# prepare chain file: from chr span to windows span
chain: genome.tab uncovered.1kb.fasta
	fasta2tab <$^2 \
	| cut -f1 \
	| tr ':-' \\t \
	| translate -a $< 1 \
	| bawk 'BEGIN { N=0; } \
	!/^[\#+,$$]/ { \
	N++; \
	len=$$4-$$3;
	print "chain 10 "$$1" "$$2" + "$$3" "$$4" "$$1" "len" + "0" "len" "N; \
	print len; \
	print ""; \
	} ' >$@

# referse the chain
rew.chain: chain
	module purge; \
	module load tools/ucsc-utils/16-Dec-2013; \
	chainSwap $< $@

# lift over mapping coordinates with respect to all chromosomes
uncovered.1kb.aln.lift.bed: rew.chain uncovered.1kb.aln.bed
	module purge; \
	module load tools/crossmap/0.2.5; \
	CrossMap.py bed $< $^2 $@

# sort
uncovered.1kb.aln.lift.sort.bed: uncovered.1kb.aln.lift.bed
	$(call load_modules); \
	sortBed -i $< >$@


ALL += uncovered.1kb.aln.lift.sort.bed uncovered.1kb.coords

CLEAN += ambiguous.remaining.fasta \
ambiguous.remaining.length \
ambiguous.remaining.stats \
chain \
genome.tab \
nucmer.uncovered.coords.log \
rew.chain \
uncovered.1kb.aln.bed \
uncovered.1kb.fasta \
uncovered.bed \
uncovered.delta \
uncovered.fdelta \
uncovered.stats