# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

# Perform analysis of ambiguous scaffolds covered by reads of the reference and reads of the sample

.ONESHELL:

scaffolds.bed:
	ln -sf ../../$(SAMPLE)/phase_6/$@

scaffolds.tab:	scaffolds.fasta
	$(call load_modules); \
	fasta_length <$< >$@

contigs.tab: contigs.fasta
	$(call load_modules); \
	fasta_length <$< >$@


# select ambiguous contigs from all contigs in the assembly
ambiguous.contigs: ../phase_1/filtered.assembly.bam_ambiguous.txt scaffolds.bed
	$(call load_modules); \
	filter_1col 1 <(\
	unhead $< \
	| cut -f1 \
	| bsort \
	| uniq) <$^2 \
	| bawk '{ if ( $$4 ~ /contig_/ ) print $$0 }' >$@

# select unaligned contigs from all contigs in the assembly
unaligned.contigs: ../phase_1/filtered.assembly.bam_unaligned.txt scaffolds.bed
	$(call load_modules); \
	filter_1col 1 <(\
	bsort <$< \
	| uniq) <$^2 \
	| bawk '{ if ( $$4 ~ /contig_/ ) print $$0 }' >$@	


# remove summary present in the last part of the text file
# after a blank line
pn.per.reference.coverage:
	sed -e '/^$$/,$$d' <../../../../aln_ref/dataset/vitis_vinifera/rkatsiteli4_contigs_pn_paired-end/phase_5/exp_317_PN40024_GCCAAT_L001.per_reference_coverage_with_unmapped.txt >$@
	
rk_4.per.reference.coverage:
	sed -e '/^$$/,$$d' <../../../../aln_ref/dataset/vitis_vinifera/rkatsiteli4_contigs_paired-end/phase_5/exp_193_rkatsiteli_Undetermined_L005.per_reference_coverage_with_unmapped.txt >$@



# ambiguous contigs
ambiguous.contigs.pn.vs.sample.cov: pn.per.reference.coverage rk_4.per.reference.coverage ambiguous.contigs scaffolds.tab ../../../../kmer_count2/dataset/rkatsiteli4_contigs/phase_3/ref.noIUPAC.fasta.k19-stats.csv
	module purge;
	module load lang/r/3.3;
	cat <<'EOF' | bRscript -
	options(width=10000)
	setwd("$(PWD)")
	
	# calculate the mode of the distribution
	mode = function(x, na.rm = FALSE) {
	if(na.rm){
		x = x[!is.na(x)]
		}
	ux = unique(x)
	return(ux[which.max(tabulate(match(x, ux)))])
	}
	
	# simple printf solution
	printf = function(...) cat(sprintf(...))
	
	pn_cov = bread(file="$<", header=T, check.names=F)
	sample_cov = bread(file="$^2", header=T, check.names=F)
	contigs = bread(file="$^3", header=F, check.names=F)
	scaff_len = bread(file="$^4", header=F, check.names=F)
	sample_contigs_kmer_cov = bread(file="$^5", header=T, check.names=F)
	
	cnames = c("seq_name", "median", "%_non_zero_corrected")
	
	sample_contigs_kmer_cov = subset(sample_contigs_kmer_cov, select=cnames)
	
	colnames(sample_contigs_kmer_cov) = c("seq_name", "sample_median_kmer_cov", "sample_%_non_zero_corrected_kmer_cov")
	
	# calculate mode of the kmer coverage to understand what to expect as single copy genome component
	no_zero_sm = sample_cov[ which(sample_contigs_kmer_cov$$sample_median_kmer_cov > 0), ]
	sm_mode = mode(sample_contigs_kmer_cov$$sample_median_kmer_cov, na.rm=T)
	
	printf("\n")
	printf("mode of the median kmer coverage distribution of sample contigs by kmers in all the contigs:\t%i\n", sm_mode)
	#######
	
	cnames = c("scaffold", "scaffold_length")
	
	colnames(scaff_len) = cnames
	
	cnames = c("scaffold", "start", "end", "contig", "contig_lenght")
	
	colnames(contigs) = cnames
	
	merged = subset( merge( contigs, pn_cov, by.x="contig", by.y="Chromosome" ), select=c( cnames, "Avg_Cov", "Median_Cov" ))
	
	cnames = c( cnames, "pn_average_coverage", "pn_median_coverage" )
	
	colnames(merged) = cnames
	
	merged = subset( merge( merged, sample_cov, by.x="contig", by.y="Chromosome" ), select=c( cnames, "Avg_Cov", "Median_Cov" ))
	
	cnames = c( cnames, "sample_average_coverage", "sample_median_coverage" )
	
	colnames(merged) = cnames
	# add scaff length
	merged = merge( merged, scaff_len, by="scaffold" )
	
	# reorder by column position
	merged = merged[c(1,10,2,3,4,5,6,7,8,9)]
	# calculate number of contigs in each scaffold
	n_contig =  as.data.frame(table(merged$$scaffold))
	colnames(n_contig) = c("scaffold", "contig_number")
	
	# add contigs number for each scaffold
	merged = merge( merged, n_contig, by="scaffold" )
	
	merged = merged[c(1,2,11,3,4,5,6,7,8,9,10)]
	
	# sort by scaffold_length in descending order, then by start in ascending order
	merged = merged[order( -merged$$scaffold_length, merged$$start ), ]
	

	

	# Select contigs median distribution values usefull tu calculate the mode of
	# the coverage distribution of the contigs in the sample.
	# Since the sample is heterozygous it have two peaks: het and homo.
	# Here I select values beyond the heterozygous peak
	no_zero_sm = sample_cov[ which(sample_cov$$Median_Cov > 50), ]
	# The same as above but here I select values beyond the low coverage contigs
	no_zero_pn = pn_cov[ which(pn_cov$$Median_Cov > 10), ]
	
	# Here I infer the mode of the distribution of the median contigs coverage
	sample_mode = mode(no_zero_sm$$Median_Cov, na.rm=T)
	pn_mode = mode(no_zero_pn$$Median_Cov, na.rm=T)
	
	printf("\n")
	printf("mode of the median coverage distribution of contigs by sample reads:\t%i\n", sample_mode)
	printf("mode of the median coverage distribution of contigs by pn reads:\t%i\n", pn_mode)
	
	# normalize the median coverage by the mode of the median converage distribution
	# moltiply for 100 because humas are better in reading integer numbers
	merged$$pn_median_norm_coverage = round(merged$$pn_median_coverage / pn_mode * 100, 0)
	merged$$sample_median_norm_coverage = round(merged$$sample_median_coverage / sample_mode * 100, 0)
		
		pdf("$(basename $@).pdf")
		
		# normalize the media coverage by the mode of the median values
		out = data.frame(rbind(cbind(merged$$pn_median_coverage / pn_mode ,1),cbind(merged$$sample_median_coverage / sample_mode ,2)))
		
		colnames(out) = c("median_coverage","group")
		
		# generate boxplot
		boxplot(out$$median_coverage ~ out$$group, names=c("pn median coverage","sample median coverage"),
				main="Normalized coverage of the ambiguous contigs", 
				xlab="", ylab="normalized median coverage distribution",
				outline=F,
				yaxt='n',
				col=terrain.colors(4))
		
		# add y axess with more frequent intervals
		axis(2, at=seq(0, 2, 0.25))
		
		# prevent the number and name of the new active device to be returned/printed
		invisible(dev.off())
	
	# remove "pn_median_coverage", "sample_median_coverage" columns
	merged$$sample_average_coverage = NULL
	merged$$pn_average_coverage = NULL
	merged$$sample_median_coverage = NULL
	merged$$pn_median_coverage = NULL
	
	# merge kmer coverage data from reads of the sample and the reference
	merged = merge(merged, sample_contigs_kmer_cov, by.x="contig", by.y="seq_name")
	# sort again
	merged = merged[order( -merged$$scaffold_length, merged$$start ), ]
	# reorder columns
	merged = merged[c(2,3,4,1,5,6,7,8,9,10,11)]
	
			pdf("ambiguous.correlation.kmer.coverage.pdf")
			xlimit = 500
			ylimit = 200
			# plot(merged$$sample_median_norm_coverage, merged$$sample_median_kmer_cov, main="Scatterplot sample median norm coverage vs median kmer cov", 
						  # xlab="sample median norm coverage", ylab="sample median kmer cov", pch=19, xlim=c(0,2000))
			plot(merged$$sample_median_norm_coverage, merged$$sample_median_kmer_cov, 
						main="Scatterplot sample median norm coverage vs median kmer cov\n of ambiguous contigs", 
						xlab="median norm coverage", ylab="assembly median kmer cov", pch=4, 
						xlim=c(0,xlimit),ylim=c(-ylimit,ylimit),cex=0.3)
						  
			points(merged$$pn_median_norm_coverage, -merged$$sample_median_kmer_cov, pch="*", col="red",cex=0.3)
			
			legend("topright",col=c("black","red"),pch=c(4,8),legend=c("sample median coverage","pn median coverage"),ncol=1)
			
			invisible(dev.off())
	
	bwrite(merged, file="$@", col.names=T, row.names=F)
	
	EOF


ambiguous.contigs.pn.vs.sample.pdf ambiguous.correlation.pdf: unaligned.contigs.pn.vs.sample.cov
	touch $@







# unaligned contigs
unaligned.contigs.pn.vs.sample.cov: pn.per.reference.coverage rk_4.per.reference.coverage unaligned.contigs scaffolds.tab ../../../../kmer_count2/dataset/rkatsiteli4_contigs/phase_3/ref.noIUPAC.fasta.k19-stats.csv
	module purge;
	module load lang/r/3.3;
	cat <<'EOF' | bRscript -
	options(width=10000)
	setwd("$(PWD)")
	
	# calculate the mode of the distribution
	mode = function(x, na.rm = FALSE) {
	if(na.rm){
		x = x[!is.na(x)]
		}
	ux = unique(x)
	return(ux[which.max(tabulate(match(x, ux)))])
	}
	
	# simple printf solution
	printf = function(...) cat(sprintf(...))
	
	pn_cov = bread(file="$<", header=T, check.names=F)
	sample_cov = bread(file="$^2", header=T, check.names=F)
	contigs = bread(file="$^3", header=F, check.names=F)
	scaff_len = bread(file="$^4", header=F, check.names=F)
	sample_contigs_kmer_cov = bread(file="$^5", header=T, check.names=F)
	
	cnames = c("seq_name", "median", "%_non_zero_corrected")
	
	sample_contigs_kmer_cov = subset(sample_contigs_kmer_cov, select=cnames)
	
	colnames(sample_contigs_kmer_cov) = c("seq_name", "sample_median_kmer_cov", "sample_%_non_zero_corrected_kmer_cov")
	
	# calculate mode of the kmer coverage to understand what to expect as single copy genome component
	no_zero_sm = sample_cov[ which(sample_contigs_kmer_cov$$sample_median_kmer_cov > 0), ]
	sm_mode = mode(sample_contigs_kmer_cov$$sample_median_kmer_cov, na.rm=T)
	
	printf("\n")
	printf("mode of the median kmer coverage distribution of sample contigs by kmers in all the contigs:\t%i\n", sm_mode)
	#######
	
	cnames = c("scaffold", "scaffold_length")
	
	colnames(scaff_len) = cnames
	
	cnames = c("scaffold", "start", "end", "contig", "contig_lenght")
	
	colnames(contigs) = cnames
	
	merged = subset( merge( contigs, pn_cov, by.x="contig", by.y="Chromosome" ), select=c( cnames, "Avg_Cov", "Median_Cov" ))
	
	cnames = c( cnames, "pn_average_coverage", "pn_median_coverage" )
	
	colnames(merged) = cnames
	
	merged = subset( merge( merged, sample_cov, by.x="contig", by.y="Chromosome" ), select=c( cnames, "Avg_Cov", "Median_Cov" ))
	
	cnames = c( cnames, "sample_average_coverage", "sample_median_coverage" )
	
	colnames(merged) = cnames
	# add scaff length
	merged = merge( merged, scaff_len, by="scaffold" )
	
	# reorder by column position
	merged = merged[c(1,10,2,3,4,5,6,7,8,9)]
	# calculate number of contigs in each scaffold
	n_contig =  as.data.frame(table(merged$$scaffold))
	colnames(n_contig) = c("scaffold", "contig_number")
	
	# add contigs number for each scaffold
	merged = merge( merged, n_contig, by="scaffold" )
	
	merged = merged[c(1,2,11,3,4,5,6,7,8,9,10)]
	
	# sort by scaffold_length in descending order, then by start in ascending order
	merged = merged[order( -merged$$scaffold_length, merged$$start ), ]
	

	

	# Select contigs median distribution values usefull tu calculate the mode of
	# the coverage distribution of the contigs in the sample.
	# Since the sample is heterozygous it have two peaks: het and homo.
	# Here I select values beyond the heterozygous peak
	no_zero_sm = sample_cov[ which(sample_cov$$Median_Cov > 50), ]
	# The same as above but here I select values beyond the low coverage contigs
	no_zero_pn = pn_cov[ which(pn_cov$$Median_Cov > 10), ]
	
	# Here I infer the mode of the distribution of the median contigs coverage
	sample_mode = mode(no_zero_sm$$Median_Cov, na.rm=T)
	pn_mode = mode(no_zero_pn$$Median_Cov, na.rm=T)
	
	printf("\n")
	printf("mode of the median coverage distribution of contigs by sample reads:\t%i\n", sample_mode)
	printf("mode of the median coverage distribution of contigs by pn reads:\t%i\n", pn_mode)
	
	# normalize the median coverage by the mode of the median converage distribution
	# moltiply for 100 because humas are better in reading integer numbers
	merged$$pn_median_norm_coverage = round(merged$$pn_median_coverage / pn_mode * 100, 0)
	merged$$sample_median_norm_coverage = round(merged$$sample_median_coverage / sample_mode * 100, 0)
		
		pdf("$(basename $@).pdf")
		
		# normalize the media coverage by the mode of the median values
		out = data.frame(rbind(cbind(merged$$pn_median_coverage / pn_mode ,1),cbind(merged$$sample_median_coverage / sample_mode ,2)))
		
		colnames(out) = c("median_coverage","group")
		
		# generate boxplot
		boxplot(out$$median_coverage ~ out$$group, names=c("pn median coverage","sample median coverage"),
				main="Normalized coverage of the ambiguous contigs", 
				xlab="", ylab="normalized median coverage distribution",
				outline=F,
				yaxt='n',
				col=terrain.colors(4))
		
		# add y axess with more frequent intervals
		axis(2, at=seq(0, 2, 0.25))
		
		# prevent the number and name of the new active device to be returned/printed
		invisible(dev.off())
	
	# remove "pn_median_coverage", "sample_median_coverage" columns
	merged$$sample_average_coverage = NULL
	merged$$pn_average_coverage = NULL
	merged$$sample_median_coverage = NULL
	merged$$pn_median_coverage = NULL
	
	# merge kmer coverage data from reads of the sample and the reference
	merged = merge(merged, sample_contigs_kmer_cov, by.x="contig", by.y="seq_name")
	# sort again
	merged = merged[order( -merged$$scaffold_length, merged$$start ), ]
	# reorder columns
	merged = merged[c(2,3,4,1,5,6,7,8,9,10,11)]
	
			pdf("unaligned.correlation.kmer.coverage.pdf")
			xlimit = 500
			ylimit = 50
			# plot(merged$$sample_median_norm_coverage, merged$$sample_median_kmer_cov, main="Scatterplot sample median norm coverage vs median kmer cov", 
						  # xlab="sample median norm coverage", ylab="sample median kmer cov", pch=19, xlim=c(0,2000))
			plot(merged$$sample_median_norm_coverage, merged$$sample_median_kmer_cov, 
						main="Scatterplot sample median norm coverage vs median kmer cov\n of unaligned contigs", 
						xlab="median norm coverage", ylab="assembly median kmer cov", pch=4, 
						xlim=c(0,xlimit),ylim=c(-ylimit,ylimit),cex=0.3)
						  
			points(merged$$pn_median_norm_coverage, -merged$$sample_median_kmer_cov, pch="*", col="red",cex=0.3)
			
			legend("topright",col=c("black","red"),pch=c(4,8),legend=c("sample median coverage","pn median coverage"),ncol=1)
			
			invisible(dev.off())
	
	bwrite(merged, file="$@", col.names=T, row.names=F)
	
	EOF


unaligned.contigs.pn.vs.sample.pdf unaligned.correlation.kmer.coverage.pdf: unaligned.contigs.pn.vs.sample.cov
	touch $@




ambiguous.contigs.fasta: ambiguous.contigs contigs.fasta
	$(call load_modules);
	(
	cut -f4 $< \
	| xargs samtools faidx $^2
	) >$@


unaligned.contigs.fasta: unaligned.contigs contigs.fasta
	$(call load_modules);
	(
	cut -f4 $< \
	| xargs samtools faidx $^2
	) >$@


#### PIPELINE LIFT-OVER WIG/BW
# transform wig to bigWig
%.cov.bw: %.cov.wig.gz contigs.tab
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load tools/ucsc-utils/16-Dec-2013; \
	wigToBigWig <(zcat $<) $^2 $@
	
# transform bigWig to BedGraph
%.cov.bgr: %.cov.bw
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load tools/ucsc-utils/16-Dec-2013; \
	bigWigToBedGraph $< $@

CLEAN += $(addsuffix .cov.bgr,self pn)


# calculate chain file for crossmap
chain: final.summary
	module purge; \
	module load lang/python/2.7; \
	allpaths2bed --chain <$< >$@

CLEAN += chain

# reverse the chain
chain.rew: chain
	module purge; \
	module load tools/ucsc-utils/16-Dec-2013; \
	chainSwap $< $@


# liftover bedgraph. 
# transform coverage of contigs to coverage of scaffolds
%.cov.sort.scaff.bgr.sorted.bgr: chain.rew %.cov.bgr
	module purge; \
	module load lang/python/2.7; \
	module load tools/crossmap/0.2.5; \
	CrossMap.py wig $< $^2 $@

CLEAN += $(addsuffix .cov.sort.scaff.bgr.sorted.bgr,self pn)

# convert bedgraph to bigWig
%.cov.sort.scaff.bw: %.cov.sort.scaff.bgr.sorted.bgr scaffolds.tab
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load tools/ucsc-utils/16-Dec-2013; \
	bedGraphToBigWig $< $^2 $@
#### 	



ALL += $(addsuffix .cov.sort.scaff.bw,self pn)
