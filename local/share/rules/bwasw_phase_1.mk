# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

REFERENCE ?=

FINAL_SUMMARY ?=

CONTIGS ?=

SCAFFOLDS ?=

log:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@

final.summary:
	ln -sf $(FINAL_SUMMARY) $@

contigs.fasta:
	zcat <$(CONTIGS) >$@

scaffolds.fasta:
	zcat <$(SCAFFOLDS) >$@

.PRECIOUS: contigs.bam
contigs.bam: log contigs.fasta
	!threads
	$(call load_modules); \
	/usr/bin/time -v \
	bwa bwasw \
	-t $$THREADNUM \
	$(REFERENCE) $^2 \
	2>$</bwa-bwasw.$@.log \
	| samtools sort -@ $$THREADNUM -m 4G -o - - >$@

# contigs.bam: log contigs.fasta
	# !threads
	# $(call load_modules); \
	# /usr/bin/time -v \
	# bwa mem \
	# -p \
	# -t $$THREADNUM \
	# -x intractg \
	# $(REFERENCE) $^2 \
	# 2>$</bwa-mem.$@.log \
	# | samtools sort -@ $$THREADNUM -m 4G -o - - >$@

### ScaPA ################################################################

.META: filtered.assembly.bam_placed-full.txt
	1	query_scaff
	2	query_start
	3	query_end
	4	query_span
	5	scaffold_length
	6	subj_chr
	7	chr_start
	8	chr_end
	9	chr_span
	10	query_coverage
	11	parsimony
	12	orientation
	13	SW-anchored
	14	SCORE
	15	identity
	16	glob_span
	17	seq_span
	18	gaps_cumul
	19	self.aligned
	20	alignments
	21	merged_object


# export alignments of the golden block for each placed scaffold
# in bed-12 format
filtered.assembly.bam_placed-full.txt: contigs.bam final.summary contigs.fasta
	module purge; \
	module load lang/python/2.7.3; \   * pysam-0.8.2.1 and biopython 1.60 needed *
	ScaPA_alpha_r004 \
	--alignment-files $< \
	--final-summary $^2 \
	--contig-fasta $^3 \
	--dump-aln \
	--out $(firstword $(subst _, ,$@))

filtered.assembly.bam_ambiguous.txt filtered.assembly.bam_log.txt filtered.assembly.bam_merging.log filtered.assembly.bam.pickle filtered.assembly.bam_placed.alignments.bed filtered.assembly.bam_placed-splits.txt filtered.assembly.bam_pseudomol.agp filtered.assembly.bam_resolving_inversions.txt filtered.assembly.bam_unaligned.txt: filtered.assembly.bam_placed-full.txt
	touch $@



# contains placed scaffolds. 
# The coordinates corresponds to the blocks that anchor scaffolds to the reference
assembly.placed-full.bed: filtered.assembly.bam_placed-full.txt
	$(call load_modules); \
	unhead $< \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@

# contains placed scaffolds as assembly.bed
# In addition to the scaffolds placed in a clean way, 
# it also contains the scaffolds placed after the split operation.
assembly.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@


# contains contigs.
# The coordinates corresponds to the alignments that anchor contigs to the reference
# only for placed scaffolds (not splitted)
# ScaPA_alpha_r002 now produce bed file without conversion
assembly.aligns.bed: filtered.assembly.bam_placed.alignments.bed
	$(call load_modules); \
	sortBed -i $< >$@

### ScaPA ################################################################


.PHONY: test
test:
	@echo 


ALL += log \
	reference.fasta \
	contigs.bam \
	filtered.assembly.bam_placed-full.txt \
	filtered.assembly.bam_ambiguous.txt \
	filtered.assembly.bam_log.txt \
	filtered.assembly.bam_merging.log \
	filtered.assembly.bam.pickle \
	filtered.assembly.bam_placed-splits.txt \
	filtered.assembly.bam_pseudomol.agp \
	filtered.assembly.bam_resolving_inversions.txt \
	filtered.assembly.bam_unaligned.txt \
	assembly.aligns.bed \
	assembly.bed

INTERMEDIATE += contigs.fasta \
	assembly.fasta

CLEAN += 
