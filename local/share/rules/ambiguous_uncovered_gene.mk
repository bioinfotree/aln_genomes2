# Copyright 2017 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:


ambiguous.contigs.bed:
	ln -sf ../half_pn_cov/ambiguous.contigs $@

unaligned.contigs.bed:
	ln -sf ../half_pn_cov/unaligned.contigs $@

ambiguous.contigs.fasta:
	ln -sf ../half_pn_cov/ambiguous.contigs.fasta $@

unaligned.contigs.fasta:
	ln -sf ../half_pn_cov/unaligned.contigs.fasta $@

ambiguous.scaffolds.bed: ../phase_1/filtered.assembly.bam_ambiguous.txt
	unhead $< \
	| cut -f1 \
	| bsort \
	| uniq >$@

unaligned.scaffolds.bed: ../phase_1/filtered.assembly.bam_unaligned.txt
	bsort <$< \
	| uniq >$@

# see makefile
scaffolds.fasta:

# obtain the parts of the scaffolds mapped (placed full + split) by scapa that were trimmed by scapa (not mapped)
trimmed.scaffolds.bed: ../phase_1/assembly.bed scaffolds.fasta
	$(call load_modules);
	bawk '!/^[\#,$$]/ { print $$4, $$7-1, $$8; }' <$< \   * remove 1 from scaffold begin of mapping since bed start from 0 *
	| bedtools complement -i stdin \
	-g <(fasta_length <$^2 | filter_1col 1 <(cut -f4 $<)) \   * calculate the genome only for the mapped scaffolds *
	>$@


	


# see makefile
sample.cov.bam:
# see makefile
pn.cov.bam:




#### LIFT OVER ANNOTATION IN BED FORMAT
chain:
	ln -sf ../half_pn_cov/$@

# see makefile
annot.sort.bed:


# liftover annotation of scaffolds to contigs from bed format
# this because the some exons are spanned across gaps
# if gff format is converted such annotation are completelly lost
# with bed format, such annotation are splitted instead, thus preserving exons
annot.contig.bed: chain annot.sort.bed
	module purge; \
	module load lang/python/2.7; \
	module load tools/crossmap/0.2.5; \
	CrossMap.py bed $< <(bsort -k 1,1V -k2,2n $^2 \
	| bawk 'BEGIN{ i=0; } { if ( $$8 == "exon" ) { split($$10,a,"=|-"); print $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8, $$9, "gene_id "a[2]"."i; i++ }; }') $@
#### LIFT OVER GTF

# convert bed to gtf format for featureCounts
exon.gtf: annot.contig.bed
	bsort -k 1,1V -k2,2n $< \
	| bawk 'BEGIN{ i=0; } { if ( $$8 == "exon" ) { print $$1, $$7, $$8, $$2+1, $$3, $$5, $$6, $$9, $$10" "$$11"."i; i++ }; }' >$@


# count reads that align on each exon.
# avoid sorting step since alignments are
# already sorted
%.exon.count.subread: exon.gtf %.cov.bam
	!threads
	featureCounts --donotsort -a $< -o $@ -t 'exon' -p -B -P -C -T $$THREADNUM $^2

# generata count tables
%.exon.count.tab: %.exon.count.subread
	bawk '!/^[#+,$$]/ { print $$0 }' $< \
	| unhead \
	| cut -f 1,7 >$@


.META: %.diffexp.txt
	1	id feature identifier
	2	baseMean mean normalised counts, averaged over all samples from both conditions
	3	baseMeanA mean normalised counts from condition A
	4	baseMeanB mean normalised counts from condition B
	5	foldChange fold change from condition A to B
	6	log2FoldChange the logarithm (to basis 2) of the fold change
	7	pval p value for the statistical significance of this change
	8	padj p value adjusted for multiple testing with the Benjamini-Hochberg procedure (see the R function p.adjust), which controls false discovery rate (FDR)


# calculate differential expression
diffexp.txt: sample.exon.count.tab pn.exon.count.tab
	module purge; \
	module load lang/python/2.7; \
	module load lang/r/2.15.1; \
	diff_exp_analysis.py \
	-i $^ \
	-o . \
	-q 1 \
	--scripts-dir $$PRJ_ROOT/local/bin \
	-n normalization \
	--do-diff-exp \
	--sample1 $< \
	--sample2 $^2 \
	--prefix-diff-exp $(basename $@)


# here I do not add 1 at start
annot.contig.exon.bed: annot.contig.bed
	bsort -k 1,1V -k2,2n $< \
	| bawk 'BEGIN{ i=0; } { if ( $$8 == "exon" ) { print $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8, $$9, $$11"."i; i++ }; }' >$@



# add coordinates to diffexp file
exon.diffexp.bed: diffexp.txt annot.contig.exon.bed
	module purge;
	module load lang/r/3.3;
	module load tools/bedtools/2.26.0;
	cat <<'EOF' | bRscript -
	options(width=10000)
	
	library('gtools')
	
	# order genomic regions by apply mixedorder on multiple vectors (columns)
	multi.mixedorder <- function(..., na.last = TRUE, decreasing = FALSE){
	do.call(order, c(
		lapply(list(...), function(l){
			if(is.character(l)){
				factor(l, levels=mixedsort(unique(l)))
			} else {
				l
			}
		}),
		list(na.last = na.last, decreasing = decreasing)
	))
	}
	
	setwd("$(PWD)")
	diffexp = bread("$<", header=T, check.names=T)
	exon = bread("$^2", header=F, check.names=T)
	colnames(exon) = c("chr", "start", "end", "name", "score", "strand", "attribute", "feature", "frame", "miRNA_id")
	
	# add coordinates to diffexp.txt file
	merged =  merge(exon, diffexp, by="miRNA_id", all.x=T)
	merged = merged[c(2:10,1,11:19)]
	
	merged = merged[multi.mixedorder(merged$$chr, merged$$start),]

	bwrite(merged, file="$@", col.names=T, row.names=F)
	
	EOF




# exons absent in the sample because differentially expressed
contig.by.deseq.absent.exon.bed: exon.diffexp.bed
	module purge;
	module load lang/r/3.3;
	module load tools/bedtools/2.26.0;
	cat <<'EOF' | bRscript -
	options(width=10000)
	setwd("$(PWD)")
	diffexp = bread("$<", header=T, check.names=T)
	
	# genes must be under expressed in the sample compared to the reference (diffexp$$log2FoldChange <= 0)
	diffexp = diffexp[ ( diffexp$$padj < 0.1 | ( diffexp[18] > 1 & diffexp[19] <= 1 ) ) 
					     & diffexp$$log2FoldChange <= 0 & !is.na(diffexp$$padj), ]
	
	bwrite(diffexp, file="$@", col.names=T, row.names=F)
	EOF


#### LIFT OVER ANNOTATION IN BED FORMAT
chain.rew:
	ln -sf ../half_pn_cov/$@

by.deseq.absent.exon.bed.tmp: chain.rew contig.by.deseq.absent.exon.bed
	module purge; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load tools/crossmap/0.2.5; \
	module load compilers/gcc/4.9.3; \
	module load tools/bedtools/2.26.0; \
	HEAD=$$(head -n 1 $^2); \
	SEP="!"; \
	CrossMap.py bed $< \    * since the bed is multicolum, transform into multicolums *
	<(cat $^2 \
		| awk -v sep="$$SEP" -F'\t' '{ \
		printf "%s\t%s\t%s\t", $$1, $$2, $$3; \
		for ( i=4; i<NF; i++ ) { \
			printf "%s%s", $$i, sep; } \
			printf "%s\n", $$NF; }'\
	) \
	| cut -f 6-9 \
	| sed -e '/^$$/d' -e 's/'"$$SEP"'/\t/g' \
	| sed -e '1s/^/'"$$HEAD\n"'/' >$@
	
by.deseq.absent.exon.bed: by.deseq.absent.exon.bed.tmp
	bawk '{ split($$10,a,"."); print $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8, $$9, a[1]"."a[2], $$11, $$12, $$13, $$14, $$15, $$16, $$17, $$18, $$19; }' $< >$@

#### 

# see makefile
# ../../../../pangenome4/dataset/pn_ref-cf_16/phase_2/cf_16.exon.del.ref.bed
exon.del.ref.bed:

# number of exons uncovered
tot.exon.number.bed: exon.del.ref.bed
	unhead $< \
	| bsort -k 1,1V -k2,2n \
	| bawk 'BEGIN{ i=0; } { if ( $$8 == "exon" ) { split($$10,a,"=|-"); print $$0, a[2]"."i; i++ }; }' >$@

ambiguous.exon.bed: ambiguous.scaffolds.bed tot.exon.number.bed
	filter_1col 1 $< <$^2 >$@

unaligned.exon.bed: unaligned.scaffolds.bed tot.exon.number.bed
	filter_1col 1 $< <$^2 >$@

# annotations in the trimmed of part of scaffolds by scapa
trimmed.exon.bed: tot.exon.number.bed trimmed.scaffolds.bed
	$(call load_modules); \
	bedtools intersect \
	-bed \
	-f 1.0 \
	-wa \
	-a $< \
	-b $^2 \
	| sortBed -i stdin >$@


# calculates uncovered exons in ambiguous unaligned and trimmed part of scaffolds
uncovered.exon.matrix.bed: tot.exon.number.bed by.deseq.absent.exon.bed ambiguous.exon.bed unaligned.exon.bed trimmed.exon.bed
	filter_1col 23 <(unhead <$^2 | cut -f 10 | bsort | uniq) <$< \
	| filter_1col 23 <(cat $^3 $^4 $^5 | cut -f 23) \
	| awk -F"\t" '{ for (x=1; x<=20; x++) {  printf "%s\t", $$x } printf "1\t%s\t%s\n", $$22, $$23; }' >$@

# total uncovered exons by assembly, ambiguous unaligned and trimmed part of scaffolds
exon.matrix.bed: uncovered.exon.matrix.bed tot.exon.number.bed
	$(call load_modules); \
	filter_1col -v 23 <(cut -f 23 <$<) <$^2  \
	| cat - $< \
	| sortBed -i stdin >$@

uncovered.exon.del.ref.bed: exon.matrix.bed
	bawk '{ if ( $$21 == 1 ) print $$0 }' <$< >$@

# see makefile
kmer.gene.sort.bed:

gene.matrix.bed: exon.matrix.bed kmer.gene.sort.bed
	module purge;
	module load lang/r/3.3;
	module load tools/bedtools/2.26.0;
	cat <<'EOF' | bRscript -
	options(width=10000)
	setwd("$(PWD)")
	
	# total exons
	all_exon = bread(file="$<", header=F, check.names=T)
	cname = c(  "scaffold", "start", "end", "name", "score", "strand", "attribute", "feature", "frame", "description",
				"size", "positions_covered_by_kmers", "sum_of_kmer_coverage", "mean_kmer_coverage_over_covered_positions", "mean_kmer_coverage_over_all_positions",
				"cover_feat", "bases", "size1", "fraction",
				"gene", "exon", "exon_gene", "exon_index" )
	colnames(all_exon) = cname
	
	# transform column to factor
	all_exon$$description = factor(all_exon$$description)
	
	# produces a list of columns according to the classification
	# defined in the factor.
	# If all the columns have the same length, the resulting list
	# is coerced to a data frame.
	out = unstack(data.frame(all_exon$$size1, all_exon$$description))
	# apply sum operation to avery
	# element of a list
	somma = lapply(out,sum)
	# tranform list to dataframe by extracting each element of the list
	all_exon_size = data.frame(unlist( lapply (somma ,`[[`,1 ) ))
	all_exon_size$$description = row.names(all_exon_size)
	all_exon_size = all_exon_size[,c(2,1)]
	colnames(all_exon_size) = c("description","size")
	
	# split string in rows of a columns by =|- and return the 2 argument
	all_exon_size$$description = unlist( lapply ( strsplit(as.character( all_exon_size$$description ), c("=|-")),`[[`,2 ) )
	
	# exons considered uncovered
	uncovered_exons = all_exon[ all_exon$$exon == 1, ]
	
	uncovered_exons$$description = factor(uncovered_exons$$description)	
	out = unstack(data.frame(uncovered_exons$$size1, uncovered_exons$$description))
	somma = lapply(out,sum)
	uncovered_exons_size = data.frame(unlist( lapply (somma ,`[[`,1 ) ))
		
	
	uncovered_exons_size$$description = row.names(uncovered_exons_size)
	
	uncovered_exons_size = uncovered_exons_size[,c(2,1)]
	colnames(uncovered_exons_size) = c( "description","size" )
	
	# split string in rows of a columns by =|- and return the 2 argument
	uncovered_exons_size$$description = unlist( lapply ( strsplit(as.character( uncovered_exons_size$$description ), c("=|-")),`[[`,2 ) )
	
	# all.x=T maintain all rows of x
	fract_cov = merge(all_exon_size, uncovered_exons_size, by=c("description"), all.x=T, suffixes=c( "_total", "_uncoverd" ) )
	
	# convert NA to 0 in the given columns
	fract_cov[c("size_total", "size_uncoverd")][is.na( fract_cov[ c("size_total", "size_uncoverd") ] )] = 0
	
	# initialize new column to 0
	fract_cov$$gene = 0
	
	# if uncovered fraction is less then 80%
	fract_cov$$gene[ fract_cov$$size_uncoverd / fract_cov$$size_total > 0.8 ] = 1
	
	# load gene names with kmer coverage
	gene_kmer_cov = bread(file="$^2", header=F, check.names=T)
	
	
	cname = c(  "scaffold", "start", "end", "name", "score", "strand", "attribute", "feature", "frame", "description",
				"size", "positions_covered_by_kmers", "sum_of_kmer_coverage", "mean_kmer_coverage_over_covered_positions", "mean_kmer_coverage_over_all_positions" )
	colnames(gene_kmer_cov) = cname
	
	# add columns for compatibility
	gene_kmer_cov$$fraction = gene_kmer_cov$$size1 = gene_kmer_cov$$bases = gene_kmer_cov$$cover_feat = 0
	
	# merge coverage
	gene_kmer_cov = merge(gene_kmer_cov, fract_cov, by.x=c("name"), by.y=c("description"), all=T,  suffixes = c("",".y"))
	
	# add columns for compatibility
	gene_kmer_cov$$exon_gene = gene_kmer_cov$$exon = 0
	
	gene_kmer_cov$$exon_gene[ gene_kmer_cov$$size_uncoverd != 0 ] = 1

	# select only usefull columns
	gene_kmer_cov = subset(gene_kmer_cov, select=c(2,3,4,1,5:19,22:24))
	
	bwrite(gene_kmer_cov, file="$@", col.names=T, row.names=F)
	
	EOF


# calculates uncovered genes as genes having >80% uncovered exons
1.gene.matrix.bed: tot.exon.bed by.deseq.absent.exon.bed ../../../../pangenome4/dataset/pn_ref-rk_4/phase_2/kmer.gene.sort.bed
	module purge;
	module load lang/r/3.3;
	module load tools/bedtools/2.26.0;
	cat <<'EOF' | bRscript -
	options(width=10000)
	setwd("$(PWD)")
	
	# total exons
	all_exon = bread(file="$<", header=F, check.names=T)
	cname = c(  "scaffold", "start", "end", "name", "score", "strand", "attribute", "feature", "frame", "description" )
	colnames(all_exon) = cname
	
	all_exon$$size1 = all_exon$$end - all_exon$$start
	
	# transform column to factor
	all_exon$$description = factor(all_exon$$description)
	
	# produces a list of columns according to the classification
	# defined in the factor.
	# If all the columns have the same length, the resulting list
	# is coerced to a data frame.
	out = unstack(data.frame(all_exon$$size1, all_exon$$description))
	# apply sum operation to avery
	# element of a list
	somma = lapply(out,sum)
	# tranform list to dataframe by extracting each element of the list
	all_exon_size = data.frame(unlist( lapply (somma ,`[[`,1 ) ))
	all_exon_size$$description = row.names(all_exon_size)
	all_exon_size = all_exon_size[,c(2,1)]
	colnames(all_exon_size) = c("description","size")
		
	# split string in rows of a columns by =|- and return the 2 argument
	all_exon_size$$description = unlist( lapply ( strsplit(as.character( all_exon_size$$description ), c("=|-")),`[[`,2 ) )
	
	
	# exons considered uncovered
	uncovered_exons = bread(file="$^2", header=T, check.names=T)
	uncovered_exons$$size1 = uncovered_exons$$end - uncovered_exons$$start
	
	# split by dot
	uncovered_exons$$miRNA_id. = unlist( lapply ( strsplit(as.character( uncovered_exons$$miRNA_id. ), c("\\.")),`[[`,1 ) )
	
	# change name of single column
	colnames(uncovered_exons)[colnames(uncovered_exons) == "miRNA_id."] = "description"
	uncovered_exons$$description = factor(uncovered_exons$$description)	
	out = unstack(data.frame(uncovered_exons$$size1, uncovered_exons$$description))
	somma = lapply(out,sum)
	uncovered_exons_size = data.frame(unlist( lapply (somma ,`[[`,1 ) ))
		
	
	uncovered_exons_size$$description = row.names(uncovered_exons_size)
	
	uncovered_exons_size = uncovered_exons_size[,c(2,1)]
	colnames(uncovered_exons_size) = c( "description","size" )
		
	
	# all.x=T maintain all rows of x
	fract_cov = merge(all_exon_size, uncovered_exons_size, by=c("description"), all.x=T, suffixes=c( "_total", "_uncoverd" ) )
	
	# convert NA to 0 in the given columns
	fract_cov[c("size_total", "size_uncoverd")][is.na( fract_cov[ c("size_total", "size_uncoverd") ] )] = 0
	
	# initialize new column to 0
	fract_cov$$gene = 0
	
	# if uncovered fraction is less then 80%
	fract_cov$$gene[ fract_cov$$size_uncoverd / fract_cov$$size_total > 0.8 ] = 1
	
	# load gene names with kmer coverage
	gene_kmer_cov = bread(file="$^3", header=F, check.names=T)
	
	
	cname = c(  "scaffold", "start", "end", "name", "score", "strand", "attribute", "feature", "frame", "description",
				"size", "positions_covered_by_kmers", "sum_of_kmer_coverage", "mean_kmer_coverage_over_covered_positions", "mean_kmer_coverage_over_all_positions" )
	colnames(gene_kmer_cov) = cname
	
	# add columns for compatibility
	gene_kmer_cov$$fraction = gene_kmer_cov$$size1 = gene_kmer_cov$$bases = gene_kmer_cov$$cover_feat = 0
	
	# merge coverage
	gene_kmer_cov = merge(gene_kmer_cov, fract_cov, by.x=c("name"), by.y=c("description"), all=T,  suffixes = c("",".y"))
	
	# add columns for compatibility
	gene_kmer_cov$$exon_gene = gene_kmer_cov$$exon = 0
	
	gene_kmer_cov$$exon_gene[ gene_kmer_cov$$size_uncoverd != 0 ] = 1
	
	sum(gene_kmer_cov$$exon_gene)
	
	sum(fract_cov$$gene)
	
	# select only usefull columns
	gene_kmer_cov = subset(gene_kmer_cov, select=c(2,3,4,1,5:19,22:24))
	
	bwrite(gene_kmer_cov, file="$@", col.names=T, row.names=F)
	
	EOF



# aggregate some statistics
del.stats: gene.matrix.bed exon.matrix.bed gene.matrix.bed exon.matrix.bed
	# heade
	cat >$@ <<EOF | bsort -V -k 1,1
	#sample	tot_genes uncovered_genes	tot_exons	uncovered_exons	gene_of_uncovered_exons
	EOF
	
	# take al the 
	# prerequisites
	# without collapsing
	# identical ones
	genes=($+)

	_tmp=$${genes[0]}
	s=$${_tmp%.gene.deleted.bed}

	n_genes=0
	if grep -qc 'gene' <$${genes[0]}; then
		n_genes=$$(bawk '{ if ( $$20 == "1" ) print $$15 }' <$${genes[0]} | wc -l)
	fi
	
	
	n_exons=0
	if grep -qc 'exon' <$${genes[1]}; then
		n_exons=$$(bawk '{ if ( $$21 == "1" ) print $$15 }' <$${genes[1]} | wc -l)
	fi
	
	
	n_gene_exons=0
	if grep -qc 'exon' <$${genes[1]}; then
		n_gene_exons=$$(bawk '{ if ( $$21 == "1" ) { split($$10,a,"="); print a[2] } }' <$${genes[1]} | bsort | uniq | wc -l)
	fi
	
	tot_gene=0
	tot_gene=$$(unhead <$${genes[2]} | wc -l | cut -f1)
	
	
	tot_exon=0
	tot_exon=$$(unhead <$${genes[3]} | wc -l | cut -f1)
	
	
	cat >>$@ <<EOF
	$$s	$$tot_gene	$$n_genes	$$tot_exon	$$n_exons	$$n_gene_exons
	EOF







# hemizygous.%.lst obtained from:
# rsync -avz ../../../../check_hemizygosity2/dataset/rkatsiteli4/1.65/hemizygous.lst assembler:/mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/aln_genomes2/dataset/rkatsiteli4_gap_25/ambiguous_uncovered_gene/hemizygous.1.65.lst	

# calculate fraction of hemizygous contigs in ambiguous and unaligned scaffolds
hemizygous.%.stat: hemizygous.%.lst ambiguous.contigs.bed unaligned.contigs.bed
	total_ambiguous="$$(cat $^2 | wc -l)";
	total_unaligned="$$(cat $^3 | wc -l)";
	ambiguous="$$(filter_1col 4 $< <$^2 | bsort | uniq | wc -l)";
	unaligned="$$(filter_1col 4 $< <$^3 | bsort | uniq | wc -l)";
	cat >$@ <<EOF
	total ambiguous	$$total_ambiguous
	total unaligned	$$total_unaligned
	ambiguous hemy	$$ambiguous
	unaligned hemy	$$unaligned
	EOF
	



CLEAN += 

ALL += hemizygous.1.65.stat

