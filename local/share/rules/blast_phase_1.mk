# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# heterozygosity Vs coverage of contigs on the reference

# TODO:

context prj/kmer_count2

# reference
REFERENCE ?=

# contigs
CONTIGS ?=

# scaffolds
SCAFFOLDS ?=

MIN_REF_LEN ?= 50000

# log dir
logs:
	mkdir -p $@

final.summary:
	ln -sf $(FINAL_SUMMARY) $@

contigs.fasta:
	zcat <$(CONTIGS) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }' \
	| bsort \
	| tab2fasta 2 \
	| resolve_iupac --bed-file=contigs.sub.bed >$@

# save list of chunks to make var
chunks.mk:
	ARRAY=( $$(seq -s " " $(SPLIT)) ); \
	echo "FASTA_CHUNCKS := $${ARRAY[@]/#/contigs.fasta.}" >$@

include chunks.mk

# split fasta
contigs.fasta.%: contigs.fasta
	$(call load_modules); \
	if [ "$*" == "1" ]; then \ 
		gt splitfasta -numfiles $(SPLIT) -force yes $<; \
	else \
		sleep 5; \   * wait untill all pieces are generated *
	fi


scaffolds.fasta:
	zcat <$(SCAFFOLDS) >$@

# denom can't handle to short references
reference.fasta:
	zcat $(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( $$1 ~ /scaffold_[0-9]+/ ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 \
	| resolve_iupac --bed-file=reference.sub.bed >$@

reference.fasta.nin: reference.fasta
	$(call load_modules); \
	makeblastdb -in $(basename $@) -dbtype nucl -title $(SAMPLE_NAME)

# dustmasker identifies and masks out low complexity parts 
# of a genome using a new and improved DUST algorithm
reference.dust.asnb: reference.fasta.nin
	$(call load_modules); \
	dustmasker \
	-in $(basename $<) \
	-infmt blastdb \
	-parse_seqids \
	-outfmt maskinfo_asn1_bin \
	-out $@

reference.count: reference.fasta.nin
	$(call load_modules); \
	windowmasker \
	-in $(basename $<) \
	-infmt blastdb \
	-mk_counts \
	-parse_seqids \
	-out $@

reference.mask.asnb: reference.fasta.nin reference.count
	$(call load_modules); \
	windowmasker \
	-in $(basename $<) \
	-infmt blastdb \
	-ustat $^2 \
	-outfmt maskinfo_asn1_bin \
	-parse_seqids \
	-out $@

reference.fasta.mask.dust.nin: reference.fasta.nin reference.mask.asnb reference.dust.asnb
	$(call load_modules); \
	makeblastdb \
	-in $(basename $<) \
	-input_type blastdb \
	-dbtype nucl \
	-parse_seqids \
	-mask_data $^2,$^3 \
	-out $(basename $@) \
	-title $(SAMPLE_NAME)

reference.fasta.mask.dust.info: reference.fasta.mask.dust.nin
	$(call load_modules); \
	blastdbcmd \
	-db $(basename $@) \
	-info \
	2>&1 \
	| tee $@

# contigs.xml: contigs.fasta reference.fasta.nin
	# !threads
	# $(call load_modules); \
	# /usr/bin/time -v \
	# blastn -evalue 0.001 -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $< -db $(basename $^2) -out $@ \
	# 2>&1 \
	# | tee $@.mm.blast


# execute denom for every chunks
# it runs in succession the indexing and alignment. 
# In oreder to avoid to rebuild indexs for each chunk, I wait for it to finish indexing and allignment of chuck 1.
# For chuncks >1, I create links for indexs in directory 1 to their respective directories. I run denom easyrun.
# It finds the index files and proceeds whith the alignment, that can be executed in parallel for all the chunk> 1.
BLAST_XML = $(addsuffix .xml,$(addprefix contigs.,$(shell seq $(SPLIT))))
contigs.%.xml: contigs.fasta.% reference.fasta.mask.dust.nin
	!threads 6
	$(call load_modules); \
	/usr/bin/time -v \
	blastn \
	-evalue 1e-03 \
	-word_size 30 \
	-task megablast \
	-num_threads $$THREADNUM \
	-xdrop_gap 40 \
	-outfmt 5 \
	-max_target_seqs 2 \
	-query $< \
	-db $(basename $^2) \
	-out $@ \
	2>&1 \
	| tee $@.mm.blast

### ScaPA ################################################################

.META: filtered.assembly.bam_placed-full.txt
	1	query_scaff
	2	query_start
	3	query_end
	4	query_span
	5	scaffold_length
	6	subj_chr
	7	chr_start
	8	chr_end
	9	chr_span
	10	query_coverage
	11	parsimony
	12	orientation
	13	SW-anchored
	14	SCORE
	15	identity
	16	glob_span
	17	seq_span
	18	gaps_cumul
	19	self.aligned
	20	alignments
	21	merged_object


# export alignments of the golden block for each placed scaffold
# in bed-12 format
filtered.assembly.bam_placed-full.txt: final.summary contigs.fasta $(BLAST_XML)
	module purge; \
	module load lang/python/2.7; \
	ScaPA_alpha_r004 \
	--alignment-files $(BLAST_XML) \
	--final-summary $< \
	--out $(firstword $(subst _, ,$@))


filtered.assembly.bam_placed.alignments.bed: filtered.assembly.bam_placed-full.txt
	touch $@

filtered.assembly.bam_placed-splits.txt: filtered.assembly.bam_placed-full.txt
	touch $@

### ScaPA ################################################################

# contains placed scaffolds. 
# The coordinates corresponds to the blocks that anchor scaffolds to the reference
assembly.placed-full.bed: filtered.assembly.bam_placed-full.txt
	$(call load_modules); \
	unhead $< \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@

# contains placed scaffolds as assembly.bed
# In addition to the scaffolds placed in a clean way, 
# it also contains the scaffolds placed after the split operation.
assembly.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@

# same as above but add ambiguous
assembly2.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt filtered.assembly.bam_ambiguous.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$6,$$7,$$8,$$1,int(100^$$14),strand,feature_start,feature_end,"255,0,0","1",$$9,"0"; \
	}' \
	| sortBed -i stdin >$@


assembly.swap.bed: filtered.assembly.bam_placed-full.txt filtered.assembly.bam_placed-splits.txt filtered.assembly.bam_ambiguous.txt
	$(call load_modules); \
	cat <(unhead $<) <(unhead $^2) \
	| bawk 'BEGIN { strand=""; } \
	!/^[\#,$$]/ { \
	if ( $$12 > 0 ) { strand="+"; } else { strand="-"; } \
	feature_start=$$2; feature_end=$$3; \
	if ( $$2 > $$3 ) { feature_start=$$3; feature_end=$$2 } \   * reverse feature coordinates if start > end *
	print $$1,feature_start,feature_end,$$6,$$7,$$8,int(100^$$14),strand; \
	}' \
	| sortBed -i stdin >$@


.PHONY: test
test:
	@echo $(BLAST_XML)


ALL += contigs.fasta \
	reference.fasta.mask.dust.info \
	$(BLAST_XML)

INTERMEDIATE += 

CLEAN += 
