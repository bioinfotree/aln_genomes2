# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

log:
	mkdir -p $@

contigs.fasta:
	ln -sf $(CONTIGS) $@

reference.fasta.gz:
	ln -sf $(REFERENCE) $@

genes.gff3:
	ln -sf /mnt/vol1/genomes/vitis_vinifera/annotation/genes/annotation/V2.1/V2.1.gff3 $@

quast: contigs.fasta reference.fasta.gz genes.gff3
	!threads
	module load tools/quast/3.2; \
	module load lang/python/2.7; \
	module load lang/perl/5.10.1; \
	module load lang/java/jdk1.8.0_74; \
	quast.py \
	$< \
	-R $^2 \
	--genes $^3 \
	--min-contig 10 \
	--threads $$THREADNUM \
	--labels "$(SAMPLE_NAME) assembly" \
	--eukaryote \
	--plots-format png \
	-o $@

	#--gage \

.PHONY: test
test:
	@echo 




ALL += 




INTERMEDIATE +=

CLEAN += 
