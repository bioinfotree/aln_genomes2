# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

REFERENCE ?= 

log:
	mkdir -p $@

reference.fasta:
	cat $(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }' \
	| bsort \
	| tab2fasta 2 >$@

# reference.fasta:
	# cat $(REFERENCE) \
	# | fasta2tab \
	# | tr " " \\t \
	# | awk 'BEGIN { OFS = "\t" } \
	# !/^[\#+,$$]/ { \
	# if ( $$1 ~ /Pt|Mt|chr[0-9]+/ ) print $$1,$$NF; }' \   * filter is needed by denom *
	# | bsort \
	# | tab2fasta 2 >$@

reference.fasta.bwt: log reference.fasta
	$(load_env); \
	bwa index $^2 \
	2>&1 \
	| tee $</$(basename $@).log


reference.fasta.mmi: log reference.fasta
	$(load_env); \
	lra index $^2 \
	2>&1 \
	| tee $</$(basename $@).lra.log


ALL +=  reference.fasta \
	reference.fasta.bwt \
	reference.fasta.mmi

INTERMEDIATE +=

CLEAN += log \
	 $(wildcard reference.fasta.*)