#!/usr/bin/env python

#import ipdb
import re
import argparse
import pysam
import sys
import copy
import time
import datetime
import os
import collections
from pprint import PrettyPrinter
#import math
#import operator
#import numpy as np

#

class Block(object):

    # blocks are originally sorted by reference coordinates
    def __init__(self, alignment, scaffold):
        self.alignments = [alignment]
        self.scaffold = scaffold
        self.merged_object = False
    #
    @property
    def refname(self):
        return self.alignments[0].ref

    @property
    def alignments_qorder(self):
        qsorted = []
        for contig in self.scaffold:

            contig_alns = [aln for aln in self.alignments if aln.qname == contig.name]
            contig_alns.sort(key=lambda x:x.orig_qstart)
            qsorted += contig_alns

        return qsorted

    @property
    def orig_qstart(self):
        first_aln = self.alignments_qorder[0]
        partial = first_aln.orig_qstart
        scaffobj = self.scaffold
        for i in range(len(scaffobj.contigs)):
            if scaffobj.contigs[i].name == first_aln.qname:
                return partial
            else:
                partial += scaffobj.lengths[i] + scaffobj.newgaps[i]
        raise Exception("Segmentation fault: block contig not in scaffold order")


    @property
    def orig_qend(self):
        last_aln = self.alignments_qorder[-1]
        partial = last_aln.orig_qend
        scaffobj = self.scaffold
        for i in range(len(scaffobj.contigs)):
            if scaffobj.contigs[i].name == last_aln.qname:
                return partial
            else:
                partial += scaffobj.lengths[i] + scaffobj.newgaps[i]
        raise Exception("Segmentation fault: block contig not in scaffold order")


    @property
    def aligned(self):
        aligned = 0
        for aln in self.alignments:
            aligned += aln.qaligned
        return aligned


    @property
    def rspan(self):
        return self.rend - self.rstart


    @property
    def scaffspans(self):
        first_aln = self.alignments_qorder[0]
        last_aln = self.alignments_qorder[-1]




        first_cont_start = first_aln.orig_qstart
        last_cont_end = last_aln.orig_qend

        # going back to scaffold object where gaps info is stored
        a = [x.name for x in self.scaffold.contigs].index(first_aln.qname)
        b = [x.name for x in self.scaffold.contigs].index(last_aln.qname)


        if a == b:
        #same contig
            span = last_cont_end - first_cont_start
            return span, span, 0

        #print a
        #print b
        first_cont_span = self.scaffold.lengths[a] - first_cont_start
        last_cont_span = last_cont_end

        glob_span = 0
        seq_span = 0
        gaps = 0
        glob_span += last_cont_span + first_cont_span
        seq_span += last_cont_span + first_cont_span

        #no cycle if a + 1 == b
        for i in range(a + 1, b):
            # omitting the first and the last
            seq_span += self.scaffold.lengths[i]
            glob_span += self.scaffold.lengths[i]
            glob_span += self.scaffold.newgaps[i]
            gaps += self.scaffold.newgaps[i]

        return glob_span + 1, seq_span + 1, gaps


    @property #fraction of the scaffold, gap included, in this block
    def glob_scaff_coverage(self):
        return float(self.scaffspans[0]) / self.scaffold.length

    @property #percentage of ACGT sequence in the block region that has alignement
    def anchored_perc(self):
        return float(self.aligned) / self.scaffspans[1]

    @property
    def parsimony(self):
        a = min(self.scaffspans[0], self.rspan)
        b = max(self.scaffspans[0], self.rspan)

        return float(abs(a)) / abs(b)

    @property
    def identity(self):
        differ = 0
        total = self.aligned
        for aln in self.alignments:
            differ += aln.mismatches
            #differ += aln.indels # mismatches includes SNP and INDELs
        return (float(total) - float(differ)) / total

    @property
    def rstart(self):
        self.alignments.sort(key=lambda x:x.rstart)
        return self.alignments[0].rstart

    @property
    def rend(self):
        self.alignments.sort(key=lambda x:x.rstart)
        return self.alignments[-1].rend


    @property
    def coeff(self):
        acc = 0
        for aln in self.alignments:
            if aln.scaff_reverse:
                aaa = aln.qaligned * -1
            else:
                aaa = aln.qaligned
            acc += aaa
        return float(acc) / self.aligned

    @property
    def score(self):
        a = self.glob_scaff_coverage
        b = self.parsimony
        c = self.anchored_perc
        d = self.identity
        #e = len(self.alignments)

        return a * b * c * d


    fields = ["query_scaff",  "query_start", "query_end", "query_span", "scaffold_length",
              "subj_chr", "chr_start", "chr_end", "chr_span",
              "query_coverage", "parsimony", "orientation", "SW-anchored",
              "SCORE", "identity", "glob_span", "seq_span",
              "gaps_cumul", "self.aligned", "#alignments", "merged_object"]
    header = "# " + "\t".join(fields)



    def __str__(self):
        query_span = self.orig_qend - self.orig_qstart + 1

        data = [self.scaffold, self.orig_qstart, self.orig_qend, query_span, self.scaffold.length,
                self.refname, self.rstart, self.rend, self.rspan,
                self.glob_scaff_coverage, self.parsimony, self.coeff, self.anchored_perc,
                self.score, self.identity, self.scaffspans[0], self.scaffspans[1],
                self.scaffspans[2], self.aligned, len(self.alignments), self.merged_object]

        data = map(lambda x:str(x), data)
        line = "\t".join(data)
        return line





class Scaffold(object):

    def __init__(self, name):
        self.counter = 0
        self.name = name
        self.contigs = []
        self.pos = []      # pos will be based on new gaps
        self.ingaps = []
        self.newgaps = []
        self.lengths = []

        self.placed = False
        self.goldenblock = False

    def __str__(self):
        return self.name



    def add_contig(self, scaffobj, name, gap,
                   length, min_gap, rank, reverse):

        #seq = fastaobj.fetch(name)
        contig = Contig(scaffobj, name, rank, reverse)
        self.contigs.append(contig)

        if len(self.contigs) == 1:
            outgap = 0
            ingap = 0
            pos = 1
        elif gap < min_gap:
            outgap = min_gap
            ingap = gap
            pos = self.pos[-1] + self.lengths[-1] + outgap
        else:
            outgap = gap
            ingap = gap
            pos = self.pos[-1] + self.lengths[-1] + outgap

        self.ingaps.append(ingap)
        self.newgaps.append(outgap)
        self.lengths.append(length)
        self.pos.append(pos)

    def __iter__(self):
        #return self
        for contig in self.contigs:
            yield contig

    @property
    def length(self):
        length = 0
        if len(self.newgaps) == 0:
            raise Exception("Uncalculated gaps!")
        assert len(self.newgaps) == len(self.contigs)
        for i in range(len(self.contigs)):
            length += self.lengths[i]
            length += self.newgaps[i]
        return length






class Contig(object):

    def __init__(self, scaffobj, name, rank, reverse):
        #self.counter = 0
        self.scaffold = scaffobj
        self.name = name
        self.alignments=[]
        self.rank = rank
        self.reverse = reverse

    def __str__(self):
        return self.name

    def __iter__(self):
        #return self
        for alignment in self.alignments:
            yield alignment




class Alignment(object):

    def __init__(self, read, bamhandler):
        self.qname = read.qname
        self.contig = None
        self.ref = bamhandler.getrname(read.tid)
        #self.ref = read
        self.rstart = read.pos + 1
        self.rend = int(read.aend)
        self.cigar = read.cigar
        self.reverse = read.is_reverse
        self.qlength = read.rlen

        # aligned part, also considering deleted bases from the ref
        #self.qaligned = read.qlen
        #self.tlen = read.tlen

        for tag in read.tags:
            if tag[0] == 'NM':
                self.mismatches = tag[1]

        accumulator = 0
        left_clip = 0
        right_clip = 0
        indels = 0
        start = False
        end = False

        for i, pattern in enumerate(self.cigar):
            if pattern[0] in [3, 4, 5] and not start:
                left_clip += pattern[1]
            elif pattern[0] in [0, 1, 6, 7, 8] and not start:
                start = left_clip + 1
                accumulator += pattern[1]
            elif pattern[0] in [0, 1, 6, 7, 8]:
                accumulator += pattern[1]
            elif pattern[0] in [3, 4, 5] and not end:
                end =  left_clip + accumulator
                right_clip += pattern[1]
            elif pattern[0] in [3, 4, 5]:
                right_clip += pattern[1]
            elif pattern[0] == 2:
                pass
                #deletion from the reference
            else:
                sys.exit("The code is not handling this CIGAR, please debug")

            if pattern[0] in [1, 2]:
                indels += pattern[1]

        self.indels = indels
        self.qstart = start
        #self.qstart2 = read.qstart # mine is 1-based, pysam uses 0-based
        if not end:
            self.qend = read.rlen
        else:
            self.qend = end
        #self.qend2 = read.qend
        self.qaligned = accumulator  # this equal to read.qlen
        self.raligned = read.alen
        self.left_clip = left_clip
        self.right_clip = right_clip

    @property
    def scaff_reverse(self):
        if ((self.reverse and self.contig.reverse) or
            (not self.reverse and not self.contig.reverse)):
            return False
        elif ((self.reverse and not self.contig.reverse) or
            (not self.reverse and self.contig.reverse)):
            return True


    @property
    def orig_qstart(self):
        if self.scaff_reverse:
            return self.qlength - self.qend + 1
        else:
            return self.qstart
    @property
    def orig_qend(self):
        if self.scaff_reverse:
            return self.qlength - self.qstart + 1
        else:
            return self.qend



class Contig_seq(object):

    def __init__(self, name, sequence):
        self.name
        self.seq


def read_contigs(fasta_file):
    fastaobj = pysam.Fastafile(fasta_file)

    ordict = collections.OrderedDict()
    for ref in fastaobj.references:
        sequence = fastaobj.fetch(ref)
        mycontig = Contig_seq(ref, sequence)
        ordict[ref] = mycontig




def read_sspace(sspace, fasta_file, **kwargs):
    fastaobj = pysam.Fastafile(fasta_file)
    list_of_ref = fastaobj.references

    sspace_h = open(sspace, 'r')
    scaffoldlist = []
    memo = {}


    for line in sspace_h.readlines():
        min_gap = kwargs.get('min_gap', 1)
        if not line.split():
            continue
        info = line.rstrip().split("|")
        #print info
        if info[0].startswith(">"):
            counter = 1
            memo = {'merged': False,
                    'gaps': 0,
                    'links': 0}

            name = info[0][1:]
            #print name
            scaffoldlist.append(Scaffold(name))
        else:
            m = re.match(r'(f|r)_tig(\d+)', info[0])
            if m.group(1) == "f":
                reverse = False
            else:
                reverse = True
            tignum = int(m.group(2))
            contig_name = list_of_ref[tignum - 1]
            m = re.match(r'size(\d+)', info[1])
            length = int(m.group(1))
            if len(info) > 2:
                m = re.match(r'links(\d+)', info[2])
                links = int(m.group(1))
            else:
                links = False
            if len(info) > 3:
                m = re.match(r'gaps(-?\d+)', info[3])
                gaps = int(m.group(1))
            else:
                gaps = False
            if len(info) > 4:
                m = re.match(r'merged(\d+)', info[4])
                merged = int(m.group(1))
            else:
                merged = False


            if memo['merged']:
                min_gap = -10000000000
                memo['gaps'] = -memo['merged']


            scaffoldlist[-1].add_contig(scaffoldlist[-1], contig_name, memo['gaps'],
                                        length, min_gap, counter, reverse)

            counter += 1

            memo = {'links': links,
                    'gaps': gaps,
                    'merged': merged}

    return scaffoldlist

def read_allpaths_summary(summary, fasta_file, **kwargs):


    min_gap = kwargs.get('min_gap', 1)
    summary_h = open(summary, 'r')

    scaffoldlist = []

    for i, line in enumerate(summary_h):
        # print i
        line = line.rstrip()
        #print line
        if line.startswith('scaffold'):
            counter = 1
            parts = line.split(" ")
            name = parts[0] + "_" + parts[1]
            #print name
            scaffoldlist.append(Scaffold(name))


        elif line != "" and not re.match('^[a-zA-Z].*', line):
            try:
                #scaffoldlist[-1]
                reverse = False #ALLPATHS output is always with contigs in FOR..
                mytuple = ((scaffoldlist[-1], ) +
                          parse_allpaths_line(line) + # this tuple contains contig_name, gap and length
                          (min_gap, counter, reverse))

                # print mytuple

#                 if i == 3:
#                     exit()


                scaffoldlist[-1].add_contig(*mytuple)
                counter += 1
                #parse_allpaths_line(line)
                pass
                #print line
            except IndexError:
                continue

    return scaffoldlist


def parse_allpaths_line(line):
    #print line
    if line.startswith(" -- "):
        m = re.match(r' -- \((-?\d+) \+\/\- \d+\) --> (\d+) \(l = (\d+)\)', line)
        gap = int(m.group(1))
        contig = "contig_" + m.group(2)
        length = int(m.group(3))
    ##
    else:
        m = re.match(r'(\d+) \(l = (\d+)\)', line)
        gap = 0
        contig = "contig_" + m.group(1)
        length = int(m.group(2))

    return (contig, gap, length)


def get_ref_lengths(bamfile):
    bamhandler = pysam.Samfile(bamfile, "rb")
    ref_dict = {}
    for reference in bamhandler.header['SQ']:
        element = reference['SN']
        length = int(reference['LN'])
        ref_dict[element] = length
    return ref_dict


def get_alignments(bam):
    #print "Ma cazzzoooo"
    bamhandler = pysam.Samfile(bam, "rb")
    #print bamhandler
    #ipdb.set_trace()
    for read in bamhandler:
        #print read
        if read.tid == -1:
            continue

        #print read.qname
        alignment = Alignment(read, bamhandler)
        #print alignment
        yield alignment



def populate_align_dict(bam):
    align_dict = {}

    aln_generator = get_alignments(bam)

    for alignment in aln_generator:

        try:
            align_dict[alignment.qname].append(alignment)
        except KeyError:
            align_dict[alignment.qname] = []
            align_dict[alignment.qname].append(alignment)

    return align_dict


def distribute_align(scafflist, align_dict):
    for scaffold in scafflist:
        for contig in scaffold.contigs:
            if contig.name in align_dict.keys():
                contig.alignments = align_dict[contig.name]
                for aln in contig:
                    aln.contig = contig


def define_blocks(scaffold_list, bamfile, **kwargs):
    #min_aln = kwargs.get('min_aln', 50)
    min_aln_dist  = kwargs.get('min_aln_dist', 100000)
    for scaffold in scaffold_list:

        aln_to_proc = []
        blocks = []
        for contig in scaffold:
            for alignment in contig:
                #print alignment
                aln_to_proc.append(alignment)
        bamhandler = pysam.Samfile(bamfile, "rb")

        # generation of blocks # forward and revrse separately
        for fr in [True, False]:
            for reference in bamhandler.header['SQ']:
                sub_list = [aln for aln in aln_to_proc if
                            (aln.ref == reference['SN'] and aln.scaff_reverse == fr)]
                sub_list.sort(key=lambda x: x.rstart)
                pointer = -10000000000

                for aln in sub_list:
                    if aln.rstart - pointer < min_aln_dist:
                        #print "appending"
                        blocks[-1].alignments.append(aln)

                    else:
                        blocks.append(Block(aln, scaffold))
                        #print "generating"
                    pointer = aln.rend

        scaffold.blocks = blocks


def merge_blocks(blocks, logger, **kwargs):

    min_delta_quantile = kwargs.get('min_delta_quantile', 0.05)
    abs_coeff_bound = kwargs.get('abs_coeff_bound', 0.85)
    # by now useless as they are splitted by strandness
    max_dist_ratio = kwargs.get('max_dist_ratio', 2.5)
    max_abs_dist = kwargs.get('max_abs_dist', 200000)
    # if abs dist is higher then threshold then apply dist_ratio

#     directory = outprefix + "_merging-logs"
#     if not os.path.exists(directory):
#         os.makedirs(directory)
#
#     logger = open(directory + "/" + blocks[0].scaffold.name + "_merging.log", "w")

    blocks.sort(key=lambda x: x.score, reverse=True)
    max_score = blocks[0].score # define a subset of best blocks to consider
    best_blocks = [ block for block in blocks if block.score > max_score * min_delta_quantile ]

    ref_dict = { refname : 1 for refname in [ block.refname for block in best_blocks ] }

    new_blocks = []

    for ref in ref_dict.keys():

        block_set_for = [ block for block in best_blocks if block.refname == ref and abs_coeff_bound <= block.coeff <= 1.0 ]
        block_set_rev = [ block for block in best_blocks if block.refname == ref and -1.0 <= block.coeff <= -abs_coeff_bound ]

        # sorted by query position
        block_set_for.sort(key=lambda x:x.orig_qstart) # the list of blocks is sorted,
        block_set_rev.sort(key=lambda x:x.orig_qstart) # not the block itself!

        for dir_blocks in [block_set_for, block_set_rev]:
            if len(dir_blocks) == 0:
                continue
            else:
                if dir_blocks == block_set_for:
                    ori = "FOR"
                else:
                    ori = "REV"
                logger.write("##\t%s\t%s\t%s\n" % (blocks[0].scaffold.name, ref, ori))


            prev_block = copy.deepcopy(dir_blocks[0])
            new_blocks.append(prev_block)

            #logger.write(str(new_blocks[-1]) + "\n")
            for i in range(1, len(dir_blocks)):
                this_block = copy.deepcopy(dir_blocks[i])
                logger.write(str(new_blocks[-1]) + "\n")
                logger.write(str(this_block) + "\n")

                if (this_block.orig_qstart >= new_blocks[-1].orig_qstart and
                    this_block.orig_qend <= new_blocks[-1].orig_qend):
                    new_blocks[-1].alignments += this_block.alignments
                    new_blocks[-1].merged_object = True
                    logger.write("ADSORBED\n")
                    continue #somehow its inner to the previous block, implicitally adsorbed

                query_dist = this_block.orig_qstart - new_blocks[-1].orig_qend # 2500

                #the only difference is when considering reference coordinates
                if dir_blocks == block_set_for:
                    ref_dist = this_block.rstart - new_blocks[-1].rend
                else:
                    ref_dist = new_blocks[-1].rstart - this_block.rend

                delta_dist = float(abs(query_dist - ref_dist))
                shortest_dist = float(min(abs(query_dist), abs(ref_dist)))
                if shortest_dist == 0:
                    shortest_dist = 1

                if ( delta_dist < max_abs_dist or delta_dist / shortest_dist < max_dist_ratio ):

                    # they can be merged
                    new_blocks[-1].alignments += this_block.alignments
                    new_blocks[-1].merged_object = True
                    #new_blocks[-1].alignments.sort(key=lambda x:x.rstart)

                    logger.write("##\t%s\t%s\t%s\tMERGED\n\n" % (query_dist, ref_dist, delta_dist))
                    # to maintain default ordering
                else:
                    logger.write(str(new_blocks[-1]) + "\n")
                    logger.write(str(this_block) + "\n")

                    logger.write("##\t%s\t%s\t%s\tNOT_MERGED\n\n" % (query_dist, ref_dist, delta_dist))
                    #prev_block = copy.deepcopy(this_block)
                    new_blocks.append(this_block)

    new_blocks.sort(key=lambda x:x.rstart) # put it in the original state  should not be necessary here
    return new_blocks


def merge_unassigned(scaff_list, prefix, **kwargs):
    min_score = kwargs.get('min_score', 0.20)
    min_fold = kwargs.get('min_fold', 3.0)

#     marged_h = outprefix + "_merging-logs.txt"
#     if not os.path.exists(directory):
#         os.makedirs(directory)

    merged_h = open(prefix + "_merging-logs.txt", "w")

    for scaffold in scaff_list:
        if scaffold.placed or len(scaffold.blocks) < 2:
            continue
        else:
            scaffold.merged_blocks = merge_blocks(scaffold.blocks, merged_h)

    merged_h.close()

    place_blocks( scaff_list,
                  blocks_to_run='merged_blocks',
                  min_score=min_score,
                  min_fold=min_fold )


def place_blocks(scaff_list, **kwargs):
    min_score = kwargs.get('min_score', 0.20)
    min_fold = kwargs.get('min_fold', 3.0)
    blocks_to_run = kwargs.get('blocks_to_run', 'blocks')
    #to_report = kwargs.get('to_report', 1)

    for scaffold in scaff_list:
        if scaffold.placed:
            continue # escape scaffolds that have already been placed in previous call of the function
        try:
            selec_blocks = getattr(scaffold, blocks_to_run)
        except AttributeError:
            scaffold.placed = False
            continue

        if len(selec_blocks) == 0:
            scaffold.placed = False
            continue


        elif len(selec_blocks) == 1:
            if selec_blocks[0].score >= min_score:
                scaffold.placed = True
                scaffold.goldenblock = selec_blocks[0]
            else:
                scaffold.placed = False

        elif len(selec_blocks) > 1:
            selec_blocks.sort(key=lambda x:x.score, reverse=True)
            if (selec_blocks[0].score >= min_score and
                selec_blocks[0].score / selec_blocks[1].score >= min_fold):
                scaffold.placed = True
                scaffold.goldenblock = selec_blocks[0]
            else:
                scaffold.placed = False



def write_results(scaff_list, out_prefix):
    best_h = open(out_prefix + "_placed.txt", 'w')
    ambig_h = open(out_prefix + "_ambiguous.txt", 'w')
    unalign_h = open(out_prefix + "_unaligned.txt", 'w')
    pre_merge_h = open(out_prefix + "_pre-merging.txt", 'w')
    log_h = open(out_prefix + "_log.txt", 'w')
    aligns_h = open(out_prefix + "_aligns.bed", 'w')

    placed = 0
    placed_bp = 0
    anchored_bp = 0
    unplaced = 0
    unplaced_bp = 0

    for i in [best_h, ambig_h, pre_merge_h]:
        i.write(Block.header + "\n")

    for scaffold in scaff_list:
        if scaffold.placed:
            best_h.write(str(scaffold.goldenblock) + "\n")
            if scaffold.goldenblock.merged_object:
                print "Writing best %s as merging result" % (scaffold.name)
                try:
                    for block in scaffold.blocks:
                        pre_merge_h.write(str(block) + "\n")
                except AttributeError:
                    print "Inconsistency for %s" % scaffold.name
            placed += 1
            placed_bp += scaffold.length
            anchored_bp += scaffold.goldenblock.scaffspans[0]

            write_aln(scaffold, aligns_h)

        elif not scaffold.placed and len(scaffold.blocks) > 0:
            unplaced += 1
            unplaced_bp += scaffold.length
            for block in scaffold.blocks:
                ambig_h.write(str(block) + "\n")
#
        elif len(scaffold.blocks) == 0:
                unplaced += 1
                unplaced_bp += scaffold.length
                unalign_h.write(str(scaffold.name) + "\n")

    best_h.close()
    ambig_h.close()
    unalign_h.close()
    pre_merge_h.close()
    aligns_h.close()

    line = "===== REPORT =====\n"
    line += "Placed %s scaffolds, representing %s bp of sequence, %s have been anchored\n" % (placed, placed_bp, anchored_bp)
    line += "Unable to place %s scaffolds, containing %s bp of sequence\n" % (unplaced, unplaced_bp)

    print line
    log_h.write( "\t".join( ["placed queries",
                             "placed queries tot length",
                             "anchored queries length",
                             "unplaced queries",
                             "unplaced queries tot length"]) + "\n" )
    log_h.write("\t".join( [ str(placed),
                             str(placed_bp),
                             str(anchored_bp),
                             str(unplaced),
                             str(unplaced_bp) ] ) )
    log_h.close()





def write_aln(scaffold, file_handler):
    """
    export alignments of the golden block for each placed scaffold
    in bed-12 format
    """
    block = scaffold.goldenblock
    for aln in block.alignments:
        strand = '+'
        if aln.reverse:
            strand = '-'
        file_handler.write( '\t'.join( [ str(aln.ref),
                                     str(aln.rstart),
                                     str(aln.rend),
                                     str(aln.qname),
                                     str(block.score),
                                     strand,
                                     '1',
                                     str(aln.raligned + 1),
                                     '255,0,0',
                                     '1',
                                     str(aln.raligned),
                                     '0',
                                     str(scaffold.name) ] ) + '\n' )







def ts():
    ts = time.time()
    ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return ts


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='NAIVE Map Allpaths scaffolds' + \
                                     'via contig align with DENOM')
    parser.add_argument('--denom-bam', dest='bam',
                        required=True,
                        help='BAM file with aligned contigs by DENOM')
    parser.add_argument('--final-summary', dest='summary',
                        required=False, default=False,
                        help='final.summary file from ALLPATHS-LG')
    parser.add_argument('--sspace-evidence', dest='sspace',
                        required=False, default=False,
                        help='evidence file from SSPACE')
    parser.add_argument('--contig-fasta', dest='fasta',
                        required=False,
                        help='FASTA containing aligned contigs')
    parser.add_argument('--max-aln-gap', dest='min_aln_dist',
                        default=50000, type=int,
                        help='Max distance between alignments to break blocks')
    parser.add_argument('--out', dest='out', required=True,
                        help='Output prefix')
    my_args = parser.parse_args()

    ################

    pp = PrettyPrinter(indent=4)

    if (not (my_args.summary or my_args.sspace) or
        (my_args.summary and my_args.sspace)):
        sys.exit("Please provide scaffolding information (--space-evidence or --final-summary)")

    if my_args.sspace and not my_args.fasta:
        sys.exit("Need to provide fasta for sspace mode")

    print "[%s] Staging in data..." % (ts())
    if my_args.summary:
        scaff_list = read_allpaths_summary(my_args.summary, my_args.fasta)
    elif my_args.sspace:
        scaff_list = read_sspace(my_args.sspace, my_args.fasta)
    else:
        sys.exit("sspace or allpaths?")


    align_dict = populate_align_dict(my_args.bam)

    distribute_align(scaff_list, align_dict)


    print "[%s] Defining blocks..." % (ts())
    define_blocks(scaff_list, my_args.bam, min_aln_dist=my_args.min_aln_dist)

    print "[%s] Easy placing..." % (ts())
    place_blocks(scaff_list, min_score=0.30, min_fold=4.0)
    print "[%s] Rescuing by merging..." % (ts())
    merge_unassigned(scaff_list, my_args.out,
                     min_score=0.10, min_fold=2.0)
    print "[%s] Writing results" % (ts())
    write_results(scaff_list, my_args.out)

    #ipdb.set_trace()

