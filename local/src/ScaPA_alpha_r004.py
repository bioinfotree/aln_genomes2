#!/usr/bin/env python

#import ipdb
import re
import argparse
import pysam
import sys
import copy
import time
import datetime
#import os
import collections
import pickle
import math
from Bio.Blast import NCBIXML
from Bio.Blast.Record import HSP
#import operator
#import numpy as np
from warnings import warn
#



'''

ScaPA - Scaffold Placement Algorithm (beta)

Scripts to place de novo assembly scaffolds over a complex genome and reconstruct tentative pseudomolecules.
It relies on alignments produced by DENOM algortithm, using contigs as query. The scripts takes as input 
- A BAM file of contigs aligned over a close-relative referece genome
- Scaffolding information of those contigs (either in AllPaths-LG or SSPACE format)
- Fasta file containing original contigs. Mostly to reconstruct naming of SSPACE scaffolder output (same order expected)



author: dscaglione@igatechnology.com

-- r004 changelog --
12-5-2015 - BLAST compability added, generate xml output with this type of command line:


blastn \
-task blastn \
-query ${query_fasta} \
-db ${dbname} \
-dust yes \
-perc_identity 90 \
-outfmt 5 \
-evalue 1e-30 > ${outprefix}.xml


The script can now take multiple files as alignment evidences, 
also mixtures of DENOM and BLAST output!


-- r003 changelog --

15-1-2015 - Resolved incosistency bug in split output
            Implementation of AGP dump for unanchored scaffolds/contigs
            
          - Implemented white list of big reference sequences to force alignment against other position on 
            secondary references (small unanchored scaffold of reference). If triggered scaffold will be a
            full-placed for the white_listed scaffold



-- r002 changelog --

28-11-2014 - IMPROVEMENT: Added extra steps where merging of blocks is executed with the aim to collapse 
             discordant-orientated blocks caused by sample or reverse inversion (original mis-assembly possibly)

25-11-2014 - IMPROVEMENT: Ability to export results in agp format

21-11-2014 - BUGFIX: Removed no-sense inclusion of alignments in adsorbed contigs spans during block merging, 
                It was only looking at scaff corrds, not genomic context, thus it was dropping scores resonless.
20-11-2014 - BUGFIX: Fixed wrong orig_qstart and orig_qend of blocks, last gap was missing in accumulation



'''


class Block(object):
    
    # blocks are originally sorted by reference coordinates
    def __init__(self, alignment, scaffold):
        self.alignments = [alignment]
        self.scaffold = scaffold
        #self.merged_object = False
        self.is_split = False
        self.tags = []
        
    @property
    def included_contigs(self):
        aligned_contigs = []
        for aln in self.alignments:
            if aln.contig.name not in aligned_contigs:
                aligned_contigs.append(aln.contig.name)
        
        the_scaffobj = self.scaffold
        all_contigs_obj = the_scaffobj.contigs
        
        indexes = []
        
        for aligned_contig in aligned_contigs:
            indexes.append([ x.name for x in all_contigs_obj ].index(aligned_contig))
        
        included = []
        for i in range(min(indexes), max(indexes) + 1):
            included.append(the_scaffobj.contigs[i].name)
        #print included
        return included      
    
    @property
    def refname(self):
        return self.alignments[0].ref
    
    @property
    def alignments_qorder(self):
        qsorted = []
        for contig in self.scaffold:
            
            contig_alns = [aln for aln in self.alignments if aln.qname == contig.name]
            contig_alns.sort(key=lambda x:x.orig_qstart)
            qsorted += contig_alns
            
        return qsorted 
    
    @property
    def orig_qstart(self):
        first_aln = self.alignments_qorder[0]
        partial = first_aln.orig_qstart
        scaffobj = self.scaffold
        for i in range(len(scaffobj.contigs)):
            if scaffobj.contigs[i].name == first_aln.qname:
                return partial + scaffobj.newgaps[i]
            else:
                partial += scaffobj.lengths[i] + scaffobj.newgaps[i]
        raise Exception("Segmentation fault: block contig not in scaffold order") 
        
    
    @property
    def orig_qend(self):
        last_aln = self.alignments_qorder[-1]
        partial = last_aln.orig_qend
        scaffobj = self.scaffold
        for i in range(len(scaffobj.contigs)):
            if scaffobj.contigs[i].name == last_aln.qname:
                return partial + scaffobj.newgaps[i]
            else:
                partial += scaffobj.lengths[i] + scaffobj.newgaps[i]
        raise Exception("Segmentation fault: block contig not in scaffold order") 

        
    @property
    def aligned(self):
        aligned = 0
        for aln in self.alignments:
            aligned += aln.qaligned
        return aligned


    @property
    def rspan(self):
        return self.rend - self.rstart
    
    
    @property
    def scaffspans(self):
        first_aln = self.alignments_qorder[0]
        last_aln = self.alignments_qorder[-1]
        

            
        
        first_cont_start = first_aln.orig_qstart
        last_cont_end = last_aln.orig_qend
        
        # going back to scaffold object where gaps info is stored
        a = [x.name for x in self.scaffold.contigs].index(first_aln.qname) 
        b = [x.name for x in self.scaffold.contigs].index(last_aln.qname)
        
            
        if a == b:
        #same contig
            span = last_cont_end - first_cont_start    
            return span, span, 0
            
        #print a
        #print b
        first_cont_span = self.scaffold.lengths[a] - first_cont_start
        last_cont_span = last_cont_end
        
        glob_span = 0
        seq_span = 0
        gaps = 0
        glob_span += last_cont_span + first_cont_span
        seq_span += last_cont_span + first_cont_span

        #no cycle if a + 1 == b
        for i in range(a + 1, b):
            # omitting the first and the last
            seq_span += self.scaffold.lengths[i]
            glob_span += self.scaffold.lengths[i]
            glob_span += self.scaffold.newgaps[i]
            gaps += self.scaffold.newgaps[i]
        
        return glob_span + 1, seq_span + 1, gaps
            
    
    @property #fraction of the scaffold, gap included, in this block
    def glob_scaff_coverage(self): 
        return float(self.scaffspans[0]) / self.scaffold.length
    
    @property #percentage of ACGT sequence in the block region that has alignment
    def anchored_perc(self):
        if self.scaffspans[1] != 0: # mvidotto modified here
            return float(self.aligned) / self.scaffspans[1] # mvidotto modified here
        warn("scaffold %s has scaffspans equal to: %s" %(self.scaffold, self.scaffspans[1]), RuntimeWarning)
        return 0

    @property
    def parsimony(self):
        a = min(self.scaffspans[0], self.rspan)
        b = max(self.scaffspans[0], self.rspan)
        
        return float(abs(a)) / abs(b)
        
    @property
    def identity(self):
        differ = 0
        total = self.aligned
        for aln in self.alignments:
            differ += aln.mismatches
            #differ += aln.indels # mismatches includes SNP and INDELs
        return (float(total) - float(differ)) / total
    
    @property
    def rstart(self):
        self.alignments.sort(key=lambda x:x.rstart)
        return self.alignments[0].rstart
    
    @property
    def rend(self):
        self.alignments.sort(key=lambda x:x.rstart)
        return self.alignments[-1].rend


    @property
    def coeff(self):
        acc = 0
        for aln in self.alignments:
            if aln.scaff_reverse:
                aaa = aln.qaligned * -1
            else:
                aaa = aln.qaligned
            acc += aaa
        return float(acc) / self.aligned
    
    @property
    def score(self):
        a = self.glob_scaff_coverage
        b = self.parsimony
        c = self.anchored_perc
        d = self.identity
        #e = len(self.alignments)
        
        return a * math.sqrt(b) * c * d
    

    fields = ["query_scaff",  "query_start", "query_end", "query_span", "scaffold_length",
              "subj_chr", "chr_start", "chr_end", "chr_span",
              "query_coverage", "parsimony", "orientation", "alignment-anchored", 
              "SCORE", "identity", "glob_span", "seq_span", 
              "gaps_cumul", "self.aligned", "#alignments", "tags"]
    header = "\t".join(fields)

    
    
    def __str__(self):
        query_span = self.orig_qend - self.orig_qstart + 1
        
        data = [self.scaffold, self.orig_qstart, self.orig_qend, query_span, self.scaffold.length,
                self.refname, self.rstart, self.rend, self.rspan,
                self.glob_scaff_coverage, self.parsimony, self.coeff, self.anchored_perc,
                self.score, self.identity, self.scaffspans[0], self.scaffspans[1], 
                self.scaffspans[2], self.aligned, len(self.alignments), ";".join(self.tags)]
                
        data = map(lambda x:str(x), data)
        line = "\t".join(data)
        return line
    

class Scaffold(object):

    def __init__(self, name):
        self.counter = 0
        self.name = name
        self.contigs = []
        self.pos = []      # pos will be based on new gaps
        self.ingaps = []    # contains gaps before each contig
        self.newgaps = []   # will contain values as expected by ALLPATH/SSPACE behavior in scaffold writing
        self.lengths = []
        
        self.splitted = False
        self.placed = False
        self.goldenblock = False
        
    def __str__(self):
        return self.name

    

    def add_contig(self, scaffobj, name, gap,
                   length, min_gap, rank, reverse):
        
        #seq = fastaobj.fetch(name)
        contig = Contig(scaffobj, name, rank, reverse)
        self.contigs.append(contig)
        
        if len(self.contigs) == 1:
            outgap = 0
            ingap = 0
            pos = 1
        elif gap < min_gap:
            outgap = min_gap
            ingap = gap
            pos = self.pos[-1] + self.lengths[-1] + outgap
        else:
            outgap = gap
            ingap = gap
            pos = self.pos[-1] + self.lengths[-1] + outgap
            
        self.ingaps.append(ingap)
        self.newgaps.append(outgap)
        self.lengths.append(length)
        self.pos.append(pos)
    #
    def __iter__(self):
        #return self
        for contig in self.contigs:
            yield contig
    
    @property
    def length(self):
        length = 0 
        if len(self.newgaps) == 0:
            raise Exception("Uncalculated gaps!")
        assert len(self.newgaps) == len(self.contigs)
        for i in range(len(self.contigs)):
            length += self.lengths[i]
            length += self.newgaps[i]
        return length
    
    def reset(self):
        self.placed = False
        self.splitted = False
        self.goldenblock = False

class Contig(object):
    
    def __init__(self, scaffobj, name, rank, reverse):
        #self.counter = 0
        self.scaffold = scaffobj
        self.name = name
        self.alignments=[]
        self.rank = rank
        self.reverse = reverse
        
    def __str__(self):
        return self.name    

    def __iter__(self):
        #return self
        for alignment in self.alignments:
            yield alignment
    

class Alignment(object):
    
    def __init__(self, element, **kwargs):
#         isinstance guarda a le classi da cui eredita, 
#         mentre type e' secco su quella classa
#         aln_attributes = {
#             'qname': '',
#         }
        
        
        if isinstance(element, pysam.calignmentfile.AlignedSegment): 
            self.load_from_bam(element, **kwargs)
        elif isinstance(element, HSP):
            self.load_from_blast(element, **kwargs)
        
        self.identity = float(self.qaligned - self.mismatches) / self.qaligned
        #print self.identity
        #print self.qaligned

#         assert(aln_attributes['qname'] != "")
#         for attr_name, attr_value in aln_attributes.iteritems():
#             setattr(self, attr_name, attr_value)
        #read = element
        
    
    def load_from_bam(self, read, **kwargs):
        bamhandler = kwargs.get('bamhandler')
        self.qname = read.qname
        self.contig = None
        self.ref = bamhandler.getrname(read.tid)
        #self.ref = read
        self.rstart = read.pos + 1
        self.rend = int(read.aend)
        self.cigar = read.cigar
        self.reverse = read.is_reverse
        self.qlength = read.rlen
        
        # aligned part, also considering deleted bases from the ref
        #self.qaligned = read.qlen
        #self.tlen = read.tlen
        
        for tag in read.tags:
            if tag[0] == 'NM':  # also counts for indels
                self.mismatches = tag[1]

        accumulator = 0
        left_clip = 0
        right_clip = 0
        indels = 0
        start = False
        end = False

        for i, pattern in enumerate(self.cigar):
            if pattern[0] in [3, 4, 5] and not start:
                left_clip += pattern[1]
            elif pattern[0] in [0, 1, 6, 7, 8] and not start:
                start = left_clip + 1
                accumulator += pattern[1]
            elif pattern[0] in [0, 1, 6, 7, 8]:
                accumulator += pattern[1]
            elif pattern[0] in [3, 4, 5] and not end:
                end =  left_clip + accumulator
                right_clip += pattern[1]
            elif pattern[0] in [3, 4, 5]:
                right_clip += pattern[1]
            elif pattern[0] == 2:
                pass
                #deletion from the reference
            else:    
                sys.exit("The code is not handling this CIGAR, please debug")
                
            if pattern[0] in [1, 2]:
                indels += pattern[1]
        
        self.indels = indels
        self.qstart = start
        #self.qstart2 = read.qstart # mine is 1-based, pysam uses 0-based
        if not end:
            self.qend = read.rlen
        else:
            self.qend = end
        #self.qend2 = read.qend
        self.qaligned = accumulator  # this equal to read.qlen
        self.raligned = read.alen
        
        #self.left_clip = left_clip
        #self.right_clip = right_clip
        
        
    def load_from_blast(self, hsp, **kwargs):    
        
            
            qname = kwargs.get('qname')
            qlength = kwargs.get('qlength')
            ref = kwargs.get('ref')
        
            self.qname = qname
            self.contig = None
            self.ref = ref
            #self.ref = read
            
            self.rstart = min(hsp.sbjct_start, hsp.sbjct_end)
            self.rend = max(hsp.sbjct_start, hsp.sbjct_end)
            self.qstart = hsp.query_start  # query is always plus in blastn
            self.qend = hsp.query_end
            
            self.cigar = None
            
            if self.rstart == hsp.sbjct_start:
                self.reverse = False
                #print "A forward alignment"
            else:
                self.reverse = True
                #print "A reverse alignment"
            self.qlength = qlength
            
            self.qaligned = self.qend - self.qstart + 1
            self.mismatches = hsp.align_length - hsp.identities
            self.indels = hsp.gaps

            self.raligned = self.rend - self.rstart + 1
            
            #self.left_clip = self.qstart - 1
            #self.right_clip = self.qlength - self.qend
        
        
        
        
    @property
    def scaff_reverse(self):
        if ((self.reverse and self.contig.reverse) or
            (not self.reverse and not self.contig.reverse)):
            return False
        elif ((self.reverse and not self.contig.reverse) or
            (not self.reverse and self.contig.reverse)):
            return True    
            
    
    @property
    def orig_qstart(self):    
        if self.scaff_reverse:
            return self.qlength - self.qend + 1
        else:
            return self.qstart
    @property
    def orig_qend(self):    
        if self.scaff_reverse:
            return self.qlength - self.qstart + 1
        else:
            return self.qend
      



def copy_block(block):
    new_block = copy.copy(block)
    new_alig_list = block.alignments[:]
    new_block.alignments = new_alig_list
    return new_block

'''

class Contig_seq(object):
    
    def __init__(self, name, sequence):
        self.name
        self.seq
        

def read_contigs(fasta_file):
    fastaobj = pysam.Fastafile(fasta_file)
    
    ordict = collections.OrderedDict()
    for ref in fastaobj.references:
        sequence = fastaobj.fetch(ref)
        mycontig = Contig_seq(ref, sequence)
        ordict[ref] = mycontig 
'''

def read_sspace(sspace, fasta_file, **kwargs):
    fastaobj = pysam.Fastafile(fasta_file)
    list_of_ref = fastaobj.references
    #print list_of_ref
    sspace_h = open(sspace, 'r')
    scaffoldlist = []
    memo = {}
    
    
    for line in sspace_h.readlines():
        min_gap = kwargs.get('min_gap', 1)
        if not line.split():
            continue
        info = line.rstrip().split("|")
        #print info
        if info[0].startswith(">"):    
            counter = 1
            memo = {'merged': False,
                    'gaps': 0,
                    'links': 0}

            name = info[0][1:]
            #print name
            scaffoldlist.append(Scaffold(name))
        else:
            m = re.match(r'(f|r)_tig(\d+)', info[0])
            if m.group(1) == "f":
                reverse = False
            else:
                reverse = True
            tignum = int(m.group(2))
            contig_name = list_of_ref[tignum - 1]
            m = re.match(r'size(\d+)', info[1])
            length = int(m.group(1))
            if len(info) > 2:
                m = re.match(r'links(\d+)', info[2])
                links = int(m.group(1))
            else:
                links = False
            if len(info) > 3:
                m = re.match(r'gaps(-?\d+)', info[3])
                gaps = int(m.group(1))
            else:
                gaps = False
            if len(info) > 4:
                m = re.match(r'merged(\d+)', info[4])
                merged = int(m.group(1))
            else:
                merged = False
            
            
            if memo['merged']:
                min_gap = -10000000000
                memo['gaps'] = -memo['merged']
            
            
            scaffoldlist[-1].add_contig(scaffoldlist[-1], contig_name, memo['gaps'],
                                        length, min_gap, counter, reverse)
            
            counter += 1
            
            memo = {'links': links, 
                    'gaps': gaps, 
                    'merged': merged}
     
    return scaffoldlist


def read_allpaths_summary(summary, fasta_file, **kwargs):
    
    
    min_gap = kwargs.get('min_gap', 1)
    summary_h = open(summary, 'r')
    
    scaffoldlist = []
    
    for line in summary_h.readlines():
        line = line.rstrip()
        #print line
        if line.startswith('scaffold'):
            counter = 1
            parts = line.split(" ")
            name = parts[0] + "_" + parts[1]
            #print name
            scaffoldlist.append(Scaffold(name))
        

        elif line != "" and not re.match('^[a-zA-Z].*', line):
            try:
                #scaffoldlist[-1]
                reverse = False #ALLPATHS output is always with contigs in FOR..
                mytuple = ((scaffoldlist[-1], ) +
                          parse_allpaths_line(line) + # this tuble contains contig_name, gap and length
                          (min_gap, counter, reverse)) 
                
                scaffoldlist[-1].add_contig(*mytuple)
                counter += 1
                #parse_allpaths_line(line) 
                pass
                #print line
            except IndexError:
                continue
     
    return scaffoldlist

        
def parse_allpaths_line(line):
    #print line
    if line.startswith(" -- "):
        m = re.match(r' -- \((-?\d+) \+\/\- \d+\) --> (\d+) \(l = (\d+)\)', line)
        gap = int(m.group(1))
        contig = "contig_" + m.group(2)
        length = int(m.group(3))
    ##
    else:
        m = re.match(r'(\d+) \(l = (\d+)\)', line)
        gap = 0
        contig = "contig_" + m.group(1)
        length = int(m.group(2))
        
    return (contig, gap, length)


# def get_ref_lengths(file):
#     bamhandler = pysam.Samfile(bamfile, "rb")
#     ref_dict = {}
#     for reference in bamhandler.header['SQ']:
#         element = reference['SN']
#         length = int(reference['LN'])
#         ref_dict[element] = length
#     return ref_dict


def alignments_generator_bam(bam):
    bamhandler = pysam.Samfile(bam, "rb")
    for read in bamhandler:
        if read.tid == -1:
            continue
        
        alignment = Alignment(read, bamhandler=bamhandler)
        yield alignment
 
def alignments_generator_blast(blast_xml):
    result_handle = open(blast_xml)
    blast_records = NCBIXML.parse(result_handle)
    
    for blast_record in blast_records:
        qlength = blast_record.query_length
        qname = blast_record.query.split()[0]
        #rint qname
        alignments = blast_record.alignments
        for blast_aln in alignments:
            ref = blast_aln.hit_def
            #print ref
            hsps = blast_aln.hsps
            for hsp in hsps:
                alignment = Alignment(hsp, qname=qname, ref=ref, qlength=qlength )
                yield alignment



def populate_align_dict(infile, align_dict, ref_dict_names, min_aln_len, min_identity):

    if infile.endswith(".bam"):
        aln_generator = alignments_generator_bam(infile)
    elif infile.endswith(".xml"):
        aln_generator = alignments_generator_blast(infile)
    else:
        sys.exit('Unrecognized file extension for %s' % infile)
    
    
    for alignment in aln_generator:
        ref_dict_names[alignment.ref] = 1
        
        
        #print alignment.identity
        
        if (not alignment.qaligned >= min_aln_len or 
            not alignment.identity >= min_identity):
            #print "discarding"
            continue
        
        try:
            align_dict[alignment.qname].append(alignment)
            #print "appending to dict"
        except KeyError:
            #print "generating qname key in dict"
            align_dict[alignment.qname] = []
            align_dict[alignment.qname].append(alignment)
   



def distribute_align(scafflist, align_dict):
    for scaffold in scafflist:
        for contig in scaffold.contigs:
            if contig.name in align_dict.keys():
                contig.alignments = align_dict[contig.name]
                for aln in contig:
                    aln.contig = contig


def define_blocks(scaffold_list, ref_dict_names, **kwargs):
    #min_aln = kwargs.get('min_aln', 50)
    min_aln_dist  = kwargs.get('min_aln_dist', 50000)
    for scaffold in scaffold_list:
        
        aln_to_proc = []
        blocks = []
        for contig in scaffold:
            for alignment in contig:
                #print alignment
                aln_to_proc.append(alignment)
        #bamhandler = pysam.Samfile(bamfile, "rb")

        # generation of blocks # forward and revrse separately
        for is_reverse in [True, False]:
            for reference in ref_dict_names.keys():
                sub_list = [aln for aln in aln_to_proc if 
                            (aln.ref == reference and aln.scaff_reverse == is_reverse)]
                sub_list.sort(key=lambda x: x.rstart)
                pointer = -10000000000
 
                for aln in sub_list:
                    if aln.rstart - pointer < min_aln_dist:
                        #print "appending"
                        blocks[-1].alignments.append(aln)
                        
                    else:
                        blocks.append(Block(aln, scaffold))
                        #print "generating"
                    pointer = aln.rend
        
        scaffold.blocks = blocks


def place_blocks(scaff_list, **kwargs):
    min_score = kwargs.get('min_score', 0.20)
    min_fold = kwargs.get('min_fold', 3.0)
    blocks_to_run = kwargs.get('blocks_to_run', 'blocks') 
    #to_report = kwargs.get('to_report', 1)
    #print "ciao" #DEBUG
    for scaffold in scaff_list:
        if scaffold.placed:
            #print "already placed" #DEBUG
            continue
            #escape scaffolds those that have already been placed
        try:    
            selec_blocks = getattr(scaffold, blocks_to_run)
        except AttributeError:
            scaffold.placed = False
            #print "can't get attibutes" ## DEBUG
            continue
            
        if len(selec_blocks) == 0:
            #print "no_select" #BEBUG
            scaffold.placed = False
            continue
            
            
        elif len(selec_blocks) == 1:
            #print "only_one" #BEBUG
            if selec_blocks[0].score >= min_score:
                scaffold.placed = True
                scaffold.goldenblock = selec_blocks[0]
            else:
                scaffold.placed = False
                
        elif len(selec_blocks) > 1:
            selec_blocks.sort(key=lambda x:x.score, reverse=True)
            #print selec_blocks[0].score ## DEBUG
            if (selec_blocks[0].score >= min_score and
                selec_blocks[0].score / selec_blocks[1].score >= min_fold):
                scaffold.placed = True
                scaffold.goldenblock = selec_blocks[0]
                #print "Placing" ## DEBUG
                #print selec_blocks[0].score ## DEBUG
            else:
                scaffold.placed = False
                #print "notthing"


def merge_blocks(blocks, outprefix, logger, **kwargs):
    min_delta_quantile = kwargs.get('min_delta_quantile', 0.10)
    abs_coeff_bound = kwargs.get('abs_coeff_bound', 0.85)
    # by now useless as they are splitted by strandness
    max_dist_ratio = kwargs.get('max_dist_ratio', 3.0)
    max_abs_dist = kwargs.get('max_abs_dist', 250000)
    strand_specific = kwargs.get('strand_specific', True)
    # if abs dist is higher then threshold then apply dist_ratio
    

    blocks.sort(key=lambda x: x.score, reverse=True)
    max_score = blocks[0].score # define a subset of best blocks to consider
    best_blocks = [block for block in blocks if block.score > max_score * min_delta_quantile]
    ref_dict = {refname : 1 for refname in [block.refname for block in best_blocks]}
    
    new_blocks = []
    
    for ref in ref_dict.keys():
        
        #
        block_set_for = [block for block in best_blocks if 
                         block.refname == ref and abs_coeff_bound <= block.coeff <= 1.0 ]
        block_set_rev = [block for block in best_blocks if 
                         block.refname == ref and -1.0 <= block.coeff <= -abs_coeff_bound ]
        # sorted by query position
        block_set_for.sort(key=lambda x:x.orig_qstart) # the list of blocks is sorted, 
        block_set_rev.sort(key=lambda x:x.orig_qstart) # not the block itself!

        if not strand_specific:
            #print block_set_for
            #print block_set_rev
            dir_list = [block_set_for + block_set_rev]
            dir_list[0].sort(key=lambda x:x.orig_qstart)
            #print dir_list
        else:
            dir_list = [block_set_for, block_set_rev]
        
        for dir_blocks in dir_list:
            if len(dir_blocks) == 0:
                continue
            else:
                if strand_specific and dir_blocks == block_set_for:
                    ori = "FOR"
                elif strand_specific:
                    ori = "REV"
                else:
                    ori = "ADMIXED orientation" 
                logger.write("======== %s -- Orientation: %s\n\n" % (ref, ori)) 
            
            
            prev_block = copy_block(dir_blocks[0])
            new_blocks.append(prev_block)
            #logger.write(str(new_blocks[-1]) + "\n")
            for i in range(1, len(dir_blocks)):
                this_block = copy_block(dir_blocks[i])
                logger.write(str(new_blocks[-1]) + "\n")
                logger.write(str(this_block) + "\n")
                
                if (this_block.orig_qstart >= new_blocks[-1].orig_qstart and 
                    this_block.orig_qend <= new_blocks[-1].orig_qend):
                    # absolutely not this generate shit
                    #new_blocks[-1].alignments += this_block.alignments
                    #new_blocks[-1].merged_object = True
                    new_blocks[-1].alignments += this_block.alignments
                    new_blocks[-1].tags.append("adsorbed")
                    logger.write("ADSORBED\n")
                    continue #somehow its inner to the previous block, no-question, adsorbed

                query_dist = max(0, this_block.orig_qstart - new_blocks[-1].orig_qend) # 2500
                #will get zero for overllaped-on-query blocks
                #the only difference is when considering reference coordinates
                if this_block.coeff > 0 and new_blocks[-1].coeff > 0:
                    #print 'TEST'
                    # forcing to positive diagonal behavior
                    ref_dist = this_block.rstart - new_blocks[-1].rend
                elif this_block.coeff < 0 and new_blocks[-1].coeff < 0:
                    # forcing to negative diagonal behavior
                    #print 'TEST2'
                    ref_dist = new_blocks[-1].rstart - this_block.rend
                # this cases should work only in mixed scenario of coeff
                elif new_blocks[-1].rstart <= this_block.rstart:
                    # moving from 
                    ref_dist = this_block.rstart - new_blocks[-1].rend
                else:
                    ref_dist = new_blocks[-1].rstart - this_block.rend
                #print ref_dist
                    
                delta_dist = float(abs(query_dist - ref_dist)) 
                shortest_dist = float(min(abs(query_dist), abs(ref_dist)))
                if shortest_dist == 0:
                    shortest_dist = 1
                
                if (delta_dist < max_abs_dist or 
                    delta_dist / shortest_dist < max_dist_ratio):
                    # they can be merged
                    new_blocks[-1].alignments += this_block.alignments
                    new_blocks[-1].tags.append("merged")
                    #new_blocks[-1].alignments.sort(key=lambda x:x.rstart)
                    logger.write("%s # %s # %s MERGED\n" % (query_dist, ref_dist, delta_dist))
                    #print "%s # %s # %s MERGED\n" % (query_dist, ref_dist, delta_dist)
                    # to maintain default ordering
                else:
                    #logger.write(str(new_blocks[-1]) + "\n")
                    #logger.write(str(this_block) + "\n")
                    logger.write("%s # %s # %s NOT MERGED\n" % (query_dist, ref_dist, delta_dist))
                    #print "%s # %s # %s NOT MERGED\n" % (query_dist, ref_dist, delta_dist)
                    #print delta_dist < max_abs_dist
                    #print delta_dist / shortest_dist
                    #prev_block = copy.deepcopy(this_block)
                    new_blocks.append(this_block)
                    
    new_blocks.sort(key=lambda x:x.rstart) 
    # put it in the original state should not be necessary here          
    #print new_blocks
    return new_blocks


def merge_unassigned(scaff_list, prefix, **kwargs):
    min_score = kwargs.get('min_score', 0.20)
    min_fold = kwargs.get('min_fold', 3.0)
    max_dist_ratio = kwargs.get('max_dist_ratio', 3.0)
    max_abs_dist = kwargs.get('max_abs_dist', 250000)
    min_delta_quantile = kwargs.get('min_delta_quantile', 0.10)
    logger = open(prefix + "_merging.log", "w")
    
    
    
    for scaffold in scaff_list:
        if scaffold.placed or len(scaffold.blocks) < 2:
            continue
        else:
            
            logger.write("###################### %s ######################\n\n" % scaffold.name)
            
            scaffold.merged_blocks = merge_blocks(scaffold.blocks, prefix, logger,
                                                  max_dist_ratio=max_dist_ratio,
                                                  max_abs_dist=max_abs_dist,
                                                  min_delta_quantile=min_delta_quantile)
            logger.write("\n\n\n\n")
            
    place_blocks(scaff_list, blocks_to_run='merged_blocks', min_score=min_score, min_fold=min_fold)


def calculate_blocks_overlap(block1, block2):
    if block1.refname != block2.refname:
        return 0

    s1 = block1.orig_qstart
    e1 = block1.orig_qend
    s2 = block2.orig_qstart
    e2 = block2.orig_qend
   
    if s2 > e1 or e2 < s1:
        return 0
    if s2 >= s1 and s2 <= e1:
        if e2 > e1:
            return e1 - s2 + 1 # return overlap
        else:
            return e2 - s2 + 1 # it's included
    if s2 <= s1 and s1 <= e2 <= e1:
        return e2 - s1 + 1
    if s2 <= s1 and e2 >= e1:
        return e1 -s1 + 1
    raise ("Segfault in overlap calculation")
        

def shave_neighboring_blocks(block1, block2):
    set1 = set(block1.included_contigs)
    set2 = set(block2.included_contigs)
    black_list = set.intersection(set1, set2)
    #print set1
    #print set2
    #print black_list
    for block in [block1, block2]:
        to_remove = []
        for aln in block.alignments:
            if aln.contig.name in black_list:
                
                to_remove.append(aln)
        for black_aln in to_remove:
            block.alignments.remove(black_aln)
        #print to_remove


def shave_neighboring_blocks_bestwins(block1, block2, white_list):
    set1 = set(block1.included_contigs)
    set2 = set(block2.included_contigs)
    black_list = set.intersection(set1, set2)
    #print "shaving"
    #print set1
    #print set2
    #print black_list
    #print block1.score
    #print block2.score
    
    [losing_block, win_block ] = sorted([block1, block2],key=lambda x:x.score)
    
    
    if (white_list and losing_block.refname in white_list and 
        not win_block.refname in white_list):
        #print "inverting"
        (losing_block, win_block) = (win_block, losing_block)
    
    to_remove = []
    for aln in losing_block.alignments:
        if aln.contig.name in black_list:
            to_remove.append(aln)
            
    for black_aln in to_remove:
        losing_block.alignments.remove(black_aln)
    #print to_remove

            
    
def safe_split(scaff_list, **kwargs):
    max_overlap_ratio = kwargs.get('max_overlap_ratio', 0.30) #with regard to shorter block
    min_score = kwargs.get('min_score', 0.10)
    blocks_to_run = kwargs.get('blocks_to_run', 'merged_blocks')
    white_list = kwargs.get('white_list', False)
    #print "ciao"
    for scaffold in scaff_list:
        split_blocks = []
        #print scaffold.name
        if scaffold.placed:
            
            continue
        
        try:
            work_on = getattr(scaffold, blocks_to_run)
            working_blocks = [copy_block(old) for old in work_on]
        except AttributeError:
            continue

        
        # else try to find misassemblies, merged blocks have already been filtered by score
        
        
        working_blocks = [block for block in working_blocks if block.score >= min_score]
        #print len(working_blocks)
        
        # this I assume that if a single block was eligible it shoulb be taken by the score ratio 
        # rule before (i.e. during merging of blocks)
        # here being only one does not means that the second was far away in term of score.
        if len(working_blocks) < 2:
            continue
        
        
        working_blocks.sort(key=lambda x:x.orig_qstart)
#         for block in working_blocks:
#             print "==>" + str(block.orig_qstart)
#             print block.included_contigs

        split_blocks.append(working_blocks[0])
        
        
        for i in range(1, len(working_blocks)):
            split_blocks.sort(key=lambda x:x.orig_qstart) # reorder since I saw that something weird can happen in nested working blocks
            #print len(split_blocks)
            # in case it was empty by the shaving...add one and go on
            if len(split_blocks) == 0:
                split_blocks.append(working_blocks[i])
                continue
            
            block_x = working_blocks[i]
            
            #print split_blocks[-1]
            #print block_x
            #ipdb.set_trace()
            
            l1 = split_blocks[-1].scaffspans[0]
            l2 = block_x.scaffspans[0]
            shorter = min([l1, l2])
            overlap = calculate_blocks_overlap(split_blocks[-1], block_x)
            
            if float(overlap) / shorter <= max_overlap_ratio:
                #print "entering if"
                # regardless they are on the same reference or not, append and try to shave
                split_blocks.append(block_x)
                
                shave_neighboring_blocks_bestwins(split_blocks[-1], split_blocks[-2], white_list) # (next, prev) is the order
                for block in (split_blocks[-1], split_blocks[-2]):
                    if len(block.alignments) == 0:
                        # if the result would be only one dum everything, something fishy occurred
                        #print "removing block"
                        split_blocks.remove(block)
                        #split_blocks.sort(key=lambda x:x.orig_qstart)
                # shave borders on the basis of contigs intersection
            else:
                # need to take only one! chose on score
                #print "entering else"
                if split_blocks[-1].score >= block_x.score:
                    continue #dropping block_x
                else:
                    split_blocks.pop() # remove last block
                    split_blocks.append(block_x) # replace with next one
            
        
        ### final absolute sanity check: drop everython if contigs for some reason remain duplicated
        memo = []
        for block in split_blocks:
            memo += block.included_contigs    
        if len([x for x, y in collections.Counter(memo).items() if y > 1]) > 0:
            split_blocks = []
            
            
#         print "###########"
#         for block in split_blocks:
#              
#             print block.orig_qstart
#             print block.included_contigs
            #print vars(split_blocks[-1])
            #print split_blocks[-1].orig_qstart
        #print len(split_blocks)
        # will also consider scaffold when 
        # only one block resuls after splitting (e.g. inner one (by query pos) get adsorbed)
        
        
        
        # filter on the basis of white_list of reference pseudomolecules
        to_remove = []
        set_of_ref = set([x.refname for x in split_blocks])
        # need to have at least one block on white_list to trigger removal of non white_list
        if white_list and len(set.intersection(set_of_ref, set(white_list))) > 0:
            print "checking in whitelist"
            for block in split_blocks:
                if not block.refname in white_list:
                    to_remove.append(block)
            for goaway in to_remove:
                split_blocks.remove(goaway)
        
        #print "number of split_blocks is %s" % str(len(split_blocks))
        
        
        split_blocks = [block for block in split_blocks if block.score >= min_score]
        
        if len(split_blocks) > 1: 
            for block in split_blocks:
                block.is_split = True
                block.tags.append("split")
            scaffold.split_blocks = split_blocks
            scaffold.splitted = True
            
            #print "splitted"
        elif len(split_blocks) == 1: #and white_list:
            #print "entering full attachment"
            scaffold.placed = True
            scaffold.goldenblock = split_blocks[0]
#         elif len(split_blocks) == 1:
#             print "entering splitted attachment"
#             scaffold.splitted = True
#             scaffold.split_blocks = split_blocks
        else:
            scaffold.splitted = False
            scaffold.placed = False


def resolve_inversions(scaff_list, out_prefix, **kwargs):
    min_score = kwargs.get('min_score', 0.05)
    min_fold = kwargs.get('min_fold', 5.0)
    min_delta_quantile = kwargs.get('min_delta_quantile', 0.20)
    # by now useless as they are splitted by strandness
    max_dist_ratio = kwargs.get('max_dist_ratio', 2.5)
    max_abs_distance = kwargs.get('max_abs_dist', 100000)
    logger = open(out_prefix + "_resolving_inversions.txt", "w")
    
    for scaffold in scaff_list:
        
        if scaffold.placed or scaffold.splitted or len(scaffold.blocks) < 2:
            continue
        
        blocks_in = getattr(scaffold, 'blocks') # just takes all blocks and apply more stringent criteria than regular merging
        
        #print "Using %s max distance" % max_abs_distance
        scaffold.merged_blocks = merge_blocks(blocks_in, out_prefix, logger,
                                              min_delta_quantile=min_delta_quantile,
                                              max_dist_ratio=max_dist_ratio,
                                              max_abs_dist=max_abs_distance,
                                              strand_specific=False)
        
        # only work on those that were not skipped    
        place_blocks([scaffold], blocks_to_run='merged_blocks',
                     min_score=min_score, min_fold=min_fold)


def write_results(scaff_list, out_prefix):
    best_h = open(out_prefix + "_placed-full.txt", 'w')
    ambig_h = open(out_prefix + "_ambiguous.txt", 'w')
    split_h = open(out_prefix + "_placed-splits.txt", 'w')
    unalign_h = open(out_prefix + "_unaligned.txt", 'w')
    #pre_merge_h = open(out_prefix + "_pre-merging.txt", 'w')
    log_h = open(out_prefix + "_log.txt", 'w')
    
    placed = 0
    placed_bp = 0
    anchored_bp = 0
    split_bp = 0
    split_count = 0
    split_leftover = 0   
    unplaced = 0
    unplaced_bp = 0     
    total_available = 0
    
    for i in [best_h, ambig_h, split_h]: #pre_merge_h]:
        i.write(Block.header + "\n")
        
    for scaffold in scaff_list:
        total_available += scaffold.length
        if scaffold.placed:
            best_h.write(str(scaffold.goldenblock) + "\n")
            placed += 1
            placed_bp += scaffold.length
            anchored_bp += scaffold.goldenblock.scaffspans[0]
#             if scaffold.goldenblock.merged_object:
#                 print "Writing best %s as merging result" % (scaffold.name)
#                 try:
#                     for block in scaffold.blocks:
#                         pre_merge_h.write(str(block) + "\n")
#                 except AttributeError:
#                     print "Inconsistency for %s" % scaffold.name

            
        
        elif scaffold.splitted:
            taken = 0
            split_count += 1
            for block in scaffold.split_blocks:
                split_h.write(str(block) + "\n")
                taken += block.scaffspans[0]
            split_bp += taken
            split_leftover += scaffold.length - taken    
            
            
        elif not scaffold.placed and len(scaffold.blocks) > 0:
            unplaced += 1
            unplaced_bp += scaffold.length
            for block in scaffold.blocks:
                ambig_h.write(str(block) + "\n")

        elif len(scaffold.blocks) == 0:
                unplaced += 1
                unplaced_bp += scaffold.length 
                unalign_h.write(str(scaffold.name) + "\n")

    
    total_placed = placed_bp + split_bp
    performance = 0
    if  total_available != 0: # mvidotto added
        performance = float(total_placed) / total_available * 100

    # line = "===== Results writing REPORT =====\n"            
    # line += "Placed %s full scaffolds, representing %s bp of sequence, %s have been anchored with SW\n" % (placed, placed_bp, anchored_bp)
    # line += "%s scaffolds subject to split, rescued %s bp of sequence, %s bp trimmed off\n" % (split_count, split_bp, split_leftover)
    # line += "Unable to place %s scaffolds, containing %s bp of sequence\n" % (unplaced, unplaced_bp)
    # line += "Total available assembly placed: %s bp, %s of total\n" % (total_placed, performance)

    line = "placed scaffolds:\t%s\n" % placed
    line += "placed bp:\t%s\n" % placed_bp
    line += "anchored bp:\t%s\n" % anchored_bp
    line += "splitted scaffolds:\t%s\n" % split_count
    line += "rescued bp:\t%s\n" % split_bp
    line += "trimmed off bp:\t%s\n" % split_leftover
    line += "unplaced scaffolds:\t%s\n" % unplaced
    line += "unplaced bp:\t%s\n" % unplaced_bp
    line += "total placed bp:\t%s\n" % total_placed
    line += "total placed %%:\t%s\n" % performance
    
    print line
    log_h.write(line)


def populate_agp(scaff_list, out_prefix, **kwargs):
    intergap = kwargs.get('intergap', 1000)
    use_split = kwargs.get('use_split', True)
    agp_h = open(out_prefix + '_pseudomol.agp', 'w')
    chr_block_dict = {}
    unused_contigs = []  # a list of tuples (name, length, orientation)
    # here I simply re-collect blocks by reference
    contigs_batch = 0
    
    for scaffold in scaff_list:
        contigs_batch += 1
        
        
        if scaffold.placed:
            #print scaffold.goldenblock.refname
            if scaffold.goldenblock.refname not in chr_block_dict.keys():
                chr_block_dict[scaffold.goldenblock.refname] = []
            chr_block_dict[scaffold.goldenblock.refname].append(scaffold.goldenblock)
        
        elif scaffold.splitted and use_split:
            
            used_contigs = []
            for block in scaffold.split_blocks:
                if block.refname not in chr_block_dict.keys():
                    chr_block_dict[block.refname] = []
                chr_block_dict[block.refname].append(block)
                used_contigs += block.included_contigs
            for i in range(len(scaffold.contigs)):
                if scaffold.contigs[i].name not in used_contigs:
                    
                    unused_contigs.append((scaffold.contigs[i].name,
                                           scaffold.lengths[i],
                                           scaffold.contigs[i].reverse,
                                           scaffold.newgaps[i], # here memorizing the gap after before unused contig
                                           contigs_batch)) #memo batch to put interscaffold gap accordingly
                else:
                    contigs_batch += 1      # change batch number when contiguity of unused is interrupted
        
        elif not scaffold.placed and not scaffold.splitted:    
            for i in range(len(scaffold.contigs)):
                #print "check"
                #print scaffold.contigs[i].reverse
                unused_contigs.append((scaffold.contigs[i].name, 
                                       scaffold.lengths[i],
                                       scaffold.contigs[i].reverse,
                                       scaffold.newgaps[i],
                                       contigs_batch))
    
    #print unused_contigs
    
    
    for ref in chr_block_dict.keys():
        counter = 0
        pointer = 0
        
        # here I sort them
        chr_block_dict[ref].sort(key=lambda x:x.rstart) # all aligned blocks ordered by reference start
        for ab in range(len(chr_block_dict[ref])):
            block = chr_block_dict[ref][ab]
            scaffold = block.scaffold
            ord_aln = block.alignments_qorder
            if scaffold.placed and not block.is_split:
                # will dump all contigs 
                start = 0
                end = len(block.scaffold.contigs)
            elif not scaffold.placed and scaffold.splitted: # some redundant checking
                #alignments oredered by query position
                first_aln = ord_aln[0]
                last_aln = ord_aln[-1]
                start = scaffold.contigs.index(first_aln.contig)
                end = scaffold.contigs.index(last_aln.contig)
                # collect unused contigs
                

            else: # something wrong happens
                print scaffold.name
                print scaffold.__repr__()
                print vars(scaffold)
                print block.__repr__()
                print vars(block)
                sys.exit("Placed or split?! Incosistend object attribute")
            #print (start, end)
            
            
            to_print = []
            #interblock gap
            if ab > 0 and ab < len(chr_block_dict[ref]):  # print before the contig/gaps part
                    sss = pointer + 1
                    eee = pointer + intergap
                    counter += 1
                    to_print.append(ref)
                    to_print.append(str(sss))
                    to_print.append(str(eee))
                    to_print.append(str(counter))
                    to_print.append("N")
                    to_print.append(str(intergap))
                    to_print.append("contig")
                    to_print.append("no")
                    to_print.append("na")
                    pointer = eee
                    line = "\t".join(to_print)
                    agp_h.write(line + '\n')
                    to_print = []
            
            
            contigs = block.scaffold.contigs[start:end + 1]
            lengths = block.scaffold.lengths[start:end + 1]
            gaps = block.scaffold.newgaps[start:end + 1]
            
            if block.coeff > 0:
                block_aln_minus = False
                # ride on as usual
            else:
                contigs.reverse()
                lengths.reverse()
                gaps.reverse()
                #print gaps
                tmp = gaps.pop() # tweak to maintaing gap as the one which preceed the contig
                gaps[0:0] = [tmp]
                block_aln_minus = True

            # now print contig and scaffold gap

            
            assert len(contigs) == len(gaps)
            
            
            to_print = []
            for i in range(len(contigs)):
                if i > 0: # to skip first contig, we dont need its preceding gap
                    sss = pointer + 1
                    eee = pointer + gaps[i]
                    counter += 1
                    to_print.append(ref)
                    to_print.append(str(sss))
                    to_print.append(str(eee))
                    to_print.append(str(counter))
                    to_print.append("N") 
                    to_print.append(str(gaps[i]))
                    to_print.append("scaffold")
                    to_print.append("yes")
                    to_print.append("paired-ends")
                    pointer = eee
                    line = "\t".join(to_print)
                    agp_h.write(line + '\n')
                    to_print = []
                    
                counter += 1
                sss = pointer + 1
                eee = pointer + lengths[i]
                to_print.append(ref)
                to_print.append(str(sss))
                to_print.append(str(eee))
                to_print.append(str(counter))
                to_print.append("W")
                to_print.append(contigs[i].name)
                to_print.append("1")
                to_print.append(str(lengths[i]))
                
                
                if ((block_aln_minus and contigs[i].reverse) or
                    (not block_aln_minus and not contigs[i].reverse)):
                    sign = "+"
                else:
                    sign = "-"
                
                to_print.append(sign)
                
                pointer = eee
                line = "\t".join(to_print)
                agp_h.write(line + '\n')
                to_print = []
    
    #### dumping unused contigs as unanchored ##############
    
    counter = 0
    pointer = 0
    prev_batch = ""
    for uc in range(len(unused_contigs)):
        
        if uc > 0 and unused_contigs[uc][4] != prev_batch:
            #inner_counter = 0
            sss = pointer + 1
            eee = pointer + intergap
            counter += 1
            to_print.append("Unanchored")
            to_print.append(str(sss))
            to_print.append(str(eee))
            to_print.append(str(counter))
            to_print.append("N")
            to_print.append(str(intergap))
            to_print.append("contig")
            to_print.append("no")
            to_print.append("na")
            pointer = eee
            line = "\t".join(to_print)
            agp_h.write(line + '\n')
            to_print = []
        
        
        
        
        elif uc > 0: # if unused_contigs[uc][4] = unused_contigs[uc + 1][4]:
            sss = pointer + 1
            eee = pointer + unused_contigs[uc][3]
            counter += 1
            to_print.append("Unanchored")
            to_print.append(str(sss))
            to_print.append(str(eee))
            to_print.append(str(counter))
            to_print.append("N") 
            to_print.append(str(unused_contigs[uc][3])) #pre gap size
            to_print.append("scaffold")
            to_print.append("yes")
            to_print.append("paired-ends")
            pointer = eee
            line = "\t".join(to_print)
            agp_h.write(line + '\n')
            to_print = []
        
        
        
        #print contig
        counter += 1
        sss = pointer + 1
        eee = pointer + unused_contigs[uc][1] # the length
        to_print.append("Unanchored")
        to_print.append(str(sss))
        to_print.append(str(eee))
        to_print.append(str(counter))
        to_print.append("W")
        to_print.append(unused_contigs[uc][0]) # contig name
        to_print.append("1")
        to_print.append(str(unused_contigs[uc][1])) # the length
        
        if unused_contigs[uc][2]: # is_reverse boolean
            sign = "-"
        else:
            sign = "+"
        
        to_print.append(sign)
        pointer = eee
        line = "\t".join(to_print)
        agp_h.write(line + '\n')
        to_print = []
        
        


            
        prev_batch = unused_contigs[uc][4]
                            
    
    agp_h.close()

def write_aln(scaff_list, out_prefix):
    """
    export alignments of the golden block for each placed scaffold
    in bed-12 format
    """
    aligns_h = open(out_prefix + "_placed.alignments.bed", 'w')

    for scaffold in scaff_list:
        if scaffold.placed:
            block = scaffold.goldenblock
            for aln in block.alignments:
                strand = '+'
                if aln.reverse:
                    strand = '-'
                aligns_h.write( '\t'.join( [ str(aln.ref),
                                             str(aln.rstart),
                                             str(aln.rend),
                                             str(aln.qname),
                                             str(block.score),
                                             strand,
                                             '1',
                                             str(aln.raligned + 1),
                                             '255,0,0',
                                             '1',
                                             str(aln.raligned),
                                             '0',
                                             str(scaffold.name) ] ) + '\n' )
    aligns_h.close()


def ts():
    ts = time.time()
    ts = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    return ts

    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Algorithm to place scaffolded contigs via' + \
                                     'BAM or BLAST alignments over chromosome-level sequences ' + \
                                     '(allpaths-LG and SSPACE scaffolding supported) ')
    parser.add_argument('--alignment-files', dest='aln_files',
                        required=True, nargs='*',
                        help='BAM / Blast XML files of aligned contigs described in scaffolds ' + \
                        '(can read multiple files and mixed data is allowed)')
    parser.add_argument('--final-summary', dest='summary',
                        required=False, default=False,
                        help='final.summary file from ALLPATHS-LG')
    parser.add_argument('--sspace-evidence', dest='sspace',
                        required=False, default=False,
                        help='evidence file from SSPACE')
    parser.add_argument('--contig-fasta', dest='fasta',
                        required=False,
                        help='FASTA containing aligned contigs; only required for SSPACE')
    parser.add_argument('--max-aln-gap', dest='min_aln_dist',
                        default=10000, type=int,
                        help='Max distance between alignments to break blocks (default: 10000)')
    parser.add_argument('--min-aln-len', dest='min_aln_len',
                        default=100, type=int,
                        help='Min length of alignment (HSP) to populate data (default: 100)')
    parser.add_argument('--min-identity', dest='min_identity',
                        default=0.90, type=float,
                        help='Min identity to consider align evidence (default: 0.90)')
    parser.add_argument('--min-score', dest='min_score',
                        default=0.10, type=float,
                        help='Min score of alignment blocks (default 0.10), this value is ' + \
                        'doubled for the "easy placing" phase (set it max at 0.50)')
    parser.add_argument('--source-pickle', dest='pickle', required=False, default=False,
                        help='FOR DEBUG: source from a pickle')
    parser.add_argument('--out', dest='out', required=True,
                        help='Output prefix')
    parser.add_argument('--white-list', dest='white_list',
                        default=False,
                        help='comma-separated list of major scaffolds to overrule minor ones')
    parser.add_argument('--dump-aln', dest='dump_aln',
                        default = False,
                        action='store_true')
    my_args = parser.parse_args()
    
    ################
    
    if my_args.pickle:
        
        pickle_reader = open(my_args.pickle, 'r')
        print "[%s] Staging in data from pickle..." % (ts())
        scaff_list = pickle.load(pickle_reader)
        print "[%s] Generating AGP from pickle..." % (ts())
        populate_agp(scaff_list, my_args.out, use_split=True, intergap=1000)
        sys.exit()
    
    if (not (my_args.summary or my_args.sspace) or 
        (my_args.summary and my_args.sspace)):
        sys.exit("Please provide scaffolding information (--space-evidence or --final-summary)")
    
    if my_args.sspace and not my_args.fasta:
        sys.exit("Need to provide fasta for sspace mode")
    
    print "[%s] Staging in data..." % (ts())
    if my_args.summary:
        scaff_list = read_allpaths_summary(my_args.summary, my_args.fasta)
    elif my_args.sspace:
        scaff_list = read_sspace(my_args.sspace, my_args.fasta)
    else:
        sys.exit("sspace or allpaths?")
    
    
    align_dict = {}
    ref_dict_names = {}
    for infile in my_args.aln_files:
        print "[%s] Parsing %s..." % (ts(), infile)
        populate_align_dict(infile, align_dict, ref_dict_names, 
                            my_args.min_aln_len, my_args.min_identity)
      
    distribute_align(scaff_list, align_dict)
      
    print "[%s] Defining blocks..." % (ts())
    define_blocks(scaff_list, ref_dict_names, min_aln_dist=my_args.min_aln_dist)
 
    print "[%s] Easy placing..." % (ts())
    place_blocks(scaff_list,
                 min_score=my_args.min_score * 2, min_fold=5.0) # min score and min ratio with second alternative block
     
    print "[%s] Rescuing by merging..." % (ts())
    merge_unassigned(scaff_list, my_args.out,
                     min_score=my_args.min_score, min_fold=3.5, #scoring limits to place merged objects
                     max_dist_ratio=4.0, # ratio of gaps (query vs subject)
                     max_abs_dist=250000, # max absolute gap to join
                     # one of th two above is sufficient to leave the algorithm to perfrom merging
                     min_delta_quantile=0.10) # the 0.x of highest score is the lowest will be considered for this phase
     
     
     
    print "[%s] Resolving inversions..." % (ts())
    resolve_inversions(scaff_list, my_args.out,
                       min_score=my_args.min_score,
                       min_fold=4.0,
                       min_delta_quantile=0.10,
                       max_dist_ratio=2.5,
                       max_abs_dist=100000)
    
    
    
    if my_args.white_list:
        white_list = my_args.white_list.split(",")
    else:
        white_list = False
    
    print "[%s] Performing splits..." % (ts())
    safe_split(scaff_list, blocks_to_run='merged_blocks',
               min_score=my_args.min_score,  # min score to consider in splitting routine
               max_overlap_ratio=0.30,
               white_list=white_list)
    
    
    
    #reader = open("fixes.pickle", 'r')
    
    #scaff_list = pickle.load(reader)
    

    print "[%s] Writing results..." % (ts())
    write_results(scaff_list, my_args.out)
    
    if my_args.dump_aln:
        print "[%s] Writing alignments (may take a while)..." % (ts())
        write_aln(scaff_list, my_args.out)
    #
    pickle_writer = open(my_args.out + ".pickle", 'wb')
    pickle.dump(scaff_list, pickle_writer, pickle.HIGHEST_PROTOCOL)
    
    populate_agp(scaff_list, my_args.out, use_split=True, intergap=1000)
    #
    #ipdb.set_trace()
    
    
