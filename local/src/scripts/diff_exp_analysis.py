#!/usr/bin/env python

"""

Authors: Giuseppe Pigola
Contact: gpigola@igatechnology.com
Date of creation: 2014-07-10
Version: 1.0.0
"""

import argparse
import os
import pipes
import logging


def diff_exp_analysis(scripts_dir, output_dir, input_files, **kwargs):
    try:
        if scripts_dir[:-1] != "/":
            scripts_dir += "/"
        if output_dir[:-1] == "/":
            output_dir = output_dir[:-1]

        prefix_diff_exp = kwargs.get('prefix_diff_exp', 'diff_exp')
        output_file1 = prefix_diff_exp + "_output.txt"
        output_file2 = prefix_diff_exp + "_normalized.txt"
        #modules = "module load lang/python/2.7.3; module load lang/r/2.15.1;"
        modules = "module purge; module load lang/python/2.7; module load lang/r/2.15.1;"
        do_diff_exp = kwargs.get('do_diff_exp', False)
        sample1 = kwargs.get('sample1')
        sample2 = kwargs.get('sample2')
        script_name = prefix_diff_exp + "_diff_exp_analysis.sh"
        normalization = kwargs.get('normalization', 'normalization')
        do_maplot = kwargs.get('do_maplot', False)
        filter_low_counts = kwargs.get('filter_low_counts', False)
        q = int(kwargs.get('q', 0))
        walltime = kwargs.get('walltime', "03:00:00")
        vmem = kwargs.get('vmem', "3Gb")
        job_name = kwargs.get('job_name', "diff_exp")
        sh = output_dir + "/" + script_name
        command1 = scripts_dir + "merge_files.py --output " + output_file1 \
            + " --input " + ' '.join(input_files)
        command2 = "R '--no-save --args smallrnafile=" + output_file1 \
                   + " outputfile=" + output_file2 + "' < " + scripts_dir \
                   + "do_edgeR_DEseq.r "
        command3 = ""
        if do_diff_exp:
            output = output_file1
            if normalization == 'normalization':
                output = output_file2
            dmp = "do_maplot=n"
            if do_maplot:
                dmp = "do_maplot=y"
            flc = "filter_low_counts=n"
            if filter_low_counts:
                flc = "filter_low_counts=y"
            command3 = "R ' --no-save --args indir=./" + " smallnafile=" \
                       + output + " sample1=" + sample1 + " sample2=" \
                       + sample2 + " outdir=./" + " outpref=" \
                       + prefix_diff_exp + " normalized=" \
                       + normalization + " " + dmp + " " + flc + "' < " \
                       + scripts_dir + "do_diff_exp.r "

        sh_file = open(sh, "w")
        sh_file.write("#LINE\n" + command1 + "\n" + command2 + "\n"
                      + command3 + "\n\n")
        sh_file.close()
        if q == 2:
            os.chdir(output_dir)
            t = pipes.Template()
            t.append("qsub -N" + job_name + " -d . -l vmem=" + vmem
                     + ",walltime=" + walltime, "--")
            f = t.open("pbs_file", "w")
            f.write(modules + "bash " + script_name)
            f.close()
            open("pbs_file").read()
            os.remove("pbs_file")
        elif q == 1:
            os.chdir(output_dir)
            os.system(modules + "bash " + script_name + " >" + prefix_diff_exp + "_out.txt"
                                                        "2>" + prefix_diff_exp + "_err.txt")
    except Exception as e:
        logger.error(str(e))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='smallrna_analysis_step2',
                                     description='Performs step 2 of smallRNA '
                                                 'analysis. Finds database '
                                                 'entry in tag count and '
                                                 'executes normalization by '
                                                 'using DEseq R package. '
                                                 'Directory structure '
                                                 'created by step 1 is '
                                                 'expected.')
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        required=False,
                        help="Verbose actions")
    parser.add_argument("-d", "--debug",
                        action="store_true",
                        dest="debug",
                        default=False,
                        required=False,
                        help="Debug actions")
    parser.add_argument("--scripts-dir",
                        dest="scripts_dir",
                        required=True,
                        help="The path of scripts pipeline")
    parser.add_argument("-o", "--output",
                        dest="output",
                        required=True,
                        help="Output directory")
    parser.add_argument("-i", "--input",
                        dest="input",
                        required=True,
                        nargs='+',
                        help="A list of file to be processed")

    parser.add_argument("--do-diff-exp",
                        dest="do_diff_exp",
                        action="store_true",
                        default=False,
                        required=False,
                        help="If present differential Expression analysis "
                             "is performed with DEseq (default False)")
    parser.add_argument("--sample1",
                        dest="sample1",
                        required=False,
                        help="A comma separated list of biological "
                             "replicates to be processed")
    parser.add_argument("--sample2",
                        dest="sample2",
                        required=False,
                        help="A comma separated list of biological "
                             "replicates to be processed")
    parser.add_argument("--prefix-diff-exp",
                        dest="prefix_diff_exp",
                        required=False,
                        default='diff_exp',
                        help="A prefix for differential expression analysis "
                             "output files (default diff_exp)")
    parser.add_argument("-n", "--normalization",
                        dest="normalization",
                        required=False,
                        default='normalization',
                        help="Possible values are raw or normalization "
                             "(default normalization)")
    parser.add_argument("--do-maplot",
                        dest="do_maplot",
                        action="store_true",
                        required=False,
                        help="if present maplot is executed (default False)")
    parser.add_argument("--filter-low-counts",
                        dest="filter_low_counts",
                        action="store_true",
                        default=False,
                        required=False,
                        help="If present low counts are filtered "
                             "(default False)")
    parser.add_argument("-q", "--qsub",
                        dest="q",
                        default=0,
                        required=False,
                        choices="012",
                        help="Possible values are 0,1,2. If 0 is specified "
                             "commands are not performed (a bash file is "
                             "created for a next execution). If 1 is "
                             "specified, commands are executed sequentially "
                             "one by one. If 2 is specified, commands are "
                             "executed sequentially one by one in a job using "
                             "qsub. (default 0).")
    parser.add_argument("--qsub-walltime",
                        dest="walltime",
                        default="03:00:00",
                        required=False,
                        help="Walltime. Used when -p parameter is set to 2 "
                             "(default 03:00:00)")
    parser.add_argument("--qsub-vmem",
                        dest="vmem",
                        default="3Gb",
                        required=False,
                        help="Memory usage. Used when -p parameter is set "
                             "to 2 (default 3Gb)")
    parser.add_argument("--qsub-job-name",
                        dest="job_name",
                        default="diff_exp",
                        required=False,
                        help="Name of job submitted to qsub. Used when -p "
                             "parameter is set to 2 (default smallRNA)")
    parser.add_argument('--version',
                        action='version',
                        version='%(prog)s 1.0.0')
    args = parser.parse_args()
    logger = logging.getLogger('Diff Exp Pipeline')
    if args.verbose:
        logging.basicConfig(level=logging.INFO)
    elif args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.NOTSET)
    if args.do_diff_exp is True and (args.sample1 is None
                                     or args.sample2 is None):
        parser.error("--do_diff_exp requires --sample1 --sample2")
    diff_exp_analysis(args.scripts_dir, args.output, args.input,
                     do_diff_exp=args.do_diff_exp,
                     sample1=args.sample1, sample2=args.sample2,
                     prefix_diff_exp=args.prefix_diff_exp,
                     normalization=args.normalization,
                     do_maplot=args.do_maplot,
                     filter_low_counts=args.filter_low_counts,
                     q=args.q, walltime=args.walltime,
                     vmem=args.vmem, job_name=args.job_name)
