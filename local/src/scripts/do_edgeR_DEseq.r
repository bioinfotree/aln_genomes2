norm.DESeq<-function(smallrnafile="nome del file con i mirna",outputfile="nome tabella normalizzata") {
	library(DESeq)
	mirnatable<-read.table(smallrnafile,sep="\t",header=TRUE,stringsAsFactors=FALSE,check.names=FALSE)
	mirnanames<-mirnatable[,1]
	mirnatable[,1]<-NULL
	conditions<-names(mirnatable)
	#Bug fix. Convert the table to numeric
	mirnatable<-apply(mirnatable,2,as.numeric)
	#Prudential step: remove empty columns, because they will cause all size factors to become NA
	mirnatable<-mirnatable[apply(mirnatable,2,sum)>0,]
	row.names(mirnatable)<-mirnanames
	#Remove miRNA with zero counts
	mirnatable<-mirnatable[apply(mirnatable,1,sum)>0,]
	cds <- newCountDataSet( mirnatable, conditions )
	cds <- estimateSizeFactors( cds )
	fattori.normalizzazione<-sizeFactors(cds)
	normalizedmirnatable<-t(apply(mirnatable,1,"/",sizeFactors(cds)))
	miRNA_id<-row.names(normalizedmirnatable)
	savenames<-names(normalizedmirnatable)
	normalizedmirnatable<-data.frame(miRNA_id,normalizedmirnatable,check.names=FALSE)
	#col.names(normalizedmirnatable)<-col.names(mirnatable)
	#row.names(normalizedmirnatable)<-row.names(mirnatable)
	write.table(normalizedmirnatable,outputfile,row.names=FALSE,quote=FALSE,sep="\t")
}

what.to.do=(commandArgs(TRUE))

if(length(what.to.do)>0) {
	smallrnafile<-as.character(unlist(strsplit(what.to.do[1],"="))[2])
	outputfile<-as.character(unlist(strsplit(what.to.do[2],"="))[2])
}
norm.DESeq(smallrnafile=smallrnafile,outputfile=outputfile)

