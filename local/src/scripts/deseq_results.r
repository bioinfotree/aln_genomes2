conta_eventi<-function(dove="/projects/ciliegio/share/pinosio/genicCNV", th=0.05)
{
setwd(dove)

coppie<-c("bigstar_brooks", "bigstar_enrica", "bigstar_katalin", "bigstar_kordia", "bigstar_regina", "bigstar_stella", "bigstar_summit", "bigstar_van", "bigstar_bakirtzeika", "bigstar_basler_adlerkirsche", "bigstar_burlat", "bigstar_durone_vignola", "bigstar_ferrovia", "bigstar_germersdorfer", "bigstar_ravenna", "bigstar_5-436", "bigstar_9-465", "bigstar_douc_4-2", "bigstar_hayntou1", "bigstar_ig4", "bigstar_katafito1", "bigstar_kwar_13-2", "bigstar_maia_1-3")

tot<-rep(0,length(coppie))
del<-rep(0,length(coppie))
dup<-rep(0,length(coppie))

tab<-data.frame(coppie, tot, del, dup)
tot<-data.frame()

for (cc in 1:(length(coppie))) {
	infile<-paste(coppie[cc],".txt",sep="")
	dati<-read.table(infile, stringsAsFactors=F, header=T)
	temp<-dati[,c(1,5,8)]
	names(temp)<-c("gene", paste(coppie[cc], "_foldChange", sep=""), paste(coppie[cc], "_padj", sep=""))
	if (cc == 1) {
	tot<-rbind(tot, temp)
	} else tot<-merge(tot, temp, by="gene", all=T, sort=F)
	dif<-dati[dati$padj<= th,]
	del<-dif[dif$baseMeanB > dif$baseMeanA,]
	dup<-dif[dif$baseMeanB < dif$baseMeanA,]
	tab$tot[cc]<-nrow(del)+nrow(dup)
	tab$del[cc]<-nrow(del)
	tab$dup[cc]<-nrow(dup)
}
write.table(tab, "deseq_results_summary.txt", row.names=F, quote=F)
write.table(tot, "deseq_merged_results.txt", row.names=F, quote=F)
}


# Funzione per analizzare i risultati
deseq_res<-function(dove="/projects/ciliegio/share/pinosio/genicCNV", th=0.05, infile="deseq_merged_results.txt", WM_genes_file="../SNP/candidate_reg/genes/WM_nucl_genes.txt", WL_genes_file="../SNP/candidate_reg/genes/WL_nucl_genes.txt", LM_genes_file="../SNP/candidate_reg/genes/LM_nucl_genes.txt")
{
setwd(dove)
tab<-read.table(infile, header=T, stringsAsFactors=F)
tab$sel<-"no"
temp<-tab[,c(1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47)]
temp[is.na(temp)]<-1

for (tt in 1:nrow(temp)) {
	if (sum(temp[tt,2:24]<0.05)>0) {
	tab$sel[tt]<-"yes"
	}
}
tab<-tab[tab$sel=="yes",]
tab<-tab[,1:(ncol(tab)-1)]
write.table(tab, "genicCNVs.txt", sep="\t", quote=F, row.names=F)

# Seleziono solo quelle WM
WM<-read.table(WM_genes_file, header=T, stringsAsFactors=F)
sel<-tab
sel<-merge(sel, WM, by="gene", sort=F, all=F)

sel$sel<-"no"
for (ss in 1:nrow(sel)) {
	if (sum(sel[ss,c(33,35,37,39,41,43,45,47)]<0.05)>5 & sum(sel[ss,c(3,5,7,9,11,13,15,17)]>=0.05)>5) {
	sel$sel[ss]<-"yes"
	}
}
sel<-sel[sel$sel=="yes",]
sel<-sel[,1:(ncol(sel)-1)]
write.table(sel, "WM_genicCNVs.txt", sep="\t", quote=F, row.names=F)


# Seleziono solo quelle WL
WL<-read.table(WL_genes_file, header=T, stringsAsFactors=F)
sel<-tab
sel<-merge(sel, WL, by="gene", sort=F, all=F)

sel$sel<-"no"
for (ss in 1:nrow(sel)) {
	if (sum(sel[ss,c(33,35,37,39,41,43,45,47)]<0.05)>5 & sum(sel[ss,c(19,21,23,25,27,29,31)]>=0.05)>5) {
	sel$sel[ss]<-"yes"
	}
}
sel<-sel[sel$sel=="yes",]
sel<-sel[,1:(ncol(sel)-1)]
write.table(sel, "WL_genicCNVs.txt", sep="\t", quote=F, row.names=F)

# Seleziono solo quelle LM
LM<-read.table(LM_genes_file, header=T, stringsAsFactors=F)
sel<-tab
sel<-merge(sel, LM, by="gene", sort=F, all=F)

sel$sel<-"no"
for (ss in 1:nrow(sel)) {
	if (sum(sel[ss,c(19,21,23,25,27,29,31)]<0.05)>5 & sum(sel[ss,c(3,5,7,9,11,13,15,17)]>=0.05)>5) {
	sel$sel[ss]<-"yes"
	}
}
sel<-sel[sel$sel=="yes",]
sel<-sel[,1:(ncol(sel)-1)]
write.table(sel, "LM_genicCNVs.txt", sep="\t", quote=F, row.names=F)

}
