#!/usr/bin/env python

"""

Authors: Giuseppe Pigola
Contact: gpigola@igatechnology.com
Date of creation: 2014-07-10
Version: 1.0.0
"""

import argparse
import logging
import collections


def merge_files(output_file, input_files):
    try:
        result_hash = collections.defaultdict(
            lambda: collections.defaultdict(list))
        for f in input_files:
            input_file = open(f, "r")
            line = input_file.readline()
            if line == "":
                break
            i = 0
            while True:
                line = input_file.readline()
                if line == "":
                    break
                line = line.strip()
                tags = line.split("\t")
                result_hash[tags[0]][f] = int(round(float(tags[1])))
                i += 1
            logger.info("Uploaded " + str(i) + " entries from " + f + "\n")
        out_file = open(output_file, "w")
        out_file.write("miRNA_id")
        for filename in input_files:
            out_file.write("\t" + filename)
        out_file.write("\n")

        for key in result_hash:
            out_file.write(key)
            for f in input_files:
                out_file.write("\t" + str(result_hash[key].get(f, 0)))
            out_file.write("\n")
        out_file.close()
    except Exception as e:
        logger.error(str(e))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='merge files',
                                     description='Merge two or more files '
                                                 'of counts into a table and '
                                                 'write it in the output '
                                                 'file.')
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        required=False,
                        help="Verbose actions")
    parser.add_argument("-d", "--debug",
                        action="store_true",
                        dest="debug",
                        default=False,
                        required=False,
                        help="Debug actions")
    parser.add_argument("-o", "--output",
                        dest="output",
                        required=True,
                        help="Output file")
    parser.add_argument("-i", "--input",
                        dest="input",
                        required=True,
                        nargs='+',
                        help="A list of file to be processed")
    parser.add_argument('--version',
                        action='version',
                        version='%(prog)s 1.0.0')
    args = parser.parse_args()
    logger = logging.getLogger('Diff Exp Pipeline')
    if args.verbose:
        logging.basicConfig(level=logging.INFO)
    elif args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.NOTSET)
    merge_files(args.output, args.input)