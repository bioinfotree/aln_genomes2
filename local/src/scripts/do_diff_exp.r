do.analysis<-function(indir="",outdir="",smallrnafile="",sel.samples=TRUE,tosel1="",tosel2="",sample.names=c("Sample1","Sample"),do.maplot=FALSE,maplotfile="maplot.pdf"
                     ,filter.low.counts=FALSE,do.DEseq=TRUE,outpdf="output.pdf",out.table="out_table.txt",
                     required.cpm=1,already.norm=TRUE,heat.pdf="heatmap.pdf") {
  library(edgeR)
  setwd(indir)
  mirnatable<-read.table(smallrnafile,sep="\t",header=TRUE,check.names=FALSE)
  if(ncol(mirnatable)>length(tosel1)+length(tosel2)) {
	  mirnanames<-mirnatable[,1]
	  mirnatable[,1]<-NULL
  } else mirnanames<-row.names(mirnatable)
  if(sel.samples) {
	  tosel<-c(tosel1,tosel2)
 	  mirnatable<-mirnatable[,tosel]
  }
  #if(do.maplot|do.edgeR) {
  mirnadge<-DGEList(mirnatable[,1:ncol(mirnatable)])
  row.names(mirnadge$counts)<-mirnanames
  mirnadge <- calcNormFactors(mirnadge,method="TMM")
  if(filter.low.counts) mirnadge<-mirnadge[rowSums(cpm(mirnadge)>required.cpm) >= 2,]
  if(do.maplot) all.pairwise.maplot(dgeobject=mirnadge,indir=indir,maplotfile=maplotfile)
  #if(do.edgeR) edgeR.analysis(dgeobject=mirnadge)
	if(do.DEseq) {
	row.names(mirnatable)<-mirnanames
	mirnatable<-mirnatable[apply(mirnatable,1,sum)>0,]
	mirnanames<-row.names(mirnatable)
	#Bug fix. Convert the table to numeric
	mirnatable<-apply(mirnatable,2,as.numeric)
	row.names(mirnatable)<-mirnanames
	DEseq.analysis(dgeobject=mirnatable,outdir=outdir,trt=tosel1,ctrl=tosel2,graph.names=sample.names,pdf.file=outpdf,
			heat.pdf=heat.pdf,out.table=out.table,already.norm=already.norm)
  } 
}

#Relatively generic function. Takes a dge object and performs pairwise ma plots for all possible pairwise comparisons
all.pairwise.maplot<-function(dgeobject=mirnadge,indir=indir,maplotfile=maplotfile) {
  pdf(maplotfile,width=10,height=7)

  #Select all the possible pairwise comaprisosns between samples
  pair.to.plot<-combn(seq(1,ncol(dgeobject$counts)),2)
  for(aaa in 1:ncol(pair.to.plot)) {
    cat(aaa,"\n")
    par(mfrow=c(1,2))
	titolo1<-paste(row.names(dgeobject$samples)[pair.to.plot[1,aaa]], "vs",row.names(dgeobject$samples)[pair.to.plot[2,aaa]],"non normalized")
	plot.object.unnorm<-maPlot(dgeobject$counts[,pair.to.plot[1,aaa]], dgeobject$counts[,pair.to.plot[2,aaa]], normalize=TRUE, pch=19, cex=0.4, ylim=c(-8, 8),main=titolo1)
	grid(col = "blue")
	abline(h = log2(dgeobject$samples$norm.factors[pair.to.plot[2,aaa]]/dgeobject$samples$norm.factors[pair.to.plot[1,aaa]]), col="red", lwd=4)
	eff.libsize <- dgeobject$samples$lib.size * dgeobject$samples$norm.factors
	titolo2<-paste(row.names(dgeobject$samples)[pair.to.plot[1,aaa]], "vs",row.names(dgeobject$samples)[pair.to.plot[2,aaa]],"normalized")
	plot.object.norm<-maPlot(dgeobject$counts[,pair.to.plot[1,aaa]]/eff.libsize[pair.to.plot[1,aaa]], dgeobject$counts[,pair.to.plot[2,aaa]]/eff.libsize[pair.to.plot[1,aaa]],
	normalize = FALSE, pch = 19, cex = 0.4, ylim = c(-8, 8),main=titolo2)
	grid(col = "blue")
	abline(h = median(plot.object.norm$M), col="blue", lwd=4)
  }
  dev.off()
}

dge.analysis<-function(dgeobject=mirnadge) {
	dgeobject$samples$group<-seq(1,length(dgeobject$samples$group))
	#y <- estimateCommonDisp(dgeobject)
	#y <- estimateTagwiseDisp(y)
	#et <- exactTest(y)

	#The lines below work also without replicates (but check warnings!)
	design <- model.matrix(~dgeobject$samples$group)
	y <- estimateGLMTrendedDisp(dgeobject,design)
	y <- estimateGLMTagwiseDisp(y,design)
	fit <- glmFit(y,design)
	lrt <- glmLRT(y,fit,coef=2)
	topTags(lrt)
}

DEseq.analysis<-function(outdir="",dgeobject=mirnatable,trt=trt,ctrl=ctrl,graph.names=c("Sample1","Sample2"),
		after.res=TRUE,pdf.file=outpdf,out.table=out.table,already.norm=TRUE,do.heatmap=TRUE,heat.pdf=heat.pdf) {
  library(DESeq)
  #if(ncol(dgeobject)!=2) stop("I'm sorry, at present I can only handle one-to-one comparisons")
  conditions<-factor(c(rep("T",length(trt)),rep("C",length(ctrl))))
  dgeobject<-round(dgeobject,0)
  dgeobject<-dgeobject[apply(dgeobject,1,sum)>0,]
  cds <- newCountDataSet( dgeobject, conditions )
  if(already.norm) cds$sizeFactor<-rep(1,length(cds$sizeFactor))
  if(!already.norm) cds <- estimateSizeFactors( cds )
  #estimateDispersion is the crucial part. method="blind" and sharingMode="fit-only" seems te methods of choice with no replicates.
  cds <- estimateDispersions( cds ,method="blind",sharingMode="fit-only",fitType="local")
  res <- nbinomTest( cds, "T", "C")
  #browser()
  to.round<-c("baseMean","baseMeanA","baseMeanB","foldChange","log2FoldChange","pval","padj")
  for(aaa in 1:length(to.round)) {
    res[,to.round[aaa]]<-round(res[,to.round[aaa]],3)
  }
  if(after.res) {
    setwd(outdir)
	names(res)[1]<-"miRNA_id"
	miRNA_id<-row.names(dgeobject)
	row.names(dgeobject)<-NULL
	dgeobject<-data.frame(miRNA_id,dgeobject,check.names=FALSE)
	#browser()
	new.res1<-merge(res,dgeobject,by="miRNA_id")
	write.table(new.res1,out.table,row.names=FALSE,sep="\t",quote=F)
	pdf(pdf.file)
	#Two plots to estimate the goodness of the estimated dispersion compared to the fitted dispersion (see DESeq help for help)
	#smoothScatter( log10(res$baseMean), log10(fitInfo(cds)$perGeneDispEsts) )
	#points(log10(res$baseMean),log10(fitInfo(cds)$fittedDispEsts),pch=".",col="red")
	#plot( log10(res$baseMean), log10(fitInfo(cds)$perGeneDispEsts) )
	#points(log10(res$baseMean),log10(fitInfo(cds)$fittedDispEsts),pch=".",col="red")
	#smoothScatter(fitInfo(cds)$perGeneDispEsts, fitInfo(cds)$fittedDispEsts )
	#plot( log10(fitInfo(cds)$perGeneDispEsts), log10(fitInfo(cds)$fittedDispEsts) )
	#plotMA(list(A=res$log2FoldChange,M=res$baseMean))
	#----------------------------
	#Plot the maplot
	titolo1<-paste(graph.names[1], "vs",graph.names[2])
	plot.object<-maPlot(res$baseMeanA, res$baseMeanB, normalize=FALSE, pch=19, cex=0.4, ylim=c(-8, 8),main=titolo1,de.tags=which(res$padj<0.05))
	#plot.object<-maPlot(dgeobject[[trt]], dgeobject[[ctrl]], normalize=FALSE, pch=19, cex=0.4, ylim=c(-8, 8),main=titolo1,de.tags=which(res$padj<0.05))
	grid(col = "blue")
	abline(h = median(plot.object$M), col="red", lwd=4)
	#hist(res$pval, breaks=100, col="skyblue", border="slateblue", main="")
	dev.off()
	if(do.heatmap) {
	#browser()
	  esf <- estimateSizeFactors( cds)
	  eds <- estimateDispersions( esf, method="blind" )
	  vsd <- getVarianceStabilizedData( eds )
	  colors <- colorRampPalette(c("white","darkblue"))(100)
	  #We only plot the second heatmap
	  #The second heatmap includes the best 10% p-values only
	  #If you want to try and plot the whole data
	  #Uncomment the first heatmap command (may crash due to problems with large data!!!)
	  toptenpercent<-round(nrow(res)/10,0)
      select<-order(res$pval)[1:toptenpercent]
	  pdf(heat.pdf)
	  #heatmap( vsd, col = colors, scale = "none")
	  heatmap( vsd[select,], col = colors, scale = "none")
	  dev.off()
    }
  }
}

#Normalization
understand.norm<-function() {
library(DESeq)
#Generate a random count matrix
counts<-matrix(round(runif(15,0,3),0),nrow=5,ncol=3,byrow=TRUE)
#Use DESeq to estimate factors size (i.e. normalization factors by which count columns will be divided)
pino<-estimateSizeFactorsForMatrix(counts)
#Dissect analysis step by step
locfunc<-median
loggeomeans <- rowMeans(log(counts))
redo.pino<-apply(counts, 2, function(cnts) exp(locfunc((log(cnts) - loggeomeans)[is.finite(loggeomeans)])))
#This should be equal to the first value of pino and redo.pino
first.col.pino<-exp(locfunc((log(counts[,1]) - loggeomeans)[is.finite(loggeomeans)]))
#Description: calculate the logarithm of the geometric mean of each row. Then, for each column find the exp of the median of the difference between each cell value and the corresponding
#             row loggeomean. This is the normalization factor by which each library should be divided. Compared to other normalization methods, looks like it works quite well.
browser()
}

what.to.do=(commandArgs(TRUE))

if(length(what.to.do)>0) {
	indir<-as.character(unlist(strsplit(what.to.do[1],"="))[2])
	smallrnafile<-as.character(unlist(strsplit(what.to.do[2],"="))[2])
	sample1<-as.character(unlist(strsplit(what.to.do[3],"="))[2])
	sample2<-as.character(unlist(strsplit(what.to.do[4],"="))[2])
	outdir<-as.character(unlist(strsplit(what.to.do[5],"="))[2])
	outpref<-as.character(unlist(strsplit(what.to.do[6],"="))[2])
	normalized<-as.character(unlist(strsplit(what.to.do[7],"="))[2])
	if(normalized=="normalized") normalized<-TRUE else normalized<-FALSE

	outpdf<-paste(outpref,"pdf",sep=".")
	out.table<-paste(outpref,"txt",sep=".")

	domaplot<-as.character(unlist(strsplit(what.to.do[8],"="))[2])
	if(domaplot=="y") domaplot<-TRUE else domaplot<-FALSE
	filter.low.counts<-as.character(unlist(strsplit(what.to.do[9],"="))[2])
	if(filter.low.counts=="y") filter.low.counts<-TRUE else filter.low.counts<-FALSE

	heatmapfile<-paste(outpref,"heatmap.pdf",sep=".")
	maplotfile<-paste(outpref,"maplot.pdf",sep=".")
}
#do.analysis(indir="./",outdir="./",smallrnafile="normalized.txt",sel.samples=TRUE,
#tosel1="/projects/igats/tmp/diff_cov_exp/test/LEU_target_hs_coverage_1_bp_enlarged_CNVs_input.txt",
#tosel2="/projects/igats/tmp/diff_cov_exp/test/target_hs_coverage_1_bp_enlarged_CNVs_input.txt",sample.names=c("Sample_1","Sample_2"),do.maplot=TRUE,maplotfile="maptest.pdf",filter.low.counts=TRUE,do.DEseq=TRUE,
#outpdf="outpdf.pdf",out.table="out.table.txt",already.norm=TRUE,heat.pdf="heatmapfile.pdf")

if(length(unlist(strsplit(sample1,split=",")))>1) {
	realsample1<-unlist(strsplit(sample1,split=","))
} else realsample1<-sample1

if(length(unlist(strsplit(sample2,split=",")))>1) {
	realsample2<-unlist(strsplit(sample2,split=","))
} else realsample2<-sample2

do.analysis(indir=indir,outdir=outdir,smallrnafile=smallrnafile,sel.samples=TRUE,tosel1=realsample1,tosel2=realsample2,sample.names=c("Sample_1","Sample_2"),do.maplot=domaplot,maplotfile=maplotfile,filter.low.counts=filter.low.counts,do.DEseq=TRUE,outpdf=outpdf,out.table=out.table,already.norm=normalized,heat.pdf=heatmapfile)



